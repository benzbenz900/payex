<!DOCTYPE html>
<head>
  <meta charset="utf-8">
  <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
  <title>WebAdmin</title>
  <meta name="keywords" content="" />
  <meta name="description" content="" />
  <meta name="viewport" content="width=device-width">
  <link rel="stylesheet" href="assets/css/templatemo_main.css">
  <script type="text/javascript" src="assets/js/jquery.js"></script>
  <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
     $('[data-toggle="tooltip"]').tooltip();
   });
 </script>
</head>
<body>
  <div class="navbar navbar-default" role="navigation">
    <div class="navbar-header">
      <div class="logo"><h1>WebAdmin</h1></div>
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
  </div>
  <div class="template-page-wrapper">
    <div class="navbar-collapse collapse templatemo-sidebar">
      <ul class="templatemo-sidebar-menu">
        <li><a href="./"><i class="fa fa-dashboard"></i> แคชบร์อด</a></li>
        <li><a href="./?a=users"><i class="fa fa-users"></i> ผู้ใช้</a></li>
        <li><a href="./?a=companies"><i class="fa fa-map-signs"></i> แคมเปน</a></li>
        <li><a href="./?a=exchanges"><i class="fa fa-refresh"></i> แลกเปลี่ยน</a></li>
        <li><a href="./?a=withdrawals"><i class="fa fa-dollar"></i> ถอนเงิน</a></li>
        <li><a href="./?a=testimonials"><i class="fa fa-comments-o"></i> คำติชม</a></li>
        <li><a href="./?a=pages"><i class="fa fa-folder-o"></i> หน้าเว็บ</a></li>
        <li><a href="./?a=settings&b=web"><i class="fa fa-cogs"></i> ตั้งค่าเว็บ</a></li>
        <li><a href="./?a=logout"><i class="fa fa-sign-out"></i> Logout</a></li>
      </ul>
    </div><!--/.navbar-collapse -->

    <div class="templatemo-content-wrapper">
      <div class="templatemo-content">