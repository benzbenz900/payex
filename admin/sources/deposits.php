<?php
$b = protect($_GET['b']);
$id = protect($_GET['id']);
?>
<ol class="breadcrumb">
	<li><a href="./">WebAdmin</a></li>
	<?php if($b == "complete") { ?>
	<li><a href="./?a=deposits">Deposits</a></li>
	<li class="active">Complete deposit</li>
	<?php } elseif($b == "delete") { ?>
	<li><a href="./?a=deposits">Deposits</a></li>
	<li class="active">Delete deposit</li>
	<?php } else { ?>
	<li class="active">Deposits</li>
	<?php } ?>
</ol>

<div class="row">
	<div class="col-lg-12">
			<?php
			if($b == "complete") {
			$query = $db->query("SELECT * FROM deposit_requests WHERE id='$id' and status='1'");
			if($query->num_rows==0) { header("Location: ./?a=deposits"); }
			$row = $query->fetch_assoc();
			$user = idinfo($row['uid'],"username");
			?>
				<div class="panel panel-primary">
                  <div class="panel-heading"> 
						<i class="fa fa-plus"></i> Complete deposit
				  </div>
                  <div class="panel-body">
					<?php
					if(isset($_GET['complete'])) {
						$time = time();
						$update = $db->query("UPDATE deposit_requests SET status='2' WHERE id='$id'");
						$check_wallet = $db->query("SELECT * FROM wallets WHERE uid='$row[uid]' and currency='$row[currency]'");
						if($check_wallet->num_rows>0) {
							$update_wallet = $db->query("UPDATE wallets SET amount=amount+$row[amount],updated='$time' WHERE uid='$row[uid]' and currency='$row[currency]'");
							echo success("You complete successfully deposit to <b>$user</b> wallet. Deposit amount $row[amount] $row[currency].");
						} else {
							$insert = $db->query("INSERT wallets (uid,amount,currency,created) VALUES ('$row[uid]','$row[amount]','$row[currency]','$time')");
							echo success("You complete successfully deposit to <b>$user</b> wallet. Deposit amount $row[amount] $row[currency].");
						}
					} else {
						echo info("Are you sure you want to add <b>$row[amount] $row[currency]</b> to <b>$user</b> wallet. Please check whether the transaction by the customer is completed before taking action.");
						echo '<a href="./?a=deposits&b=complete&id='.$row[id].'&complete=1" class="btn btn-success"><i class="fa fa-check"></i> Yes</a>&nbsp;&nbsp;
								<a href="./?a=deposits&b=delete&id'.$row[id].'" class="btn btn-danger"><i class="fa fa-times"></i> No, delete it</a>';
					}
					?>
				  </div>
				</div>
			<?php
			} elseif($b == "delete") {
			$query = $db->query("SELECT * FROM deposit_requests WHERE id='$id' and status='1'");
			if($query->num_rows==0) { header("Location: ./?a=deposits"); }
			$row = $query->fetch_assoc();
				?>
				<div class="panel panel-primary">
                  <div class="panel-heading"> 
						<i class="fa fa-times"></i> Delete deposit
				  </div>
                  <div class="panel-body">
					<?php
					if(isset($_GET['confirmed'])) {
						$user = idinfo($row['uid'],"username");
						$delete = $db->query("DELETE FROM deposit_requests WHERE id='$id'");
						echo success("Deposit from ($user) was deleted.");
					} else {
						$user = idinfo($row['uid'],"username");
						echo info("Are you sure you want to delete deposit from ($user) for $row[amount] $row[currency]?");
						echo '<a href="./?a=deposits&b=delete&id='.$row[id].'&confirmed=1" class="btn btn-success"><i class="fa fa-check"></i> Yes</a>&nbsp;&nbsp;
								<a href="./?a=deposits" class="btn btn-danger"><i class="fa fa-times"></i> No</a>';
					}
					?>
				  </div>
				</div>
				<?php
			} else {
				header("Location: ./?a=dashboard");
			}
			?>
	</div>
</div>