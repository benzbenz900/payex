<?php
$b = protect($_GET['b']);
$id = protect($_GET['id']);
?>
<ol class="breadcrumb">
	<li><a href="./">WebAdmin</a></li>
	<?php if($b == "approve") { ?>
	<li><a href="./?a=testimonials">Testimonials</a></li>
	<li class="active">Approve testimonial</li>
	<?php } elseif($b == "delete") { ?>
	<li><a href="./?a=testimonials">Testimonials</a></li>
	<li class="active">Delete testimonial</li>
	<?php } else { ?>
	<li class="active">Testimonials</li>
	<?php } ?>
</ol>

<div class="row">
	<div class="col-lg-12">
			<?php
			if($b == "approve") {
			$query = $db->query("SELECT * FROM testimonials WHERE id='$id'");
			if($query->num_rows==0) { header("Location: ./?a=testimonials"); }
			$row = $query->fetch_assoc();
				?>
				<div class="panel panel-primary">
                  <div class="panel-heading"> 
						<i class="fa fa-check"></i> Approve testimonial
				  </div>
                  <div class="panel-body">
					<?php
					if(isset($_GET['confirmed'])) {
						$user = idinfo($row['uid'],"username");
						$update = $db->query("UPDATE testimonials SET status='1' WHERE id='$id'");
						echo success("Testimonial from ($user) was approved.");
					} else {
						$user = idinfo($row['uid'],"username");
						echo info("Are you sure you want to approve testimonial from ($user)?");
						echo '<a href="./?a=testimonials&b=approve&id='.$row[id].'&confirmed=1" class="btn btn-success"><i class="fa fa-check"></i> Yes</a>&nbsp;&nbsp;
								<a href="./?a=testimonials" class="btn btn-danger"><i class="fa fa-times"></i> No</a>';
					}
					?>
				  </div>
				</div>
				<?php
			} elseif($b == "delete") {
			$query = $db->query("SELECT * FROM testimonials WHERE id='$id'");
			if($query->num_rows==0) { header("Location: ./?a=testimonials"); }
			$row = $query->fetch_assoc();
				?>
				<div class="panel panel-primary">
                  <div class="panel-heading"> 
						<i class="fa fa-times"></i> Delete testimonial
				  </div>
                  <div class="panel-body">
					<?php
					if(isset($_GET['confirmed'])) {
						$user = idinfo($row['uid'],"username");
						$delete = $db->query("DELETE FROM testimonials WHERE id='$id'");
						echo success("Testimonial from ($user) was deleted.");
					} else {
						$user = idinfo($row['uid'],"username");
						echo info("Are you sure you want to delete testimonial from ($user)?");
						echo '<a href="./?a=testimonials&b=delete&id='.$row[id].'&confirmed=1" class="btn btn-success"><i class="fa fa-check"></i> Yes</a>&nbsp;&nbsp;
								<a href="./?a=testimonials" class="btn btn-danger"><i class="fa fa-times"></i> No</a>';
					}
					?>
				  </div>
				</div>
				<?php
			} else {
				?>
				<div class="panel panel-primary">
                  <div class="panel-heading"> 
						<i class="fa fa-comments-o"></i> Testimonials 
				  </div>
                  <div class="panel-body">
					<table class="table table-hover">
						<thead>
							<tr>
								<td width="15%">From user</td>
								<td width="60%">Feedback</td>
								<td width="20%">Status</td>
								<td width="5%">Action</td>
							</tr>
						</thead>
						<tbody>
							<?php
							$page = (int) (!isset($_GET["page"]) ? 1 : $_GET["page"]);
							$limit = 20;
							$startpoint = ($page * $limit) - $limit;
							if($page == 1) {
								$i = 1;
							} else {
								$i = $page * $limit;
							}
							$statement = "testimonials";
							$query = $db->query("SELECT * FROM {$statement} ORDER BY status, id LIMIT {$startpoint} , {$limit}");
							if($query->num_rows>0) {
								while($row = $query->fetch_assoc()) {
									$rows[] = $row;
								}
								foreach($rows as $row) {
									?>
									<tr>
										<td><?php echo idinfo($row['uid'],"username"); ?></td>
										<td><?php echo $row['content']; ?></td>
										<td><?php if($row['status'] == "0") { echo 'Pending'; } else { echo 'Published'; } ?></td>
										<td>
										  <?php
										  if($row['status'] == "0") {
										  ?>
											<a href="./?a=testimonials&b=approve&id=<?php echo $row['id']; ?>" data-toggle="tooltip" data-placement="bottom" title="Approve"><i class="fa fa-check"></i></a> 
											<a href="./?a=testimonials&b=delete&id=<?php echo $row['id']; ?>" data-toggle="tooltip" data-placement="bottom" title="Delete"><i class="fa fa-times"></i></a>
										  <?php 
										  } else {
											echo '-';
										  }
										  ?>
										</td>
									</tr>
									<?php
								}
							} else {
								echo '<tr><td colspan="6">No have recorded testimonials.</td></tr>';
							}
							?>
						</tbody>
					</table>
					<?php
					$ver = "./?a=testimonials";
					if(admin_pagination($statement,$ver,$limit,$page)) {
						echo admin_pagination($statement,$ver,$limit,$page);
					}
					?>
			      </div>
				</div>
				<?php
			}
			?>
	</div>
</div>