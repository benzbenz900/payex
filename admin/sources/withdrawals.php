<?php
$b = protect($_GET['b']);
$id = protect($_GET['id']);
?>
<ol class="breadcrumb">
	<li><a href="./">WebAdmin</a></li>
	<?php if($b == "mark_completed") { ?>
	<li><a href="./?a=withdrawals">Withdrawals</a></li>
	<li class="active">Mark as completed</li>
	<?php } elseif($b == "mark_denied") { ?>
	<li><a href="./?a=withdrawals">Withdrawals</a></li>
	<li class="active">Mark as denied</li>
	<?php } else { ?>
	<li class="active">Withdrawals</li>
	<?php } ?>
</ol>

<div class="row">
	<div class="col-lg-12">
			<?php
			if($b == "mark_completed") {
			$query = $db->query("SELECT * FROM withdrawals WHERE id='$id'");
			if($query->num_rows==0) { header("Location: ./?a=withdrawals"); }
			$row = $query->fetch_assoc();
				?>
				<div class="panel panel-primary">
                  <div class="panel-heading"> 
						<i class="fa fa-check"></i> Mark as completed 
				  </div>
                  <div class="panel-body">
					<?php
					if(isset($_GET['confirmed'])) {
						$update = $db->query("UPDATE withdrawals SET status='2' WHERE id='$id'");
						echo success("Your changes was saved successfully.");
					} else {
						$user = idinfo($row['uid'],"username");
						echo info("Are you sure that you have completed the payment of <b>$user</b>?");
						echo '<a href="./?a=withdrawals&b=mark_completed&id='.$row[id].'&confirmed=1" class="btn btn-success"><i class="fa fa-check"></i> Yes</a>&nbsp;&nbsp;
								<a href="./?a=withdrawals" class="btn btn-danger"><i class="fa fa-times"></i> No</a>';
					}
					?>
				  </div>
				</div>
				<?php
			} elseif($b == "mark_denied") {
			$query = $db->query("SELECT * FROM withdrawals WHERE id='$id'");
			if($query->num_rows==0) { header("Location: ./?a=withdrawals"); }
			$row = $query->fetch_assoc();
				?>
				<div class="panel panel-primary">
                  <div class="panel-heading"> 
						<i class="fa fa-check"></i> Mark as denied 
				  </div>
                  <div class="panel-body">
					<?php
					if(isset($_GET['confirmed'])) {
						$update = $db->query("UPDATE withdrawals SET status='3' WHERE id='$id'");
						echo success("Your changes was saved successfully.");
					} else {
						$user = idinfo($row['uid'],"username");
						echo info("Are you sure you want to cancel the payment of <b>$user</b>?");
						echo '<a href="./?a=withdrawals&b=mark_denied&id='.$row[id].'&confirmed=1" class="btn btn-success"><i class="fa fa-check"></i> Yes</a>&nbsp;&nbsp;
								<a href="./?a=withdrawals" class="btn btn-danger"><i class="fa fa-times"></i> No</a>';
					}
					?>
				  </div>
				</div>
				<?php
			} else {
				?>
				<div class="panel panel-primary">
                  <div class="panel-heading"> 
						<i class="fa fa-dollar"></i> Withdrawals 
				  </div>
                  <div class="panel-body">
					<table class="table table-hover">
						<thead>
							<tr>
								<td width="15%">User</td>
								<td width="35%">Account</td>
								<td width="20%">Requested on</td>
								<td width="15%">Amount</td>
								<td width="15%">Status</td>
								<td width="5%">Action</td>
							</tr>
						</thead>
						<tbody>
							<?php
							$page = (int) (!isset($_GET["page"]) ? 1 : $_GET["page"]);
							$limit = 20;
							$startpoint = ($page * $limit) - $limit;
							if($page == 1) {
								$i = 1;
							} else {
								$i = $page * $limit;
							}
							$statement = "withdrawals";
							$query = $db->query("SELECT * FROM {$statement} ORDER BY status, id LIMIT {$startpoint} , {$limit}");
							if($query->num_rows>0) {
								while($row = $query->fetch_assoc()) {
									$rows[] = $row;
								}
								foreach($rows as $row) {
									?>
									<tr>
										<td><?php echo idinfo($row['uid'],"username"); ?></td>
										<td><?php echo $row['account']; ?> (<?php echo $row['company']; ?>)</td>
										<td><?php echo date("d/m/Y H:i",$row['requested_on']); ?></td>
										<td><?php echo $row['amount']; ?> <?php echo $row['currency']; ?></td>
										<td><?php if($row['status'] == "1") { echo 'In process'; } elseif($row['status'] == "2") { echo 'Completed'; } else { echo 'Denied'; } ?></td>
										<td>
										  <?php
										  if($row['status'] == "1") {
										  ?>
											<a href="./?a=withdrawals&b=mark_completed&id=<?php echo $row['id']; ?>" data-toggle="tooltip" data-placement="bottom" title="Mark as completed"><i class="fa fa-check"></i></a> 
											<a href="./?a=withdrawals&b=mark_denied&id=<?php echo $row['id']; ?>" data-toggle="tooltip" data-placement="bottom" title="Mark as denied"><i class="fa fa-times"></i></a>
										  <?php 
										  } else {
											echo '-';
										  }
										  ?>
										</td>
									</tr>
									<?php
								}
							} else {
								echo '<tr><td colspan="6">No have recorded withdrawals.</td></tr>';
							}
							?>
						</tbody>
					</table>
					<?php
					$ver = "./?a=withdrawals";
					if(admin_pagination($statement,$ver,$limit,$page)) {
						echo admin_pagination($statement,$ver,$limit,$page);
					}
					?>
			      </div>
				</div>
				<?php
			}
			?>
	</div>
</div>