<?php
$b = protect($_GET['b']);
$c = protect($_GET['c']);
$d = protect($_GET['d']);
$id = protect($_GET['id']);
?>
<ol class="breadcrumb">
	<li><a href="./">WebAdmin</a></li>
	<li class="active">Companies</li>
</ol>

<div class="row">
	<div class="col-lg-12">
		<?php
		if($b == "setup_account") {
			$id = protect($_GET['id']);
			$query = $db->query("SELECT * FROM companies WHERE id='$id'");
			if($query->num_rows==0) { header("Location: ./?a=companies"); }
			$row = $query->fetch_assoc();
			?>
			<div class="panel panel-primary">
				<div class="panel-heading">
					Setup account
				</div>
				<div class="panel-body">
					<?php
					$name = $row['name'];
					if($name == "PayPal") {
						if(isset($_POST['btn_setup'])) {
							$a_field_1 = protect($_POST['a_field_1']);
							if(empty($a_field_1)) { echo error("Please enter your PayPal email address."); }
							elseif(!isValidEmail($a_field_1)) { echo error("Please enter valid PayPal email address."); }
							else {
								$update = $db->query("UPDATE companies SET a_field_1='$a_field_1' WHERE id='$row[id]'");
								header("Location: ./?a=companies");
							}
						}
						?>
						<form action="" method="POST">
							<div class="form-group">
								<label>Your PayPal email address</label>
								<input type="text" class="form-control" name="a_field_1" value="<?php echo $row['a_field_1']; ?>">
							</div>
							<button type="submit" class="btn btn-primary" name="btn_setup"><i class="fa fa-cog"></i> Setup</button>
						</form>
						<?php
					} elseif($name == "Payeer") {
						if(isset($_POST['btn_setup'])) {
							$a_field_1 = protect($_POST['a_field_1']);
							$a_field_2 = protect($_POST['a_field_2']);
							if(empty($a_field_1)) { echo error("Please enter your Payeer account."); }
							elseif(strlen($a_field_1)<8) { echo error("Please enter valid Payeer account."); }
							elseif(empty($a_field_2)) { echo error("Please enter your Payeer secret key."); }
							else {
								$update = $db->query("UPDATE companies SET a_field_1='$a_field_1',a_field_2='$a_field_2' WHERE id='$row[id]'");
								header("Location: ./?a=companies");
							}
						}
						?>
						<form action="" method="POST">
							<div class="form-group">
								<label>Your Payeer</label>
								<input type="text" class="form-control" name="a_field_1" value="<?php echo $row['a_field_1']; ?>">
							</div>
							<div class="form-group">
								<label>Secret key</label>
								<input type="text" class="form-control" name="a_field_2" value="<?php echo $row['a_field_2']; ?>">
							</div>
							<button type="submit" class="btn btn-primary" name="btn_setup"><i class="fa fa-cog"></i> Setup</button>
						</form>
						<?php
					} elseif($name == "Perfect Money") {
						if(isset($_POST['btn_setup'])) {
							$a_field_1 = protect($_POST['a_field_1']);
							if(empty($a_field_1)) { echo error("Please enter your Perfect Money account."); }
							elseif(strlen($a_field_1)<7) { echo error("Please enter valid Perfect Money account."); }
							else {
								$update = $db->query("UPDATE companies SET a_field_1='$a_field_1' WHERE id='$row[id]'");
								header("Location: ./?a=companies");
							}
						}
						?>
						<form action="" method="POST">
							<div class="form-group">
								<label>Your Perfect Money account</label>
								<input type="text" class="form-control" name="a_field_1" value="<?php echo $row['a_field_1']; ?>">
							</div>
							<button type="submit" class="btn btn-primary" name="btn_setup"><i class="fa fa-cog"></i> Setup</button>
						</form>
						<?php
					} elseif($name == "AdvCash") {
						if(isset($_POST['btn_setup'])) {
							$a_field_1 = protect($_POST['a_field_1']);
							if(empty($a_field_1)) { echo error("Please enter your AdvCash email address."); }
							elseif(!isValidEmail($a_field_1)) { echo error("Please enter valid AdvCash email address."); }
							else {
								$update = $db->query("UPDATE companies SET a_field_1='$a_field_1' WHERE id='$row[id]'");
								header("Location: ./?a=companies");
							}
						}
						?>
						<form action="" method="POST">
							<div class="form-group">
								<label>Your AdvCash email address</label>
								<input type="text" class="form-control" name="a_field_1" value="<?php echo $row['a_field_1']; ?>">
							</div>
							<button type="submit" class="btn btn-primary" name="btn_setup"><i class="fa fa-cog"></i> Setup</button>
						</form>
						<?php
					} elseif($name == "OKPay") {
						if(isset($_POST['btn_setup'])) {
							$a_field_1 = protect($_POST['a_field_1']);
							if(empty($a_field_1)) { echo error("Please enter your OKPay account."); }
							elseif(strlen($a_field_1)<12) { echo error("Please enter valid OKPay account."); }
							else {
								$update = $db->query("UPDATE companies SET a_field_1='$a_field_1' WHERE id='$row[id]'");
								header("Location: ./?a=companies");
							}
						}
						?>
						<form action="" method="POST">
							<div class="form-group">
								<label>Your OKPay account</label>
								<input type="text" class="form-control" name="a_field_1" value="<?php echo $row['a_field_1']; ?>">
							</div>
							<button type="submit" class="btn btn-primary" name="btn_setup"><i class="fa fa-cog"></i> Setup</button>
						</form>
						<?php
					} elseif($name == "Bank Transfer") {
						if(isset($_POST['btn_setup'])) {
							$a_field_1 = protect($_POST['a_field_1']);
							$a_field_2 = protect($_POST['a_field_2']);
							$a_field_3 = protect($_POST['a_field_3']);
							$a_field_4 = protect($_POST['a_field_4']);
							$a_field_5 = protect($_POST['a_field_5']);
							if(empty($a_field_1) or empty($a_field_2) or empty($a_field_3) or empty($a_field_4) or empty($a_field_5)) { echo error("Please complete all fields."); }
							else {
								$update = $db->query("UPDATE companies SET a_field_1='$a_field_1',a_field_2='$a_field_2',a_field_3='$a_field_3',a_field_4='$a_field_4',a_field_5='$a_field_5' WHERE id='$row[id]'");
								header("Location: ./?a=companies");
							}
						}
						?>
						<form action="" method="POST">
							<div class="form-group">
								<label>ชื่อเจ้าของบัญชีธนาคาร</label>
								<input type="text" class="form-control" name="a_field_1" value="<?php echo $row['a_field_1'] ?>">
							</div>
							<div class="form-group">
								<label>หมายเลขบัญชีธนาคาร / IBAN</label>
								<input type="text" class="form-control" name="a_field_4" value="<?php echo $row['a_field_4']; ?>">
							</div>
							<div class="form-group">
								<label>SWIFT Code</label>
								<input type="text" class="form-control" name="a_field_5" value="<?php echo $row['a_field_5']; ?>">
							</div>
							<div class="form-group">
								<label>ชื่อธนาคาร</label>
								<input type="text" class="form-control" name="a_field_2" value="<?php echo $row['a_field_2']; ?>">
							</div>
							<div class="form-group">
								<label>สาขาธนาคารประเทศ, ที่อยู่</label>
								<input type="text" class="form-control" name="a_field_3" value="<?php echo $row['a_field_3']; ?>">
							</div>
							<button type="submit" class="btn btn-primary" name="btn_setup"><i class="fa fa-cog"></i> Setup</button>
						</form>
						<?php
					} elseif($name == "Western union") {
						if(isset($_POST['btn_setup'])) {
							$a_field_1 = protect($_POST['a_field_1']);
							$a_field_2 = protect($_POST['a_field_2']);
							if(empty($a_field_1)) { echo error("Please enter your full name."); }
							elseif(empty($a_field_2)) { echo error("Please enter your location."); }
							else {
								$update = $db->query("UPDATE companies SET a_field_1='$a_field_1',a_field_2='$a_field_2' WHERE id='$row[id]'");
								header("Location: ./?a=companies");
							}
						}
						?>
						<form action="" method="POST">
							<div class="form-group">
								<label>Your full name</label>
								<input type="text" class="form-control" name="a_field_1" value="<?php echo $row['a_field_1']; ?>">
							</div>
							<div class="form-group">
								<label>Your location</label>
								<input type="text" class="form-control" name="a_field_2" value="<?php echo $row['a_field_2']; ?>">
							</div>
							<button type="submit" class="btn btn-primary" name="btn_setup"><i class="fa fa-cog"></i> Setup</button>
						</form>
						<?php
					} elseif($name == "Moneygram") {
						if(isset($_POST['btn_setup'])) {
							$a_field_1 = protect($_POST['a_field_1']);
							$a_field_2 = protect($_POST['a_field_2']);
							if(empty($a_field_1)) { echo error("Please enter your full name."); }
							elseif(empty($a_field_2)) { echo error("Please enter your location."); }
							else {
								$update = $db->query("UPDATE companies SET a_field_1='$a_field_1',a_field_2='$a_field_2' WHERE id='$row[id]'");
								header("Location: ./?a=companies");
							}
						}
						?>
						<form action="" method="POST">
							<div class="form-group">
								<label>Your full name</label>
								<input type="text" class="form-control" name="a_field_1" value="<?php echo $row['a_field_1']; ?>">
							</div>
							<div class="form-group">
								<label>Your location</label>
								<input type="text" class="form-control" name="a_field_2" value="<?php echo $row['a_field_2']; ?>">
							</div>
							<button type="submit" class="btn btn-primary" name="btn_setup"><i class="fa fa-cog"></i> Setup</button>
						</form>
						<?php
					} elseif($name == "Entromoney") {
						if(isset($_POST['btn_setup'])) {
							$a_field_1 = protect($_POST['a_field_1']);
							$a_field_2 = protect($_POST['a_field_2']);
							$a_field_3 = protect($_POST['a_field_3']);
							$a_field_4 = protect($_POST['a_field_4']);
							if(empty($a_field_1)) { echo error("Please enter your account id."); }
							elseif(empty($a_field_2)) { echo error("Please enter your receiver."); }
							elseif(empty($a_field_3)) { echo error("Please enter SCI ID."); }
							elseif(empty($a_field_4)) { echo error("Please enter SCI PASS."); }
							else {
								$update = $db->query("UPDATE companies SET a_field_1='$a_field_1',a_field_2='$a_field_2',a_field_3='$a_field_3',a_field_4='$a_field_4' WHERE id='$row[id]'");
								header("Location: ./?a=companies");
							}
						}
						?>
						<form action="" method="POST">
							<div class="form-group">
								<label>Your Entromoney Account ID</label>
								<input type="text" class="form-control" name="a_field_1" value="<?php echo $row['a_field_1']; ?>">
							</div>
							<div class="form-group">
								<label>Your Entromoney Receiver (Example: U11111111 or E1111111)</label>
								<input type="text" class="form-control" name="a_field_2" value="<?php echo $row['a_field_2']; ?>">
							</div>
							<div class="form-group">
								<label>SCI ID</label>
								<input type="text" class="form-control" name="a_field_3" value="<?php echo $row['a_field_3']; ?>">
							</div>
							<div class="form-group">
								<label>SCI PASS</label>
								<input type="text" class="form-control" name="a_field_4" value="<?php echo $row['a_field_4']; ?>">
							</div>
							<button type="submit" class="btn btn-primary" name="btn_setup"><i class="fa fa-cog"></i> Setup</button>
						</form>
						<?php
					} elseif($name == "Bitcoin") {
						if(isset($_POST['btn_setup'])) {
							$a_field_1 = protect($_POST['a_field_1']);
							if(empty($a_field_1)) { echo error("Please enter your Bitcoin address."); }
							else {
								$update = $db->query("UPDATE companies SET a_field_1='$a_field_1' WHERE id='$row[id]'");
								header("Location: ./?a=companies");
							}
						}
						?>
						<form action="" method="POST">
							<div class="form-group">
								<label>Your Bitcoin Address</label>
								<input type="text" class="form-control" name="a_field_1" value="<?php echo $row['a_field_1']; ?>">
							</div>
							<button type="submit" class="btn btn-primary" name="btn_setup"><i class="fa fa-cog"></i> Setup</button>
						</form>
						<?php
					} elseif($name == "Payza") {
						if(isset($_POST['btn_setup'])) {
							$a_field_1 = protect($_POST['a_field_1']);
							$a_field_2 = protect($_POST['a_field_2']);
							if(empty($a_field_1)) { echo error("Please enter your Payza email address."); }
							elseif(empty($a_field_2)) { echo error("Please enter your Payza Merchant IPN SECURITY CODE."); }
							else {
								$update = $db->query("UPDATE companies SET a_field_2='$a_field_2',a_field_1='$a_field_1' WHERE id='$row[id]'");
								header("Location: ./?a=companies");
							}
						}
						?>
						<form action="" method="POST">
							<div class="form-group">
								<label>Your Payza address</label>
								<input type="text" class="form-control" name="a_field_1" value="<?php echo $row['a_field_1']; ?>">
							</div>
							<div class="form-group">
								<label>IPN SECURITY CODE</label>
								<input type="text" class="form-control" name="a_field_2" value="<?php echo $row['a_field_2']; ?>">
							</div>
							<button type="submit" class="btn btn-primary" name="btn_setup"><i class="fa fa-cog"></i> Setup</button>
						</form>
						<?php
					} else {
						header("Location: ./?a=companies");
					}
					?>
				</div>
			</div>
			<?php
		} elseif($b == "manageCurrenciesList") {
			if($c == "add") {
				$id = protect($_GET['id']);
				$query = $db->query("SELECT * FROM companies WHERE id='$id'");
				if($query->num_rows==0) { header("Location: ./?a=companies"); }
				$row = $query->fetch_assoc();
				?>
				<script type="text/javascript">
					function changeCur1(value) {
						$("#cur_from").html(value);
					}

					function changeCur2(value) {
						$("#cur_to").html(value);
					}
				</script>
				<div class="panel panel-primary">
					<div class="panel-heading">
						Add currency for <?php echo $row['name']; ?>
					</div>
					<div class="panel-body">
						<?php
						if(isset($_POST['btn_add'])) {
							$cid = $row['id'];
							$currency_from = protect($_POST['currency_from']);
							$currency_to = protect($_POST['currency_to']);
							$company_from = $row['name'];
							$company_to = protect($_POST['company_to']);
							$rate = protect($_POST['rate']);
							$check = $db->query("SELECT * FROM currencies WHERE cid='$cid' and company_from='$company_from' and company_to='$company_to' and currency_from='$currency_from' and currency_to='$currency_to'");
							if(empty($currency_from) or empty($currency_to) or empty($company_from) or empty($company_to) or empty($rate)) { echo error("All fields are required."); }
							elseif(!is_numeric($rate)) { echo error("Please enter exchange rate with numbers. Eg: 0.99 , 1.23 and etc."); }
							elseif($check->num_rows>0) { echo error("This currency is already exists."); }
							else {
								$insert = $db->query("INSERT currencies (cid,company_from,company_to,currency_from,currency_to,rate) VALUES ('$cid','$company_from','$company_to','$currency_from','$currency_to','$rate')");
								echo success("Currency from ($currency_from) to ($currency_to) was added successfully.");
							}
						}
						?>
						<form action="" method="POST">
							<div class="form-group">
								<label>From</label>
								<input type="text" class="form-control" name="company_from" disabled value="<?php echo $row['name']; ?>">
							</div>
							<div class="form-group">
								<label>To</label>
								<select class="form-control" name="company_to">
									<?php
									$list = explode(",",$row['receive_list']);
									foreach($list as $l) {
										if (strpos($l,'//') !== false) { } else {
											echo '<option value="'.$l.'">'.$l.'</option>';
										}
									}
									?>
								</select>
							</div>
							<div class="form-group">
								<label>From currency</label>
								<select class="form-control" name="currency_from" onchange="changeCur1(this.value);">
									<?php if($row['name'] == "Bitcoin") { ?>
									<option value="BTC">BTC - Bitcoin</option>
									<?php } else { ?>
									<option value="AED">AED - United Arab Emirates Dirham</option>
									<option value="AFN">AFN - Afghanistan Afghani</option>
									<option value="ALL">ALL - Albania Lek</option>
									<option value="AMD">AMD - Armenia Dram</option>
									<option value="ANG">ANG - Netherlands Antilles Guilder</option>
									<option value="AOA">AOA - Angola Kwanza</option>
									<option value="ARS">ARS - Argentina Peso</option>
									<option value="AUD">AUD - Australia Dollar</option>
									<option value="AWG">AWG - Aruba Guilder</option>
									<option value="AZN">AZN - Azerbaijan New Manat</option>
									<option value="BAM">BAM - Bosnia and Herzegovina Convertible Marka</option>
									<option value="BBD">BBD - Barbados Dollar</option>
									<option value="BDT">BDT - Bangladesh Taka</option>
									<option value="BGN">BGN - Bulgaria Lev</option>
									<option value="BHD">BHD - Bahrain Dinar</option>
									<option value="BIF">BIF - Burundi Franc</option>
									<option value="BMD">BMD - Bermuda Dollar</option>
									<option value="BND">BND - Brunei Darussalam Dollar</option>
									<option value="BOB">BOB - Bolivia Boliviano</option>
									<option value="BRL">BRL - Brazil Real</option>
									<option value="BSD">BSD - Bahamas Dollar</option>
									<option value="BTN">BTN - Bhutan Ngultrum</option>
									<option value="BWP">BWP - Botswana Pula</option>
									<option value="BYR">BYR - Belarus Ruble</option>
									<option value="BZD">BZD - Belize Dollar</option>
									<option value="CAD">CAD - Canada Dollar</option>
									<option value="CDF">CDF - Congo/Kinshasa Franc</option>
									<option value="CHF">CHF - Switzerland Franc</option>
									<option value="CLP">CLP - Chile Peso</option>
									<option value="CNY">CNY - China Yuan Renminbi</option>
									<option value="COP">COP - Colombia Peso</option>
									<option value="CRC">CRC - Costa Rica Colon</option>
									<option value="CUC">CUC - Cuba Convertible Peso</option>
									<option value="CUP">CUP - Cuba Peso</option>
									<option value="CVE">CVE - Cape Verde Escudo</option>
									<option value="CZK">CZK - Czech Republic Koruna</option>
									<option value="DJF">DJF - Djibouti Franc</option>
									<option value="DKK">DKK - Denmark Krone</option>
									<option value="DOP">DOP - Dominican Republic Peso</option>
									<option value="DZD">DZD - Algeria Dinar</option>
									<option value="EGP">EGP - Egypt Pound</option>
									<option value="ERN">ERN - Eritrea Nakfa</option>
									<option value="ETB">ETB - Ethiopia Birr</option>
									<option value="EUR">EUR - Euro Member Countries</option>
									<option value="FJD">FJD - Fiji Dollar</option>
									<option value="FKP">FKP - Falkland Islands (Malvinas) Pound</option>
									<option value="GBP">GBP - United Kingdom Pound</option>
									<option value="GEL">GEL - Georgia Lari</option>
									<option value="GGP">GGP - Guernsey Pound</option>
									<option value="GHS">GHS - Ghana Cedi</option>
									<option value="GIP">GIP - Gibraltar Pound</option>
									<option value="GMD">GMD - Gambia Dalasi</option>
									<option value="GNF">GNF - Guinea Franc</option>
									<option value="GTQ">GTQ - Guatemala Quetzal</option>
									<option value="GYD">GYD - Guyana Dollar</option>
									<option value="HKD">HKD - Hong Kong Dollar</option>
									<option value="HNL">HNL - Honduras Lempira</option>
									<option value="HPK">HRK - Croatia Kuna</option>
									<option value="HTG">HTG - Haiti Gourde</option>
									<option value="HUF">HUF - Hungary Forint</option>
									<option value="IDR">IDR - Indonesia Rupiah</option>
									<option value="ILS">ILS - Israel Shekel</option>
									<option value="IMP">IMP - Isle of Man Pound</option>
									<option value="INR">INR - India Rupee</option>
									<option value="IQD">IQD - Iraq Dinar</option>
									<option value="IRR">IRR - Iran Rial</option>
									<option value="ISK">ISK - Iceland Krona</option>
									<option value="JEP">JEP - Jersey Pound</option>
									<option value="JMD">JMD - Jamaica Dollar</option>
									<option value="JOD">JOD - Jordan Dinar</option>
									<option value="JPY">JPY - Japan Yen</option>
									<option value="KES">KES - Kenya Shilling</option>
									<option value="KGS">KGS - Kyrgyzstan Som</option>
									<option value="KHR">KHR - Cambodia Riel</option>
									<option value="KMF">KMF - Comoros Franc</option>
									<option value="KPW">KPW - Korea (North) Won</option>
									<option value="KRW">KRW - Korea (South) Won</option>
									<option value="KWD">KWD - Kuwait Dinar</option>
									<option value="KYD">KYD - Cayman Islands Dollar</option>
									<option value="KZT">KZT - Kazakhstan Tenge</option>
									<option value="LAK">LAK - Laos Kip</option>
									<option value="LBP">LBP - Lebanon Pound</option>
									<option value="LKR">LKR - Sri Lanka Rupee</option>
									<option value="LRD">LRD - Liberia Dollar</option>
									<option value="LSL">LSL - Lesotho Loti</option>
									<option value="LYD">LYD - Libya Dinar</option>
									<option value="MAD">MAD - Morocco Dirham</option>
									<option value="MDL">MDL - Moldova Leu</option>
									<option value="MGA">MGA - Madagascar Ariary</option>
									<option value="MKD">MKD - Macedonia Denar</option>
									<option value="MMK">MMK - Myanmar (Burma) Kyat</option>
									<option value="MNT">MNT - Mongolia Tughrik</option>
									<option value="MOP">MOP - Macau Pataca</option>
									<option value="MRO">MRO - Mauritania Ouguiya</option>
									<option value="MUR">MUR - Mauritius Rupee</option>
									<option value="MVR">MVR - Maldives (Maldive Islands) Rufiyaa</option>
									<option value="MWK">MWK - Malawi Kwacha</option>
									<option value="MXN">MXN - Mexico Peso</option>
									<option value="MYR">MYR - Malaysia Ringgit</option>
									<option value="MZN">MZN - Mozambique Metical</option>
									<option value="NAD">NAD - Namibia Dollar</option>
									<option value="NGN">NGN - Nigeria Naira</option>
									<option value="NTO">NIO - Nicaragua Cordoba</option>
									<option value="NOK">NOK - Norway Krone</option>
									<option value="NPR">NPR - Nepal Rupee</option>
									<option value="NZD">NZD - New Zealand Dollar</option>
									<option value="OMR">OMR - Oman Rial</option>
									<option value="PAB">PAB - Panama Balboa</option>
									<option value="PEN">PEN - Peru Nuevo Sol</option>
									<option value="PGK">PGK - Papua New Guinea Kina</option>
									<option value="PHP">PHP - Philippines Peso</option>
									<option value="PKR">PKR - Pakistan Rupee</option>
									<option value="PLN">PLN - Poland Zloty</option>
									<option value="PYG">PYG - Paraguay Guarani</option>
									<option value="QAR">QAR - Qatar Riyal</option>
									<option value="RON">RON - Romania New Leu</option>
									<option value="RSD">RSD - Serbia Dinar</option>
									<option value="RUB">RUB - Russia Ruble</option>
									<option value="RWF">RWF - Rwanda Franc</option>
									<option value="SAR">SAR - Saudi Arabia Riyal</option>
									<option value="SBD">SBD - Solomon Islands Dollar</option>
									<option value="SCR">SCR - Seychelles Rupee</option>
									<option value="SDG">SDG - Sudan Pound</option>
									<option value="SEK">SEK - Sweden Krona</option>
									<option value="SGD">SGD - Singapore Dollar</option>
									<option value="SHP">SHP - Saint Helena Pound</option>
									<option value="SLL">SLL - Sierra Leone Leone</option>
									<option value="SOS">SOS - Somalia Shilling</option>
									<option value="SRL">SPL* - Seborga Luigino</option>
									<option value="SRD">SRD - Suriname Dollar</option>
									<option value="STD">STD - Sao Tome and Principe Dobra</option>
									<option value="SVC">SVC - El Salvador Colon</option>
									<option value="SYP">SYP - Syria Pound</option>
									<option value="SZL">SZL - Swaziland Lilangeni</option>
									<option value="THB">THB - Thailand Baht</option>
									<option value="TJS">TJS - Tajikistan Somoni</option>
									<option value="TMT">TMT - Turkmenistan Manat</option>
									<option value="TND">TND - Tunisia Dinar</option>
									<option value="TOP">TOP - Tonga Pa'anga</option>
									<option value="TRY">TRY - Turkey Lira</option>
									<option value="TTD">TTD - Trinidad and Tobago Dollar</option>
									<option value="TVD">TVD - Tuvalu Dollar</option>
									<option value="TWD">TWD - Taiwan New Dollar</option>
									<option value="TZS">TZS - Tanzania Shilling</option>
									<option value="UAH">UAH - Ukraine Hryvnia</option>
									<option value="UGX">UGX - Uganda Shilling</option>
									<option value="USD">USD - United States Dollar</option>
									<option value="UYU">UYU - Uruguay Peso</option>
									<option value="UZS">UZS - Uzbekistan Som</option>
									<option value="VEF">VEF - Venezuela Bolivar</option>
									<option value="VND">VND - Viet Nam Dong</option>
									<option value="VUV">VUV - Vanuatu Vatu</option>
									<option value="WST">WST - Samoa Tala</option>
									<option value="XAF">XAF - Communaute Financiere Africaine (BEAC) CFA Franc BEAC</option>
									<option value="XCD">XCD - East Caribbean Dollar</option>
									<option value="XDR">XDR - International Monetary Fund (IMF) Special Drawing Rights</option>
									<option value="XOF">XOF - Communaute Financiere Africaine (BCEAO) Franc</option>
									<option value="XPF">XPF - Comptoirs Francais du Pacifique (CFP) Franc</option>
									<option value="YER">YER - Yemen Rial</option>
									<option value="ZAR">ZAR - South Africa Rand</option>
									<option value="ZMW">ZMW - Zambia Kwacha</option>
									<option value="ZWD">ZWD - Zimbabwe Dollar</option>
									<?php } ?>
								</select>
							</div>
							<div class="form-group">
								<label>To currency</label>
								<select class="form-control" name="currency_to" onchange="changeCur2(this.value);">
									<option value="AED">AED - United Arab Emirates Dirham</option>
									<option value="AFN">AFN - Afghanistan Afghani</option>
									<option value="ALL">ALL - Albania Lek</option>
									<option value="AMD">AMD - Armenia Dram</option>
									<option value="ANG">ANG - Netherlands Antilles Guilder</option>
									<option value="AOA">AOA - Angola Kwanza</option>
									<option value="ARS">ARS - Argentina Peso</option>
									<option value="AUD">AUD - Australia Dollar</option>
									<option value="AWG">AWG - Aruba Guilder</option>
									<option value="AZN">AZN - Azerbaijan New Manat</option>
									<option value="BTC">BTC - Bitcoin</option>
									<option value="BAM">BAM - Bosnia and Herzegovina Convertible Marka</option>
									<option value="BBD">BBD - Barbados Dollar</option>
									<option value="BDT">BDT - Bangladesh Taka</option>
									<option value="BGN">BGN - Bulgaria Lev</option>
									<option value="BHD">BHD - Bahrain Dinar</option>
									<option value="BIF">BIF - Burundi Franc</option>
									<option value="BMD">BMD - Bermuda Dollar</option>
									<option value="BND">BND - Brunei Darussalam Dollar</option>
									<option value="BOB">BOB - Bolivia Boliviano</option>
									<option value="BRL">BRL - Brazil Real</option>
									<option value="BSD">BSD - Bahamas Dollar</option>
									<option value="BTN">BTN - Bhutan Ngultrum</option>
									<option value="BWP">BWP - Botswana Pula</option>
									<option value="BYR">BYR - Belarus Ruble</option>
									<option value="BZD">BZD - Belize Dollar</option>
									<option value="CAD">CAD - Canada Dollar</option>
									<option value="CDF">CDF - Congo/Kinshasa Franc</option>
									<option value="CHF">CHF - Switzerland Franc</option>
									<option value="CLP">CLP - Chile Peso</option>
									<option value="CNY">CNY - China Yuan Renminbi</option>
									<option value="COP">COP - Colombia Peso</option>
									<option value="CRC">CRC - Costa Rica Colon</option>
									<option value="CUC">CUC - Cuba Convertible Peso</option>
									<option value="CUP">CUP - Cuba Peso</option>
									<option value="CVE">CVE - Cape Verde Escudo</option>
									<option value="CZK">CZK - Czech Republic Koruna</option>
									<option value="DJF">DJF - Djibouti Franc</option>
									<option value="DKK">DKK - Denmark Krone</option>
									<option value="DOP">DOP - Dominican Republic Peso</option>
									<option value="DZD">DZD - Algeria Dinar</option>
									<option value="EGP">EGP - Egypt Pound</option>
									<option value="ERN">ERN - Eritrea Nakfa</option>
									<option value="ETB">ETB - Ethiopia Birr</option>
									<option value="EUR">EUR - Euro Member Countries</option>
									<option value="FJD">FJD - Fiji Dollar</option>
									<option value="FKP">FKP - Falkland Islands (Malvinas) Pound</option>
									<option value="GBP">GBP - United Kingdom Pound</option>
									<option value="GEL">GEL - Georgia Lari</option>
									<option value="GGP">GGP - Guernsey Pound</option>
									<option value="GHS">GHS - Ghana Cedi</option>
									<option value="GIP">GIP - Gibraltar Pound</option>
									<option value="GMD">GMD - Gambia Dalasi</option>
									<option value="GNF">GNF - Guinea Franc</option>
									<option value="GTQ">GTQ - Guatemala Quetzal</option>
									<option value="GYD">GYD - Guyana Dollar</option>
									<option value="HKD">HKD - Hong Kong Dollar</option>
									<option value="HNL">HNL - Honduras Lempira</option>
									<option value="HPK">HRK - Croatia Kuna</option>
									<option value="HTG">HTG - Haiti Gourde</option>
									<option value="HUF">HUF - Hungary Forint</option>
									<option value="IDR">IDR - Indonesia Rupiah</option>
									<option value="ILS">ILS - Israel Shekel</option>
									<option value="IMP">IMP - Isle of Man Pound</option>
									<option value="INR">INR - India Rupee</option>
									<option value="IQD">IQD - Iraq Dinar</option>
									<option value="IRR">IRR - Iran Rial</option>
									<option value="ISK">ISK - Iceland Krona</option>
									<option value="JEP">JEP - Jersey Pound</option>
									<option value="JMD">JMD - Jamaica Dollar</option>
									<option value="JOD">JOD - Jordan Dinar</option>
									<option value="JPY">JPY - Japan Yen</option>
									<option value="KES">KES - Kenya Shilling</option>
									<option value="KGS">KGS - Kyrgyzstan Som</option>
									<option value="KHR">KHR - Cambodia Riel</option>
									<option value="KMF">KMF - Comoros Franc</option>
									<option value="KPW">KPW - Korea (North) Won</option>
									<option value="KRW">KRW - Korea (South) Won</option>
									<option value="KWD">KWD - Kuwait Dinar</option>
									<option value="KYD">KYD - Cayman Islands Dollar</option>
									<option value="KZT">KZT - Kazakhstan Tenge</option>
									<option value="LAK">LAK - Laos Kip</option>
									<option value="LBP">LBP - Lebanon Pound</option>
									<option value="LKR">LKR - Sri Lanka Rupee</option>
									<option value="LRD">LRD - Liberia Dollar</option>
									<option value="LSL">LSL - Lesotho Loti</option>
									<option value="LYD">LYD - Libya Dinar</option>
									<option value="MAD">MAD - Morocco Dirham</option>
									<option value="MDL">MDL - Moldova Leu</option>
									<option value="MGA">MGA - Madagascar Ariary</option>
									<option value="MKD">MKD - Macedonia Denar</option>
									<option value="MMK">MMK - Myanmar (Burma) Kyat</option>
									<option value="MNT">MNT - Mongolia Tughrik</option>
									<option value="MOP">MOP - Macau Pataca</option>
									<option value="MRO">MRO - Mauritania Ouguiya</option>
									<option value="MUR">MUR - Mauritius Rupee</option>
									<option value="MVR">MVR - Maldives (Maldive Islands) Rufiyaa</option>
									<option value="MWK">MWK - Malawi Kwacha</option>
									<option value="MXN">MXN - Mexico Peso</option>
									<option value="MYR">MYR - Malaysia Ringgit</option>
									<option value="MZN">MZN - Mozambique Metical</option>
									<option value="NAD">NAD - Namibia Dollar</option>
									<option value="NGN">NGN - Nigeria Naira</option>
									<option value="NTO">NIO - Nicaragua Cordoba</option>
									<option value="NOK">NOK - Norway Krone</option>
									<option value="NPR">NPR - Nepal Rupee</option>
									<option value="NZD">NZD - New Zealand Dollar</option>
									<option value="OMR">OMR - Oman Rial</option>
									<option value="PAB">PAB - Panama Balboa</option>
									<option value="PEN">PEN - Peru Nuevo Sol</option>
									<option value="PGK">PGK - Papua New Guinea Kina</option>
									<option value="PHP">PHP - Philippines Peso</option>
									<option value="PKR">PKR - Pakistan Rupee</option>
									<option value="PLN">PLN - Poland Zloty</option>
									<option value="PYG">PYG - Paraguay Guarani</option>
									<option value="QAR">QAR - Qatar Riyal</option>
									<option value="RON">RON - Romania New Leu</option>
									<option value="RSD">RSD - Serbia Dinar</option>
									<option value="RUB">RUB - Russia Ruble</option>
									<option value="RWF">RWF - Rwanda Franc</option>
									<option value="SAR">SAR - Saudi Arabia Riyal</option>
									<option value="SBD">SBD - Solomon Islands Dollar</option>
									<option value="SCR">SCR - Seychelles Rupee</option>
									<option value="SDG">SDG - Sudan Pound</option>
									<option value="SEK">SEK - Sweden Krona</option>
									<option value="SGD">SGD - Singapore Dollar</option>
									<option value="SHP">SHP - Saint Helena Pound</option>
									<option value="SLL">SLL - Sierra Leone Leone</option>
									<option value="SOS">SOS - Somalia Shilling</option>
									<option value="SRL">SPL* - Seborga Luigino</option>
									<option value="SRD">SRD - Suriname Dollar</option>
									<option value="STD">STD - Sao Tome and Principe Dobra</option>
									<option value="SVC">SVC - El Salvador Colon</option>
									<option value="SYP">SYP - Syria Pound</option>
									<option value="SZL">SZL - Swaziland Lilangeni</option>
									<option value="THB">THB - Thailand Baht</option>
									<option value="TJS">TJS - Tajikistan Somoni</option>
									<option value="TMT">TMT - Turkmenistan Manat</option>
									<option value="TND">TND - Tunisia Dinar</option>
									<option value="TOP">TOP - Tonga Pa'anga</option>
									<option value="TRY">TRY - Turkey Lira</option>
									<option value="TTD">TTD - Trinidad and Tobago Dollar</option>
									<option value="TVD">TVD - Tuvalu Dollar</option>
									<option value="TWD">TWD - Taiwan New Dollar</option>
									<option value="TZS">TZS - Tanzania Shilling</option>
									<option value="UAH">UAH - Ukraine Hryvnia</option>
									<option value="UGX">UGX - Uganda Shilling</option>
									<option value="USD">USD - United States Dollar</option>
									<option value="UYU">UYU - Uruguay Peso</option>
									<option value="UZS">UZS - Uzbekistan Som</option>
									<option value="VEF">VEF - Venezuela Bolivar</option>
									<option value="VND">VND - Viet Nam Dong</option>
									<option value="VUV">VUV - Vanuatu Vatu</option>
									<option value="WST">WST - Samoa Tala</option>
									<option value="XAF">XAF - Communaute Financiere Africaine (BEAC) CFA Franc BEAC</option>
									<option value="XCD">XCD - East Caribbean Dollar</option>
									<option value="XDR">XDR - International Monetary Fund (IMF) Special Drawing Rights</option>
									<option value="XOF">XOF - Communaute Financiere Africaine (BCEAO) Franc</option>
									<option value="XPF">XPF - Comptoirs Francais du Pacifique (CFP) Franc</option>
									<option value="YER">YER - Yemen Rial</option>
									<option value="ZAR">ZAR - South Africa Rand</option>
									<option value="ZMW">ZMW - Zambia Kwacha</option>
									<option value="ZWD">ZWD - Zimbabwe Dollar</option>
								</select>
							</div>
							<div class="form-group">
								<label>Exchange rate</label>
								<div class="input-group">
									<span class="input-group-addon">1 <span id="cur_from"><?php if($row['name'] == "Bitcoin") { echo 'BTC'; } else { echo 'AED'; } ?></span> = </span>
									<input type="text" class="form-control" name="rate" aria-label="Amount">
									<span class="input-group-addon"><span id="cur_to">AED</span></span>
								</div>
							</div>
							<button type="submit" class="btn btn-primary" name="btn_add"><i class="fa fa-plus"></i> Add</button>
						</form>
					</div>
				</div>
				<?php
			} elseif($c == "edit") {
				$id = protect($_GET['id']);
				$eid = protect($_GET['eid']);
				$query = $db->query("SELECT * FROM currencies WHERE id='$eid'");
				if($query->num_rows==0) { header("Location: ./?a=companies&b=manageCurrenciesList&id=$id"); }
				$row = $query->fetch_assoc();
				$companyQuery = $db->query("SELECT * FROM companies WHERE id='$row[cid]'");
				$row2 = $companyQuery->fetch_assoc();
				?>
				<script type="text/javascript">
					function changeCur1(value) {
						$("#cur_from").html(value);
					}

					function changeCur2(value) {
						$("#cur_to").html(value);
					}
				</script>
				<div class="panel panel-primary">
					<div class="panel-heading">
						Edit currency
					</div>
					<div class="panel-body">
						<?php
						if(isset($_POST['btn_save'])) {
							$cid = $row['cid'];
							$currency_from = protect($_POST['currency_from']);
							$currency_to = protect($_POST['currency_to']);
							$company_from = $row['company_from'];
							$company_to = protect($_POST['company_to']);
							$rate = protect($_POST['rate']);
							$check = $db->query("SELECT * FROM currencies WHERE cid='$cid' and company_from='$company_from' and company_to='$company_to' and currency_from='$currency_from' and currency_to='$currency_to'");
							if(empty($currency_from) or empty($currency_to) or empty($company_from) or empty($company_to) or empty($rate)) { echo error("All fields are required."); }
							elseif(!is_numeric($rate)) { echo error("Please enter exchange rate with numbers. Eg: 0.99 , 1.23 and etc."); }
							else {
								$update = $db->query("UPDATE currencies SET company_from='$company_from',company_to='$company_to',currency_from='$currency_from',currency_to='$currency_to',rate='$rate' WHERE id='$eid'");
								$query = $db->query("SELECT * FROM currencies WHERE id='$eid'");
								$row = $query->fetch_assoc();
								echo success("Your changes was saved successfully.");
							}
						}
						?>
						<form action="" method="POST">
							<div class="form-group">
								<label>From</label>
								<input type="text" class="form-control" name="company_from" disabled value="<?php echo $row['company_from']; ?>">
							</div>
							<div class="form-group">
								<label>To</label>
								<select class="form-control" name="company_to">
									<?php
									$list = explode(",",$row2['receive_list']);
									foreach($list as $l) {
										if (strpos($l,'//') !== false) { } else {
											if($row['company_to'] == $l) { $sel = 'selected'; } else { $sel = ''; }
											echo '<option value="'.$l.'" '.$sel.'>'.$l.'</option>';
										}
									}
									?>
								</select>
							</div>
							<div class="form-group">
								<label>From currency</label>
								<select class="form-control" name="currency_from" onchange="changeCur1(this.value);">
									<?php if($row['company_from'] == "Bitcoin") { ?>
									<option value="BTC">BTC - Bitcoin</option>
									<?php } else { ?>
									<option value="AED" <?php if($row['currency_from'] == "AED") { echo 'selected'; } ?>>AED - United Arab Emirates Dirham</option>
									<option value="AFN" <?php if($row['currency_from'] == "AFN") { echo 'selected'; } ?>>AFN - Afghanistan Afghani</option>
									<option value="ALL" <?php if($row['currency_from'] == "ALL") { echo 'selected'; } ?>>ALL - Albania Lek</option>
									<option value="AMD" <?php if($row['currency_from'] == "AMD") { echo 'selected'; } ?>>AMD - Armenia Dram</option>
									<option value="ANG" <?php if($row['currency_from'] == "ANG") { echo 'selected'; } ?>>ANG - Netherlands Antilles Guilder</option>
									<option value="AOA" <?php if($row['currency_from'] == "AOA") { echo 'selected'; } ?>>AOA - Angola Kwanza</option>
									<option value="ARS" <?php if($row['currency_from'] == "ARS") { echo 'selected'; } ?>>ARS - Argentina Peso</option>
									<option value="AUD" <?php if($row['currency_from'] == "AUD") { echo 'selected'; } ?>>AUD - Australia Dollar</option>
									<option value="AWG" <?php if($row['currency_from'] == "AWG") { echo 'selected'; } ?>>AWG - Aruba Guilder</option>
									<option value="AZN" <?php if($row['currency_from'] == "AZN") { echo 'selected'; } ?>>AZN - Azerbaijan New Manat</option>
									<option value="BAM" <?php if($row['currency_from'] == "BAM") { echo 'selected'; } ?>>BAM - Bosnia and Herzegovina Convertible Marka</option>
									<option value="BBD" <?php if($row['currency_from'] == "BBD") { echo 'selected'; } ?>>BBD - Barbados Dollar</option>
									<option value="BDT" <?php if($row['currency_from'] == "BDT") { echo 'selected'; } ?>>BDT - Bangladesh Taka</option>
									<option value="BGN" <?php if($row['currency_from'] == "BGN") { echo 'selected'; } ?>>BGN - Bulgaria Lev</option>
									<option value="BHD" <?php if($row['currency_from'] == "BHD") { echo 'selected'; } ?>>BHD - Bahrain Dinar</option>
									<option value="BIF" <?php if($row['currency_from'] == "BIF") { echo 'selected'; } ?>>BIF - Burundi Franc</option>
									<option value="BMD" <?php if($row['currency_from'] == "BMD") { echo 'selected'; } ?>>BMD - Bermuda Dollar</option>
									<option value="BND" <?php if($row['currency_from'] == "BND") { echo 'selected'; } ?>>BND - Brunei Darussalam Dollar</option>
									<option value="BOB" <?php if($row['currency_from'] == "BOB") { echo 'selected'; } ?>>BOB - Bolivia Boliviano</option>
									<option value="BRL" <?php if($row['currency_from'] == "BRL") { echo 'selected'; } ?>>BRL - Brazil Real</option>
									<option value="BSD" <?php if($row['currency_from'] == "BSD") { echo 'selected'; } ?>>BSD - Bahamas Dollar</option>
									<option value="BTN" <?php if($row['currency_from'] == "BTN") { echo 'selected'; } ?>>BTN - Bhutan Ngultrum</option>
									<option value="BWP" <?php if($row['currency_from'] == "BWP") { echo 'selected'; } ?>>BWP - Botswana Pula</option>
									<option value="BYR" <?php if($row['currency_from'] == "BYR") { echo 'selected'; } ?>>BYR - Belarus Ruble</option>
									<option value="BZD" <?php if($row['currency_from'] == "BZD") { echo 'selected'; } ?>>BZD - Belize Dollar</option>
									<option value="CAD" <?php if($row['currency_from'] == "CAD") { echo 'selected'; } ?>>CAD - Canada Dollar</option>
									<option value="CDF" <?php if($row['currency_from'] == "CDF") { echo 'selected'; } ?>>CDF - Congo/Kinshasa Franc</option>
									<option value="CHF" <?php if($row['currency_from'] == "CHF") { echo 'selected'; } ?>>CHF - Switzerland Franc</option>
									<option value="CLP" <?php if($row['currency_from'] == "CLP") { echo 'selected'; } ?>>CLP - Chile Peso</option>
									<option value="CNY" <?php if($row['currency_from'] == "CNY") { echo 'selected'; } ?>>CNY - China Yuan Renminbi</option>
									<option value="COP" <?php if($row['currency_from'] == "COP") { echo 'selected'; } ?>>COP - Colombia Peso</option>
									<option value="CRC" <?php if($row['currency_from'] == "CRC") { echo 'selected'; } ?>>CRC - Costa Rica Colon</option>
									<option value="CUC" <?php if($row['currency_from'] == "CUC") { echo 'selected'; } ?>>CUC - Cuba Convertible Peso</option>
									<option value="CUP" <?php if($row['currency_from'] == "CUP") { echo 'selected'; } ?>>CUP - Cuba Peso</option>
									<option value="CVE" <?php if($row['currency_from'] == "CVE") { echo 'selected'; } ?>>CVE - Cape Verde Escudo</option>
									<option value="CZK" <?php if($row['currency_from'] == "CZK") { echo 'selected'; } ?>>CZK - Czech Republic Koruna</option>
									<option value="DJF" <?php if($row['currency_from'] == "DJF") { echo 'selected'; } ?>>DJF - Djibouti Franc</option>
									<option value="DKK" <?php if($row['currency_from'] == "DKK") { echo 'selected'; } ?>>DKK - Denmark Krone</option>
									<option value="DOP" <?php if($row['currency_from'] == "DOP") { echo 'selected'; } ?>>DOP - Dominican Republic Peso</option>
									<option value="DZD" <?php if($row['currency_from'] == "DZD") { echo 'selected'; } ?>>DZD - Algeria Dinar</option>
									<option value="EGP" <?php if($row['currency_from'] == "EGP") { echo 'selected'; } ?>>EGP - Egypt Pound</option>
									<option value="ERN" <?php if($row['currency_from'] == "ERN") { echo 'selected'; } ?>>ERN - Eritrea Nakfa</option>
									<option value="ETB" <?php if($row['currency_from'] == "ETB") { echo 'selected'; } ?>>ETB - Ethiopia Birr</option>
									<option value="EUR" <?php if($row['currency_from'] == "EUR") { echo 'selected'; } ?>>EUR - Euro Member Countries</option>
									<option value="FJD" <?php if($row['currency_from'] == "FJD") { echo 'selected'; } ?>>FJD - Fiji Dollar</option>
									<option value="FKP" <?php if($row['currency_from'] == "FKP") { echo 'selected'; } ?>>FKP - Falkland Islands (Malvinas) Pound</option>
									<option value="GBP" <?php if($row['currency_from'] == "GBP") { echo 'selected'; } ?>>GBP - United Kingdom Pound</option>
									<option value="GEL" <?php if($row['currency_from'] == "GEL") { echo 'selected'; } ?>>GEL - Georgia Lari</option>
									<option value="GGP" <?php if($row['currency_from'] == "GGP") { echo 'selected'; } ?>>GGP - Guernsey Pound</option>
									<option value="GHS" <?php if($row['currency_from'] == "GHS") { echo 'selected'; } ?>>GHS - Ghana Cedi</option>
									<option value="GIP" <?php if($row['currency_from'] == "GIP") { echo 'selected'; } ?>>GIP - Gibraltar Pound</option>
									<option value="GMD" <?php if($row['currency_from'] == "GMD") { echo 'selected'; } ?>>GMD - Gambia Dalasi</option>
									<option value="GNF" <?php if($row['currency_from'] == "GNF") { echo 'selected'; } ?>>GNF - Guinea Franc</option>
									<option value="GTQ" <?php if($row['currency_from'] == "GTQ") { echo 'selected'; } ?>>GTQ - Guatemala Quetzal</option>
									<option value="GYD" <?php if($row['currency_from'] == "GYD") { echo 'selected'; } ?>>GYD - Guyana Dollar</option>
									<option value="HKD" <?php if($row['currency_from'] == "HKD") { echo 'selected'; } ?>>HKD - Hong Kong Dollar</option>
									<option value="HNL" <?php if($row['currency_from'] == "HNL") { echo 'selected'; } ?>>HNL - Honduras Lempira</option>
									<option value="HPK" <?php if($row['currency_from'] == "HPK") { echo 'selected'; } ?>>HRK - Croatia Kuna</option>
									<option value="HTG" <?php if($row['currency_from'] == "HTG") { echo 'selected'; } ?>>HTG - Haiti Gourde</option>
									<option value="HUF" <?php if($row['currency_from'] == "HUF") { echo 'selected'; } ?>>HUF - Hungary Forint</option>
									<option value="IDR" <?php if($row['currency_from'] == "IDR") { echo 'selected'; } ?>>IDR - Indonesia Rupiah</option>
									<option value="ILS" <?php if($row['currency_from'] == "ILS") { echo 'selected'; } ?>>ILS - Israel Shekel</option>
									<option value="IMP" <?php if($row['currency_from'] == "IMP") { echo 'selected'; } ?>>IMP - Isle of Man Pound</option>
									<option value="INR" <?php if($row['currency_from'] == "INR") { echo 'selected'; } ?>>INR - India Rupee</option>
									<option value="IQD" <?php if($row['currency_from'] == "IQD") { echo 'selected'; } ?>>IQD - Iraq Dinar</option>
									<option value="IRR" <?php if($row['currency_from'] == "IRR") { echo 'selected'; } ?>>IRR - Iran Rial</option>
									<option value="ISK" <?php if($row['currency_from'] == "ISK") { echo 'selected'; } ?>>ISK - Iceland Krona</option>
									<option value="JEP" <?php if($row['currency_from'] == "JEP") { echo 'selected'; } ?>>JEP - Jersey Pound</option>
									<option value="JMD" <?php if($row['currency_from'] == "JMD") { echo 'selected'; } ?>>JMD - Jamaica Dollar</option>
									<option value="JOD" <?php if($row['currency_from'] == "JOD") { echo 'selected'; } ?>>JOD - Jordan Dinar</option>
									<option value="JPY" <?php if($row['currency_from'] == "JPY") { echo 'selected'; } ?>>JPY - Japan Yen</option>
									<option value="KES" <?php if($row['currency_from'] == "KES") { echo 'selected'; } ?>>KES - Kenya Shilling</option>
									<option value="KGS" <?php if($row['currency_from'] == "KGS") { echo 'selected'; } ?>>KGS - Kyrgyzstan Som</option>
									<option value="KHR" <?php if($row['currency_from'] == "KHR") { echo 'selected'; } ?>>KHR - Cambodia Riel</option>
									<option value="KMF" <?php if($row['currency_from'] == "KMF") { echo 'selected'; } ?>>KMF - Comoros Franc</option>
									<option value="KPW" <?php if($row['currency_from'] == "KPW") { echo 'selected'; } ?>>KPW - Korea (North) Won</option>
									<option value="KRW" <?php if($row['currency_from'] == "KRW") { echo 'selected'; } ?>>KRW - Korea (South) Won</option>
									<option value="KWD" <?php if($row['currency_from'] == "KWD") { echo 'selected'; } ?>>KWD - Kuwait Dinar</option>
									<option value="KYD" <?php if($row['currency_from'] == "KYD") { echo 'selected'; } ?>>KYD - Cayman Islands Dollar</option>
									<option value="KZT" <?php if($row['currency_from'] == "KZT") { echo 'selected'; } ?>>KZT - Kazakhstan Tenge</option>
									<option value="LAK" <?php if($row['currency_from'] == "LAK") { echo 'selected'; } ?>>LAK - Laos Kip</option>
									<option value="LBP" <?php if($row['currency_from'] == "LBP") { echo 'selected'; } ?>>LBP - Lebanon Pound</option>
									<option value="LKR" <?php if($row['currency_from'] == "LKR") { echo 'selected'; } ?>>LKR - Sri Lanka Rupee</option>
									<option value="LRD" <?php if($row['currency_from'] == "LRD") { echo 'selected'; } ?>>LRD - Liberia Dollar</option>
									<option value="LSL" <?php if($row['currency_from'] == "LSL") { echo 'selected'; } ?>>LSL - Lesotho Loti</option>
									<option value="LYD" <?php if($row['currency_from'] == "LYD") { echo 'selected'; } ?>>LYD - Libya Dinar</option>
									<option value="MAD" <?php if($row['currency_from'] == "MAD") { echo 'selected'; } ?>>MAD - Morocco Dirham</option>
									<option value="MDL" <?php if($row['currency_from'] == "MDL") { echo 'selected'; } ?>>MDL - Moldova Leu</option>
									<option value="MGA" <?php if($row['currency_from'] == "MGA") { echo 'selected'; } ?>>MGA - Madagascar Ariary</option>
									<option value="MKD" <?php if($row['currency_from'] == "MKD") { echo 'selected'; } ?>>MKD - Macedonia Denar</option>
									<option value="MMK" <?php if($row['currency_from'] == "MMK") { echo 'selected'; } ?>>MMK - Myanmar (Burma) Kyat</option>
									<option value="MNT" <?php if($row['currency_from'] == "MNT") { echo 'selected'; } ?>>MNT - Mongolia Tughrik</option>
									<option value="MOP" <?php if($row['currency_from'] == "MOP") { echo 'selected'; } ?>>MOP - Macau Pataca</option>
									<option value="MRO" <?php if($row['currency_from'] == "MRO") { echo 'selected'; } ?>>MRO - Mauritania Ouguiya</option>
									<option value="MUR" <?php if($row['currency_from'] == "MUR") { echo 'selected'; } ?>>MUR - Mauritius Rupee</option>
									<option value="MVR" <?php if($row['currency_from'] == "MVR") { echo 'selected'; } ?>>MVR - Maldives (Maldive Islands) Rufiyaa</option>
									<option value="MWK" <?php if($row['currency_from'] == "MWK") { echo 'selected'; } ?>>MWK - Malawi Kwacha</option>
									<option value="MXN" <?php if($row['currency_from'] == "MXN") { echo 'selected'; } ?>>MXN - Mexico Peso</option>
									<option value="MYR" <?php if($row['currency_from'] == "MYR") { echo 'selected'; } ?>>MYR - Malaysia Ringgit</option>
									<option value="MZN" <?php if($row['currency_from'] == "MZN") { echo 'selected'; } ?>>MZN - Mozambique Metical</option>
									<option value="NAD" <?php if($row['currency_from'] == "NAD") { echo 'selected'; } ?>>NAD - Namibia Dollar</option>
									<option value="NGN" <?php if($row['currency_from'] == "NGN") { echo 'selected'; } ?>>NGN - Nigeria Naira</option>
									<option value="NTO" <?php if($row['currency_from'] == "NTO") { echo 'selected'; } ?>>NIO - Nicaragua Cordoba</option>
									<option value="NOK" <?php if($row['currency_from'] == "NOK") { echo 'selected'; } ?>>NOK - Norway Krone</option>
									<option value="NPR" <?php if($row['currency_from'] == "NPR") { echo 'selected'; } ?>>NPR - Nepal Rupee</option>
									<option value="NZD" <?php if($row['currency_from'] == "NZD") { echo 'selected'; } ?>>NZD - New Zealand Dollar</option>
									<option value="OMR" <?php if($row['currency_from'] == "OMR") { echo 'selected'; } ?>>OMR - Oman Rial</option>
									<option value="PAB" <?php if($row['currency_from'] == "PAB") { echo 'selected'; } ?>>PAB - Panama Balboa</option>
									<option value="PEN" <?php if($row['currency_from'] == "PEN") { echo 'selected'; } ?>>PEN - Peru Nuevo Sol</option>
									<option value="PGK" <?php if($row['currency_from'] == "PGK") { echo 'selected'; } ?>>PGK - Papua New Guinea Kina</option>
									<option value="PHP" <?php if($row['currency_from'] == "PHP") { echo 'selected'; } ?>>PHP - Philippines Peso</option>
									<option value="PKR" <?php if($row['currency_from'] == "PKR") { echo 'selected'; } ?>>PKR - Pakistan Rupee</option>
									<option value="PLN" <?php if($row['currency_from'] == "PLN") { echo 'selected'; } ?>>PLN - Poland Zloty</option>
									<option value="PYG" <?php if($row['currency_from'] == "PYG") { echo 'selected'; } ?>>PYG - Paraguay Guarani</option>
									<option value="QAR" <?php if($row['currency_from'] == "QAR") { echo 'selected'; } ?>>QAR - Qatar Riyal</option>
									<option value="RON" <?php if($row['currency_from'] == "RON") { echo 'selected'; } ?>>RON - Romania New Leu</option>
									<option value="RSD" <?php if($row['currency_from'] == "RSD") { echo 'selected'; } ?>>RSD - Serbia Dinar</option>
									<option value="RUB" <?php if($row['currency_from'] == "RUB") { echo 'selected'; } ?>>RUB - Russia Ruble</option>
									<option value="RWF" <?php if($row['currency_from'] == "RWF") { echo 'selected'; } ?>>RWF - Rwanda Franc</option>
									<option value="SAR" <?php if($row['currency_from'] == "SAR") { echo 'selected'; } ?>>SAR - Saudi Arabia Riyal</option>
									<option value="SBD" <?php if($row['currency_from'] == "SBD") { echo 'selected'; } ?>>SBD - Solomon Islands Dollar</option>
									<option value="SCR" <?php if($row['currency_from'] == "SCR") { echo 'selected'; } ?>>SCR - Seychelles Rupee</option>
									<option value="SDG" <?php if($row['currency_from'] == "SDG") { echo 'selected'; } ?>>SDG - Sudan Pound</option>
									<option value="SEK" <?php if($row['currency_from'] == "SEK") { echo 'selected'; } ?>>SEK - Sweden Krona</option>
									<option value="SGD" <?php if($row['currency_from'] == "SGD") { echo 'selected'; } ?>>SGD - Singapore Dollar</option>
									<option value="SHP" <?php if($row['currency_from'] == "SHP") { echo 'selected'; } ?>>SHP - Saint Helena Pound</option>
									<option value="SLL" <?php if($row['currency_from'] == "SLL") { echo 'selected'; } ?>>SLL - Sierra Leone Leone</option>
									<option value="SOS" <?php if($row['currency_from'] == "SOS") { echo 'selected'; } ?>>SOS - Somalia Shilling</option>
									<option value="SRL" <?php if($row['currency_from'] == "SRL") { echo 'selected'; } ?>>SPL* - Seborga Luigino</option>
									<option value="SRD" <?php if($row['currency_from'] == "SRD") { echo 'selected'; } ?>>SRD - Suriname Dollar</option>
									<option value="STD" <?php if($row['currency_from'] == "STD") { echo 'selected'; } ?>>STD - Sao Tome and Principe Dobra</option>
									<option value="SVC" <?php if($row['currency_from'] == "SVC") { echo 'selected'; } ?>>SVC - El Salvador Colon</option>
									<option value="SYP" <?php if($row['currency_from'] == "SYP") { echo 'selected'; } ?>>SYP - Syria Pound</option>
									<option value="SZL" <?php if($row['currency_from'] == "SZL") { echo 'selected'; } ?>>SZL - Swaziland Lilangeni</option>
									<option value="THB" <?php if($row['currency_from'] == "THB") { echo 'selected'; } ?>>THB - Thailand Baht</option>
									<option value="TJS" <?php if($row['currency_from'] == "TJS") { echo 'selected'; } ?>>TJS - Tajikistan Somoni</option>
									<option value="TMT" <?php if($row['currency_from'] == "TMT") { echo 'selected'; } ?>>TMT - Turkmenistan Manat</option>
									<option value="TND" <?php if($row['currency_from'] == "TND") { echo 'selected'; } ?>>TND - Tunisia Dinar</option>
									<option value="TOP" <?php if($row['currency_from'] == "TOP") { echo 'selected'; } ?>>TOP - Tonga Pa'anga</option>
									<option value="TRY" <?php if($row['currency_from'] == "TRY") { echo 'selected'; } ?>>TRY - Turkey Lira</option>
									<option value="TTD" <?php if($row['currency_from'] == "TTD") { echo 'selected'; } ?>>TTD - Trinidad and Tobago Dollar</option>
									<option value="TVD" <?php if($row['currency_from'] == "TVD") { echo 'selected'; } ?>>TVD - Tuvalu Dollar</option>
									<option value="TWD" <?php if($row['currency_from'] == "TWD") { echo 'selected'; } ?>>TWD - Taiwan New Dollar</option>
									<option value="TZS" <?php if($row['currency_from'] == "TZS") { echo 'selected'; } ?>>TZS - Tanzania Shilling</option>
									<option value="UAH" <?php if($row['currency_from'] == "UAH") { echo 'selected'; } ?>>UAH - Ukraine Hryvnia</option>
									<option value="UGX" <?php if($row['currency_from'] == "UGX") { echo 'selected'; } ?>>UGX - Uganda Shilling</option>
									<option value="USD" <?php if($row['currency_from'] == "USD") { echo 'selected'; } ?>>USD - United States Dollar</option>
									<option value="UYU" <?php if($row['currency_from'] == "UYU") { echo 'selected'; } ?>>UYU - Uruguay Peso</option>
									<option value="UZS" <?php if($row['currency_from'] == "UZS") { echo 'selected'; } ?>>UZS - Uzbekistan Som</option>
									<option value="VEF" <?php if($row['currency_from'] == "VEF") { echo 'selected'; } ?>>VEF - Venezuela Bolivar</option>
									<option value="VND" <?php if($row['currency_from'] == "VND") { echo 'selected'; } ?>>VND - Viet Nam Dong</option>
									<option value="VUV" <?php if($row['currency_from'] == "VUV") { echo 'selected'; } ?>>VUV - Vanuatu Vatu</option>
									<option value="WST" <?php if($row['currency_from'] == "WST") { echo 'selected'; } ?>>WST - Samoa Tala</option>
									<option value="XAF" <?php if($row['currency_from'] == "XAF") { echo 'selected'; } ?>>XAF - Communaute Financiere Africaine (BEAC) CFA Franc BEAC</option>
									<option value="XCD" <?php if($row['currency_from'] == "XCD") { echo 'selected'; } ?>>XCD - East Caribbean Dollar</option>
									<option value="XDR" <?php if($row['currency_from'] == "XDR") { echo 'selected'; } ?>>XDR - International Monetary Fund (IMF) Special Drawing Rights</option>
									<option value="XOF" <?php if($row['currency_from'] == "XOF") { echo 'selected'; } ?>>XOF - Communaute Financiere Africaine (BCEAO) Franc</option>
									<option value="XPF" <?php if($row['currency_from'] == "XPF") { echo 'selected'; } ?>>XPF - Comptoirs Francais du Pacifique (CFP) Franc</option>
									<option value="YER" <?php if($row['currency_from'] == "YER") { echo 'selected'; } ?>>YER - Yemen Rial</option>
									<option value="ZAR" <?php if($row['currency_from'] == "ZAR") { echo 'selected'; } ?>>ZAR - South Africa Rand</option>
									<option value="ZMW" <?php if($row['currency_from'] == "ZMW") { echo 'selected'; } ?>>ZMW - Zambia Kwacha</option>
									<option value="ZWD" <?php if($row['currency_from'] == "ZWD") { echo 'selected'; } ?>>ZWD - Zimbabwe Dollar</option>
									<?php } ?>
								</select>
							</div>
							<div class="form-group">
								<label>To currency</label>
								<select class="form-control" name="currency_to" onchange="changeCur2(this.value);">
									<option value="AED" <?php if($row['currency_to'] == "AED") { echo 'selected'; } ?>>AED - United Arab Emirates Dirham</option>
									<option value="AFN" <?php if($row['currency_to'] == "AFN") { echo 'selected'; } ?>>AFN - Afghanistan Afghani</option>
									<option value="ALL" <?php if($row['currency_to'] == "ALL") { echo 'selected'; } ?>>ALL - Albania Lek</option>
									<option value="AMD" <?php if($row['currency_to'] == "AMD") { echo 'selected'; } ?>>AMD - Armenia Dram</option>
									<option value="ANG" <?php if($row['currency_to'] == "ANG") { echo 'selected'; } ?>>ANG - Netherlands Antilles Guilder</option>
									<option value="AOA" <?php if($row['currency_to'] == "AOA") { echo 'selected'; } ?>>AOA - Angola Kwanza</option>
									<option value="ARS" <?php if($row['currency_to'] == "ARS") { echo 'selected'; } ?>>ARS - Argentina Peso</option>
									<option value="AUD" <?php if($row['currency_to'] == "AUD") { echo 'selected'; } ?>>AUD - Australia Dollar</option>
									<option value="AWG" <?php if($row['currency_to'] == "AWG") { echo 'selected'; } ?>>AWG - Aruba Guilder</option>
									<option value="AZN" <?php if($row['currency_to'] == "AZN") { echo 'selected'; } ?>>AZN - Azerbaijan New Manat</option>
									<option value="BTC" <?php if($row['currency_to'] == "BTC") { echo 'selected'; } ?>>BTC - Bitcoin</option>
									<option value="BAM" <?php if($row['currency_to'] == "BAM") { echo 'selected'; } ?>>BAM - Bosnia and Herzegovina Convertible Marka</option>
									<option value="BBD" <?php if($row['currency_to'] == "BBD") { echo 'selected'; } ?>>BBD - Barbados Dollar</option>
									<option value="BDT" <?php if($row['currency_to'] == "BDT") { echo 'selected'; } ?>>BDT - Bangladesh Taka</option>
									<option value="BGN" <?php if($row['currency_to'] == "BGN") { echo 'selected'; } ?>>BGN - Bulgaria Lev</option>
									<option value="BHD" <?php if($row['currency_to'] == "BHD") { echo 'selected'; } ?>>BHD - Bahrain Dinar</option>
									<option value="BIF" <?php if($row['currency_to'] == "BIF") { echo 'selected'; } ?>>BIF - Burundi Franc</option>
									<option value="BMD" <?php if($row['currency_to'] == "BMD") { echo 'selected'; } ?>>BMD - Bermuda Dollar</option>
									<option value="BND" <?php if($row['currency_to'] == "BND") { echo 'selected'; } ?>>BND - Brunei Darussalam Dollar</option>
									<option value="BOB" <?php if($row['currency_to'] == "BOB") { echo 'selected'; } ?>>BOB - Bolivia Boliviano</option>
									<option value="BRL" <?php if($row['currency_to'] == "BRL") { echo 'selected'; } ?>>BRL - Brazil Real</option>
									<option value="BSD" <?php if($row['currency_to'] == "BSD") { echo 'selected'; } ?>>BSD - Bahamas Dollar</option>
									<option value="BTN" <?php if($row['currency_to'] == "BTN") { echo 'selected'; } ?>>BTN - Bhutan Ngultrum</option>
									<option value="BWP" <?php if($row['currency_to'] == "BWP") { echo 'selected'; } ?>>BWP - Botswana Pula</option>
									<option value="BYR" <?php if($row['currency_to'] == "BYR") { echo 'selected'; } ?>>BYR - Belarus Ruble</option>
									<option value="BZD" <?php if($row['currency_to'] == "BZD") { echo 'selected'; } ?>>BZD - Belize Dollar</option>
									<option value="CAD" <?php if($row['currency_to'] == "CAD") { echo 'selected'; } ?>>CAD - Canada Dollar</option>
									<option value="CDF" <?php if($row['currency_to'] == "CDF") { echo 'selected'; } ?>>CDF - Congo/Kinshasa Franc</option>
									<option value="CHF" <?php if($row['currency_to'] == "CHF") { echo 'selected'; } ?>>CHF - Switzerland Franc</option>
									<option value="CLP" <?php if($row['currency_to'] == "CLP") { echo 'selected'; } ?>>CLP - Chile Peso</option>
									<option value="CNY" <?php if($row['currency_to'] == "CNY") { echo 'selected'; } ?>>CNY - China Yuan Renminbi</option>
									<option value="COP" <?php if($row['currency_to'] == "COP") { echo 'selected'; } ?>>COP - Colombia Peso</option>
									<option value="CRC" <?php if($row['currency_to'] == "CRC") { echo 'selected'; } ?>>CRC - Costa Rica Colon</option>
									<option value="CUC" <?php if($row['currency_to'] == "CUC") { echo 'selected'; } ?>>CUC - Cuba Convertible Peso</option>
									<option value="CUP" <?php if($row['currency_to'] == "CUP") { echo 'selected'; } ?>>CUP - Cuba Peso</option>
									<option value="CVE" <?php if($row['currency_to'] == "CVE") { echo 'selected'; } ?>>CVE - Cape Verde Escudo</option>
									<option value="CZK" <?php if($row['currency_to'] == "CZK") { echo 'selected'; } ?>>CZK - Czech Republic Koruna</option>
									<option value="DJF" <?php if($row['currency_to'] == "DJF") { echo 'selected'; } ?>>DJF - Djibouti Franc</option>
									<option value="DKK" <?php if($row['currency_to'] == "DKK") { echo 'selected'; } ?>>DKK - Denmark Krone</option>
									<option value="DOP" <?php if($row['currency_to'] == "DOP") { echo 'selected'; } ?>>DOP - Dominican Republic Peso</option>
									<option value="DZD" <?php if($row['currency_to'] == "DZD") { echo 'selected'; } ?>>DZD - Algeria Dinar</option>
									<option value="EGP" <?php if($row['currency_to'] == "EGP") { echo 'selected'; } ?>>EGP - Egypt Pound</option>
									<option value="ERN" <?php if($row['currency_to'] == "ERN") { echo 'selected'; } ?>>ERN - Eritrea Nakfa</option>
									<option value="ETB" <?php if($row['currency_to'] == "ETB") { echo 'selected'; } ?>>ETB - Ethiopia Birr</option>
									<option value="EUR" <?php if($row['currency_to'] == "EUR") { echo 'selected'; } ?>>EUR - Euro Member Countries</option>
									<option value="FJD" <?php if($row['currency_to'] == "FJD") { echo 'selected'; } ?>>FJD - Fiji Dollar</option>
									<option value="FKP" <?php if($row['currency_to'] == "FKP") { echo 'selected'; } ?>>FKP - Falkland Islands (Malvinas) Pound</option>
									<option value="GBP" <?php if($row['currency_to'] == "GBP") { echo 'selected'; } ?>>GBP - United Kingdom Pound</option>
									<option value="GEL" <?php if($row['currency_to'] == "GEL") { echo 'selected'; } ?>>GEL - Georgia Lari</option>
									<option value="GGP" <?php if($row['currency_to'] == "GGP") { echo 'selected'; } ?>>GGP - Guernsey Pound</option>
									<option value="GHS" <?php if($row['currency_to'] == "GHS") { echo 'selected'; } ?>>GHS - Ghana Cedi</option>
									<option value="GIP" <?php if($row['currency_to'] == "GIP") { echo 'selected'; } ?>>GIP - Gibraltar Pound</option>
									<option value="GMD" <?php if($row['currency_to'] == "GMD") { echo 'selected'; } ?>>GMD - Gambia Dalasi</option>
									<option value="GNF" <?php if($row['currency_to'] == "GNF") { echo 'selected'; } ?>>GNF - Guinea Franc</option>
									<option value="GTQ" <?php if($row['currency_to'] == "GTQ") { echo 'selected'; } ?>>GTQ - Guatemala Quetzal</option>
									<option value="GYD" <?php if($row['currency_to'] == "GYD") { echo 'selected'; } ?>>GYD - Guyana Dollar</option>
									<option value="HKD" <?php if($row['currency_to'] == "HKD") { echo 'selected'; } ?>>HKD - Hong Kong Dollar</option>
									<option value="HNL" <?php if($row['currency_to'] == "HNL") { echo 'selected'; } ?>>HNL - Honduras Lempira</option>
									<option value="HPK" <?php if($row['currency_to'] == "HPK") { echo 'selected'; } ?>>HRK - Croatia Kuna</option>
									<option value="HTG" <?php if($row['currency_to'] == "HTG") { echo 'selected'; } ?>>HTG - Haiti Gourde</option>
									<option value="HUF" <?php if($row['currency_to'] == "HUF") { echo 'selected'; } ?>>HUF - Hungary Forint</option>
									<option value="IDR" <?php if($row['currency_to'] == "IDR") { echo 'selected'; } ?>>IDR - Indonesia Rupiah</option>
									<option value="ILS" <?php if($row['currency_to'] == "ILS") { echo 'selected'; } ?>>ILS - Israel Shekel</option>
									<option value="IMP" <?php if($row['currency_to'] == "IMP") { echo 'selected'; } ?>>IMP - Isle of Man Pound</option>
									<option value="INR" <?php if($row['currency_to'] == "INR") { echo 'selected'; } ?>>INR - India Rupee</option>
									<option value="IQD" <?php if($row['currency_to'] == "IQD") { echo 'selected'; } ?>>IQD - Iraq Dinar</option>
									<option value="IRR" <?php if($row['currency_to'] == "IRR") { echo 'selected'; } ?>>IRR - Iran Rial</option>
									<option value="ISK" <?php if($row['currency_to'] == "ISK") { echo 'selected'; } ?>>ISK - Iceland Krona</option>
									<option value="JEP" <?php if($row['currency_to'] == "JEP") { echo 'selected'; } ?>>JEP - Jersey Pound</option>
									<option value="JMD" <?php if($row['currency_to'] == "JMD") { echo 'selected'; } ?>>JMD - Jamaica Dollar</option>
									<option value="JOD" <?php if($row['currency_to'] == "JOD") { echo 'selected'; } ?>>JOD - Jordan Dinar</option>
									<option value="JPY" <?php if($row['currency_to'] == "JPY") { echo 'selected'; } ?>>JPY - Japan Yen</option>
									<option value="KES" <?php if($row['currency_to'] == "KES") { echo 'selected'; } ?>>KES - Kenya Shilling</option>
									<option value="KGS" <?php if($row['currency_to'] == "KGS") { echo 'selected'; } ?>>KGS - Kyrgyzstan Som</option>
									<option value="KHR" <?php if($row['currency_to'] == "KHR") { echo 'selected'; } ?>>KHR - Cambodia Riel</option>
									<option value="KMF" <?php if($row['currency_to'] == "KMF") { echo 'selected'; } ?>>KMF - Comoros Franc</option>
									<option value="KPW" <?php if($row['currency_to'] == "KPW") { echo 'selected'; } ?>>KPW - Korea (North) Won</option>
									<option value="KRW" <?php if($row['currency_to'] == "KRW") { echo 'selected'; } ?>>KRW - Korea (South) Won</option>
									<option value="KWD" <?php if($row['currency_to'] == "KWD") { echo 'selected'; } ?>>KWD - Kuwait Dinar</option>
									<option value="KYD" <?php if($row['currency_to'] == "KYD") { echo 'selected'; } ?>>KYD - Cayman Islands Dollar</option>
									<option value="KZT" <?php if($row['currency_to'] == "KZT") { echo 'selected'; } ?>>KZT - Kazakhstan Tenge</option>
									<option value="LAK" <?php if($row['currency_to'] == "LAK") { echo 'selected'; } ?>>LAK - Laos Kip</option>
									<option value="LBP" <?php if($row['currency_to'] == "LBP") { echo 'selected'; } ?>>LBP - Lebanon Pound</option>
									<option value="LKR" <?php if($row['currency_to'] == "LKR") { echo 'selected'; } ?>>LKR - Sri Lanka Rupee</option>
									<option value="LRD" <?php if($row['currency_to'] == "LRD") { echo 'selected'; } ?>>LRD - Liberia Dollar</option>
									<option value="LSL" <?php if($row['currency_to'] == "LSL") { echo 'selected'; } ?>>LSL - Lesotho Loti</option>
									<option value="LYD" <?php if($row['currency_to'] == "LYD") { echo 'selected'; } ?>>LYD - Libya Dinar</option>
									<option value="MAD" <?php if($row['currency_to'] == "MAD") { echo 'selected'; } ?>>MAD - Morocco Dirham</option>
									<option value="MDL" <?php if($row['currency_to'] == "MDL") { echo 'selected'; } ?>>MDL - Moldova Leu</option>
									<option value="MGA" <?php if($row['currency_to'] == "MGA") { echo 'selected'; } ?>>MGA - Madagascar Ariary</option>
									<option value="MKD" <?php if($row['currency_to'] == "MKD") { echo 'selected'; } ?>>MKD - Macedonia Denar</option>
									<option value="MMK" <?php if($row['currency_to'] == "MMK") { echo 'selected'; } ?>>MMK - Myanmar (Burma) Kyat</option>
									<option value="MNT" <?php if($row['currency_to'] == "MNT") { echo 'selected'; } ?>>MNT - Mongolia Tughrik</option>
									<option value="MOP" <?php if($row['currency_to'] == "MOP") { echo 'selected'; } ?>>MOP - Macau Pataca</option>
									<option value="MRO" <?php if($row['currency_to'] == "MRO") { echo 'selected'; } ?>>MRO - Mauritania Ouguiya</option>
									<option value="MUR" <?php if($row['currency_to'] == "MUR") { echo 'selected'; } ?>>MUR - Mauritius Rupee</option>
									<option value="MVR" <?php if($row['currency_to'] == "MVR") { echo 'selected'; } ?>>MVR - Maldives (Maldive Islands) Rufiyaa</option>
									<option value="MWK" <?php if($row['currency_to'] == "MWK") { echo 'selected'; } ?>>MWK - Malawi Kwacha</option>
									<option value="MXN" <?php if($row['currency_to'] == "MXN") { echo 'selected'; } ?>>MXN - Mexico Peso</option>
									<option value="MYR" <?php if($row['currency_to'] == "MYR") { echo 'selected'; } ?>>MYR - Malaysia Ringgit</option>
									<option value="MZN" <?php if($row['currency_to'] == "MZN") { echo 'selected'; } ?>>MZN - Mozambique Metical</option>
									<option value="NAD" <?php if($row['currency_to'] == "NAD") { echo 'selected'; } ?>>NAD - Namibia Dollar</option>
									<option value="NGN" <?php if($row['currency_to'] == "NGN") { echo 'selected'; } ?>>NGN - Nigeria Naira</option>
									<option value="NTO" <?php if($row['currency_to'] == "NTO") { echo 'selected'; } ?>>NIO - Nicaragua Cordoba</option>
									<option value="NOK" <?php if($row['currency_to'] == "NOK") { echo 'selected'; } ?>>NOK - Norway Krone</option>
									<option value="NPR" <?php if($row['currency_to'] == "NPR") { echo 'selected'; } ?>>NPR - Nepal Rupee</option>
									<option value="NZD" <?php if($row['currency_to'] == "NZD") { echo 'selected'; } ?>>NZD - New Zealand Dollar</option>
									<option value="OMR" <?php if($row['currency_to'] == "OMR") { echo 'selected'; } ?>>OMR - Oman Rial</option>
									<option value="PAB" <?php if($row['currency_to'] == "PAB") { echo 'selected'; } ?>>PAB - Panama Balboa</option>
									<option value="PEN" <?php if($row['currency_to'] == "PEN") { echo 'selected'; } ?>>PEN - Peru Nuevo Sol</option>
									<option value="PGK" <?php if($row['currency_to'] == "PGK") { echo 'selected'; } ?>>PGK - Papua New Guinea Kina</option>
									<option value="PHP" <?php if($row['currency_to'] == "PHP") { echo 'selected'; } ?>>PHP - Philippines Peso</option>
									<option value="PKR" <?php if($row['currency_to'] == "PKR") { echo 'selected'; } ?>>PKR - Pakistan Rupee</option>
									<option value="PLN" <?php if($row['currency_to'] == "PLN") { echo 'selected'; } ?>>PLN - Poland Zloty</option>
									<option value="PYG" <?php if($row['currency_to'] == "PYG") { echo 'selected'; } ?>>PYG - Paraguay Guarani</option>
									<option value="QAR" <?php if($row['currency_to'] == "QAR") { echo 'selected'; } ?>>QAR - Qatar Riyal</option>
									<option value="RON" <?php if($row['currency_to'] == "RON") { echo 'selected'; } ?>>RON - Romania New Leu</option>
									<option value="RSD" <?php if($row['currency_to'] == "RSD") { echo 'selected'; } ?>>RSD - Serbia Dinar</option>
									<option value="RUB" <?php if($row['currency_to'] == "RUB") { echo 'selected'; } ?>>RUB - Russia Ruble</option>
									<option value="RWF" <?php if($row['currency_to'] == "RWF") { echo 'selected'; } ?>>RWF - Rwanda Franc</option>
									<option value="SAR" <?php if($row['currency_to'] == "SAR") { echo 'selected'; } ?>>SAR - Saudi Arabia Riyal</option>
									<option value="SBD" <?php if($row['currency_to'] == "SBD") { echo 'selected'; } ?>>SBD - Solomon Islands Dollar</option>
									<option value="SCR" <?php if($row['currency_to'] == "SCR") { echo 'selected'; } ?>>SCR - Seychelles Rupee</option>
									<option value="SDG" <?php if($row['currency_to'] == "SDG") { echo 'selected'; } ?>>SDG - Sudan Pound</option>
									<option value="SEK" <?php if($row['currency_to'] == "SEK") { echo 'selected'; } ?>>SEK - Sweden Krona</option>
									<option value="SGD" <?php if($row['currency_to'] == "SGD") { echo 'selected'; } ?>>SGD - Singapore Dollar</option>
									<option value="SHP" <?php if($row['currency_to'] == "SHP") { echo 'selected'; } ?>>SHP - Saint Helena Pound</option>
									<option value="SLL" <?php if($row['currency_to'] == "SLL") { echo 'selected'; } ?>>SLL - Sierra Leone Leone</option>
									<option value="SOS" <?php if($row['currency_to'] == "SOS") { echo 'selected'; } ?>>SOS - Somalia Shilling</option>
									<option value="SRL" <?php if($row['currency_to'] == "SRL") { echo 'selected'; } ?>>SPL* - Seborga Luigino</option>
									<option value="SRD" <?php if($row['currency_to'] == "SRD") { echo 'selected'; } ?>>SRD - Suriname Dollar</option>
									<option value="STD" <?php if($row['currency_to'] == "STD") { echo 'selected'; } ?>>STD - Sao Tome and Principe Dobra</option>
									<option value="SVC" <?php if($row['currency_to'] == "SVC") { echo 'selected'; } ?>>SVC - El Salvador Colon</option>
									<option value="SYP" <?php if($row['currency_to'] == "SYP") { echo 'selected'; } ?>>SYP - Syria Pound</option>
									<option value="SZL" <?php if($row['currency_to'] == "SZL") { echo 'selected'; } ?>>SZL - Swaziland Lilangeni</option>
									<option value="THB" <?php if($row['currency_to'] == "THB") { echo 'selected'; } ?>>THB - Thailand Baht</option>
									<option value="TJS" <?php if($row['currency_to'] == "TJS") { echo 'selected'; } ?>>TJS - Tajikistan Somoni</option>
									<option value="TMT" <?php if($row['currency_to'] == "TMT") { echo 'selected'; } ?>>TMT - Turkmenistan Manat</option>
									<option value="TND" <?php if($row['currency_to'] == "TND") { echo 'selected'; } ?>>TND - Tunisia Dinar</option>
									<option value="TOP" <?php if($row['currency_to'] == "TOP") { echo 'selected'; } ?>>TOP - Tonga Pa'anga</option>
									<option value="TRY" <?php if($row['currency_to'] == "TRY") { echo 'selected'; } ?>>TRY - Turkey Lira</option>
									<option value="TTD" <?php if($row['currency_to'] == "TTD") { echo 'selected'; } ?>>TTD - Trinidad and Tobago Dollar</option>
									<option value="TVD" <?php if($row['currency_to'] == "TVD") { echo 'selected'; } ?>>TVD - Tuvalu Dollar</option>
									<option value="TWD" <?php if($row['currency_to'] == "TWD") { echo 'selected'; } ?>>TWD - Taiwan New Dollar</option>
									<option value="TZS" <?php if($row['currency_to'] == "TZS") { echo 'selected'; } ?>>TZS - Tanzania Shilling</option>
									<option value="UAH" <?php if($row['currency_to'] == "UAH") { echo 'selected'; } ?>>UAH - Ukraine Hryvnia</option>
									<option value="UGX" <?php if($row['currency_to'] == "UGX") { echo 'selected'; } ?>>UGX - Uganda Shilling</option>
									<option value="USD" <?php if($row['currency_to'] == "USD") { echo 'selected'; } ?>>USD - United States Dollar</option>
									<option value="UYU" <?php if($row['currency_to'] == "UYU") { echo 'selected'; } ?>>UYU - Uruguay Peso</option>
									<option value="UZS" <?php if($row['currency_to'] == "UZS") { echo 'selected'; } ?>>UZS - Uzbekistan Som</option>
									<option value="VEF" <?php if($row['currency_to'] == "VEF") { echo 'selected'; } ?>>VEF - Venezuela Bolivar</option>
									<option value="VND" <?php if($row['currency_to'] == "VND") { echo 'selected'; } ?>>VND - Viet Nam Dong</option>
									<option value="VUV" <?php if($row['currency_to'] == "VUV") { echo 'selected'; } ?>>VUV - Vanuatu Vatu</option>
									<option value="WST" <?php if($row['currency_to'] == "WST") { echo 'selected'; } ?>>WST - Samoa Tala</option>
									<option value="XAF" <?php if($row['currency_to'] == "XAF") { echo 'selected'; } ?>>XAF - Communaute Financiere Africaine (BEAC) CFA Franc BEAC</option>
									<option value="XCD" <?php if($row['currency_to'] == "XCD") { echo 'selected'; } ?>>XCD - East Caribbean Dollar</option>
									<option value="XDR" <?php if($row['currency_to'] == "XDR") { echo 'selected'; } ?>>XDR - International Monetary Fund (IMF) Special Drawing Rights</option>
									<option value="XOF" <?php if($row['currency_to'] == "XOF") { echo 'selected'; } ?>>XOF - Communaute Financiere Africaine (BCEAO) Franc</option>
									<option value="XPF" <?php if($row['currency_to'] == "XPF") { echo 'selected'; } ?>>XPF - Comptoirs Francais du Pacifique (CFP) Franc</option>
									<option value="YER" <?php if($row['currency_to'] == "YER") { echo 'selected'; } ?>>YER - Yemen Rial</option>
									<option value="ZAR" <?php if($row['currency_to'] == "ZAR") { echo 'selected'; } ?>>ZAR - South Africa Rand</option>
									<option value="ZMW" <?php if($row['currency_to'] == "ZMW") { echo 'selected'; } ?>>ZMW - Zambia Kwacha</option>
									<option value="ZWD" <?php if($row['currency_to'] == "ZWD") { echo 'selected'; } ?>>ZWD - Zimbabwe Dollar</option>
								</select>
							</div>
							<div class="form-group">
								<label>Exchange rate</label>
								<div class="input-group">
									<span class="input-group-addon">1 <span id="cur_from"><?php echo $row['currency_from']; ?></span> = </span>
									<input type="text" class="form-control" name="rate" value="<?php echo $row['rate']; ?>" aria-label="Amount">
									<span class="input-group-addon"><span id="cur_to"><?php echo $row['currency_to']; ?></span></span>
								</div>
							</div>
							<button type="submit" class="btn btn-primary" name="btn_save"><i class="fa fa-check"></i> Save Changes</button>
						</form>
					</div>
				</div>
				<?php
			} elseif($c == "delete") {
				$id = protect($_GET['id']);
				$eid = protect($_GET['eid']);
				$query = $db->query("SELECT * FROM currencies WHERE id='$eid'");
				if($query->num_rows==0) { header("Location: ./?a=companies&b=manageCurrenciesList&id=$id"); }
				$row = $query->fetch_assoc();
				$delete = $db->query("DELETE FROM currencies WHERE id='$eid'");
				$redirect = './?a=companies&b=manageCurrenciesList&id='.$id;
				header("Location: $redirect");
			} else {
				$id = protect($_GET['id']);
				$query = $db->query("SELECT * FROM companies WHERE id='$id'");
				if($query->num_rows==0) { header("Location: ./?a=companies"); }
				$row = $query->fetch_assoc();
				?>
				<div class="panel panel-primary">
					<div class="panel-heading">
						Currency list for <?php echo $row['name']; ?>
						<span class="pull-right">
							<a href="./?a=companies&b=manageCurrenciesList&id=<?php echo $row['id']; ?>&c=add" style="color:#fff;"><i class="fa fa-plus"></i> Add currency</a>
						</span>
					</div>
					<div class="panel-body">
						<table class="table table-hover">
							<thead>
								<tr>
									<td width="15%">From</td>
									<td width="15%">To</td>
									<td width="15%">From currency</td>
									<td width="15%">To currency</td>
									<td width="25%">Exchange rate</td>
									<td width="15%">Action</td>
								</tr>
							</thead>
							<tbody>
								<?php
								$currenciesList = $db->query("SELECT * FROM currencies WHERE cid='$row[id]' and company_from='$row[name]' ORDER BY id");
								if($currenciesList->num_rows>0) {
									while($list = $currenciesList->fetch_assoc()) {
										?>
										<tr>
											<td><?php echo $list['company_from']; ?></td>
											<td><?php echo $list['company_to']; ?></td>
											<td><?php echo $list['currency_from']; ?></td>
											<td><?php echo $list['currency_to']; ?></td>
											<td>1 <?php echo $list['currency_from']; ?> = <?php echo $list['rate']." ".$list['currency_to']; ?></td>
											<td>
												<a href="./?a=companies&b=manageCurrenciesList&id=<?php echo $row['id']; ?>&c=edit&eid=<?php echo $list['id']; ?>" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="fa fa-pencil"></i></a>
												<a href="./?a=companies&b=manageCurrenciesList&id=<?php echo $row['id']; ?>&c=delete&eid=<?php echo $list['id']; ?>" data-toggle="tooltip" data-placement="bottom" title="Delete"><i class="fa fa-times"></i></a>
											</td>
										</tr>
										<?php
									}
								} else {
									echo '<tr><td colspan="6">No have currencies for '.$row[name].'.</td></tr>';
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
				<?php
			}
		} elseif($b == "turn_on") {
			$id = protect($_GET['id']);
			$query = $db->query("SELECT * FROM companies WHERE id='$id'");
			if($query->num_rows==0) { header("Location: ./?a=companies"); }
			$update = $db->query("UPDATE companies SET allow_send='1' WHERE id='$id'");
			header("Location: ./?a=companies");
		} elseif($b == "turn_off") {
			$id = protect($_GET['id']);
			$query = $db->query("SELECT * FROM companies WHERE id='$id'");
			if($query->num_rows==0) { header("Location: ./?a=companies"); }
			$update = $db->query("UPDATE companies SET allow_send='0' WHERE id='$id'");
			header("Location: ./?a=companies");
		} elseif($b == "manageReceiveList") {
			if($c == "turn_on") {
				$id = protect($_GET['id']);
				$query = $db->query("SELECT * FROM companies WHERE id='$id'");
				if($query->num_rows==0) { header("Location: ./?a=companies"); }
				$row = $query->fetch_assoc();
				$company = protect($_GET['company']);
				$replace1 = $company;
				$exp = explode("//",$company);
				$replace2 = $exp[1];
				$replace3 = str_replace($replace1,$replace2,$row['receive_list']);
				$update = $db->query("UPDATE companies SET receive_list='$replace3' WHERE id='$row[id]'");
				$redirect = './?a=companies&b=manageReceiveList&id='.$row[id];
				header("Location:$redirect");
			} elseif($c == "turn_off") {
				$id = protect($_GET['id']);
				$query = $db->query("SELECT * FROM companies WHERE id='$id'");
				if($query->num_rows==0) { header("Location: ./?a=companies"); }
				$row = $query->fetch_assoc();
				$company = protect($_GET['company']);
				$replace1 = $company;
				$replace2 = '//'.$company;
				$replace3 = str_replace($replace1,$replace2,$row['receive_list']);
				$update = $db->query("UPDATE companies SET receive_list='$replace3' WHERE id='$row[id]'");
				$redirect = './?a=companies&b=manageReceiveList&id='.$row[id];
				header("Location:$redirect");
			} else {
				$id = protect($_GET['id']);
				$query = $db->query("SELECT * FROM companies WHERE id='$id'");
				if($query->num_rows==0) { header("Location: ./?a=companies"); }
				$row = $query->fetch_assoc();
				?>
				<div class="panel panel-primary">
					<div class="panel-heading">
						Management list of allowed exchange companies
					</div>
					<div class="panel-body">
						<table class="table table-hover">
							<thead>
								<tr>
									<td width="15%">User send</td>
									<td width="15%">User receive</td>
									<td width="15%">Allow user receive</td>
									<td width="40%">Currencies</td>
								</tr>
							</thead>
							<?php
							$list = explode(",",$row['receive_list']);
							foreach($list as $l) {
								?>
								<tr>
									<td><?php echo $row['name']; ?></td>
									<td><?php echo $l; ?></td>
									<td>
										<?php
										if (strpos($l,'//') !== false) {
											echo 'No (<a href="./?a=companies&b=manageReceiveList&id='.$id.'&c=turn_on&company='.$l.'">Turn on</a>)';
										} else {
											echo 'Yes (<a href="./?a=companies&b=manageReceiveList&id='.$id.'&c=turn_off&company='.$l.'">Turn off</a>)';
										}
										?>
									</td>
									<td>
										<?php
										$getCurrencies = $db->query("SELECT * FROM currencies WHERE cid='$row[id]' and company_from='$row[name]' and company_to='$l'");
										if($getCurrencies->num_rows>0) {
											echo $getCurrencies->num_rows;
										} else {
											echo '0';
										}
										?>  (<a href="./?a=companies&b=manageCurrenciesList&id=<?php echo $row['id']; ?>"><i class="fa fa-pencil"></i> Manage</a>)

									</td>
								</tr>
								<?php
							}
							?>
						</thead>
					</table>
				</div>
			</div>
			<?php
		}

	} else {
		?>
		<script type="text/javascript">
			function loadCurrencies(value) {
				var url = "<?php echo $settings['url']; ?>";
				var data_url = url + "admin/requests/loadCurrencies.php?value="+value;
				$.ajax({
					type: "GET",
					url: data_url,
					dataType: "html",
					success: function (data) {
						$("#currency_to_list").html(data);
					}
				});
			}
		</script>
		<div class="panel panel-primary">
			<div class="panel-heading">
				<i class="fa fa-map-signs"></i> Companies
			</div>
			<div class="panel-body">
				<h3>กำหนดอัตราแลกเปลี่ยน</h3>
				<?php
				if(isset($_POST['btn_update'])) {
					$currency_from = protect($_POST['currency_from']);
					$currency_to = protect($_POST['currency_to']);
					$rate = protect($_POST['rate']);
					if(empty($currency_from) or empty($currency_to) or empty($rate)) { echo error("All fields are required."); }
					elseif(!is_numeric($rate)) { echo error("Please enter exchange rate with numbers. Eg: 0.96, 1.69 and etc."); }
					else {
						$update = $db->query("UPDATE currencies SET rate='$rate' WHERE currency_from='$currency_from' and currency_to='$currency_to'");
						echo success("Rate from $currency_from to $currency_to now is $rate.");
					}
				}
				?>
				<form action="" method="POST">
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label>Currency from</label>
								<select class="form-control" name="currency_from" onchange="loadCurrencies(this.value);">
									<option value=""></option>
									<?php
									$query2 = $db->query("SELECT * FROM currencies ORDER BY id");
									while($row2 = $query2->fetch_assoc()) {
										if (strpos($list,$row2['currency_from']) !== false) { } else {
											echo '<option value="'.$row2[currency_from].'">'.$row2[currency_from].'</option>';
										}
										$list .= $row2['currency_from'].",";
									}
									?>
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>Currency to</label>
								<select class="form-control" name="currency_to" id="currency_to_list">

								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>Rate</label>
								<input type="text" class="form-control" name="rate">
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>&nbsp;</label>
								<button type="submit" class="btn btn-primary btn-block" name="btn_update"><i class="fa fa-check"></i> Update</button>
							</div>
						</div>
					</div>
				</form>
				<small>ระบบนี่จะอัพเดจค่าแลกเปลี่ยนสกลุนเงินทั้งหมดที่ตั้งค่าไว้</small>

				<br><br><br>

				<table class="table table-hover">
					<thead>
						<tr>
							<td width="15%">Name</td>
							<td width="20%">Account</td>
							<td width="15%">Allow user send</td>
							<td width="10%">Currencies</td>
							<td width="40%">User receive in</td>
						</tr>
					</thead>
					<tbody>
						<?php
						$query = $db->query("SELECT * FROM companies ORDER BY id");
						if($query->num_rows>0) {
							while($row = $query->fetch_assoc()) {
								?>
								<tr>
									<td><?php echo $row['name']; ?></td>
									<td><?php if(empty($row['a_field_1'])) { echo '<a href="./?a=companies&b=setup_account&id='.$row[id].'">Set up account</a>'; } else { echo 'Ready (<a href="./?a=companies&b=setup_account&id='.$row[id].'">Edit</a>)'; } ?></td>
									<td>
										<?php
										if($row['allow_send'] == "1") {
											echo 'Yes (<a href="./?a=companies&b=turn_off&id='.$row[id].'">Turn off</a>)';
										} else {
											echo 'No (<a href="./?a=companies&b=turn_on&id='.$row[id].'">Turn on</a>)';
										}
										?>
									</td>
									<td>
										<?php
										$getCurrencies = $db->query("SELECT * FROM currencies WHERE cid='$row[id]' and company_from='$row[name]'");
										if($getCurrencies->num_rows>0) {
											echo $getCurrencies->num_rows;
										} else {
											echo '0';
										}
										?>  (<a href="./?a=companies&b=manageCurrenciesList&id=<?php echo $row['id']; ?>"><i class="fa fa-pencil"></i> Manage</a>)
									</td>
									<td>
										<?php
										$i=1;
										$list = explode(",",$row['receive_list']);
										foreach($list as $l) {
											if (strpos($l,'//') !== false) { } else {
												if($i == 1) {
													echo $l;
												} else {
													echo ', '.$l;
												}
											}
											$i++;
										}
										?> (<a href="./?a=companies&b=manageReceiveList&id=<?php echo $row['id']; ?>"><i class="fa fa-pencil"></i> Manage</a>)
									</td>
								</tr>
								<?php
							}
						} else {
							echo '<tr><td colspan="5">No have companies.</td></tr>';
						}
						?>
					</tbody>
				</table>
			</div>
		</div>
		<?php
	}
	?>
</div>
</div>