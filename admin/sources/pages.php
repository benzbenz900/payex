<?php
$b = protect($_GET['b']);
$id = protect($_GET['id']);
?>
<ol class="breadcrumb">
	<li><a href="./">WebAdmin</a></li>
	<?php if($b == "edit") { ?>
	<li><a href="./?a=pages">Pages</a></li>
	<li class="active">Edit</li>
	<?php } else { ?>
	<li class="active">Pages</li>
	<?php } ?>
</ol>

<div class="row">
	<div class="col-lg-12">
			<?php
			if($b == "edit" && $id == "about") {
				?>
				<div class="panel panel-primary">
                  <div class="panel-heading"> 
						<i class="fa fa-pencil"></i> Edit page
				  </div>
                  <div class="panel-body">
					<?php
					if(isset($_POST['btn_save'])) {
						$content = $_POST['content'];
						if(empty($content)) { echo error("Please enter page content."); }
						else {
							$update = $db->query("UPDATE settings SET p_about='$content'");
							echo success("Your changes was saved successfully.");
							$settingsQuery = $db->query("SELECT * FROM settings ORDER BY id DESC LIMIT 1");
							$settings = $settingsQuery->fetch_assoc();
						}
					}
					?>
					<form action="" method="POST">
						<div class="form-group">
							<label>Title</label>
							<input type="text" class="form-control" value="About" disabled>
						</div>
						<div class="form-group">
							<label>Content</label>
							<textarea class="form-control" rows="15" name="content"><?php echo $settings['p_about']; ?></textarea>
						</div>	
						<button type="submit" class="btn btn-primary" name="btn_save"><i class="fa fa-check"></i> Save changes</button>
					</form>
				  </div>
				</div>
				<?php
			 } elseif($b == "edit" && $id == "privacy_policy") {
				?>
				<div class="panel panel-primary">
                  <div class="panel-heading"> 
						<i class="fa fa-pencil"></i> Edit page
				  </div>
                  <div class="panel-body">
					<?php
					if(isset($_POST['btn_save'])) {
						$content = $_POST['content'];
						if(empty($content)) { echo error("Please enter page content."); }
						else {
							$update = $db->query("UPDATE settings SET p_poilcy='$content'");
							echo success("Your changes was saved successfully.");
							$settingsQuery = $db->query("SELECT * FROM settings ORDER BY id DESC LIMIT 1");
							$settings = $settingsQuery->fetch_assoc();
						}
					}
					?>
					<form action="" method="POST">
						<div class="form-group">
							<label>Title</label>
							<input type="text" class="form-control" value="Privacy policy" disabled>
						</div>
						<div class="form-group">
							<label>Content</label>
							<textarea class="form-control" rows="15" name="content"><?php echo $settings['p_poilcy']; ?></textarea>
						</div>	
						<button type="submit" class="btn btn-primary" name="btn_save"><i class="fa fa-check"></i> Save changes</button>
					</form>
				  </div>
				</div>
				<?php
			} elseif($b == "edit" && $id == "terms_of_service") {
				?>
				<div class="panel panel-primary">
                  <div class="panel-heading"> 
						<i class="fa fa-pencil"></i> Edit page
				  </div>
                  <div class="panel-body">
					<?php
					if(isset($_POST['btn_save'])) {
						$content = $_POST['content'];
						if(empty($content)) { echo error("Please enter page content."); }
						else {
							$update = $db->query("UPDATE settings SET p_terms='$content'");
							echo success("Your changes was saved successfully.");
							$settingsQuery = $db->query("SELECT * FROM settings ORDER BY id DESC LIMIT 1");
							$settings = $settingsQuery->fetch_assoc();
						}
					}
					?>
					<form action="" method="POST">
						<div class="form-group">
							<label>Title</label>
							<input type="text" class="form-control" value="Terms of service" disabled>
						</div>
						<div class="form-group">
							<label>Content</label>
							<textarea class="form-control" rows="15" name="content"><?php echo $settings['p_terms']; ?></textarea>
						</div>	
						<button type="submit" class="btn btn-primary" name="btn_save"><i class="fa fa-check"></i> Save changes</button>
					</form>
				  </div>
				</div>
				<?php
			} else {
				?>
				<div class="panel panel-primary">
                  <div class="panel-heading"> 
						<i class="fa fa-folder-o"></i> Pages 
				  </div>
                  <div class="panel-body">
					<table class="table table-hover">
						<thead>
							<tr>
								<td width="95%">Page title</td>
								<td width="5%">Action</td>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>About</td>
								<td><a href="./?a=pages&b=edit&id=about" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="fa fa-pencil"></i></a> </td>
							</tr>
							<tr>
								<td>Privacy policy</td>
								<td><a href="./?a=pages&b=edit&id=privacy_policy" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="fa fa-pencil"></i></a> </td>
							</tr>
							<tr>
								<td>Terms of service</td>
								<td><a href="./?a=pages&b=edit&id=terms_of_service" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="fa fa-pencil"></i></a> </td>
							</tr>
							<tr>
								<td>FAQ</td>
								<td><a href="./?a=faq" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="fa fa-pencil"></i></a> </td>
							</tr>
						</tbody>
					</table>
			      </div>
				</div>
				<?php
			}
			?>
	</div>
</div>