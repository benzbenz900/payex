<?php
$b = protect($_GET['b']);
$id = protect($_GET['id']);
?>
<ol class="breadcrumb">
	<li><a href="./">WebAdmin</a></li>
	<?php if($b == "preview") { ?>
	<li><a href="./?a=exchanges">Exchanges</a></li>
	<li class="active">Preview</li>
	<?php } else { ?>
	<li class="active">Exchanges</li>
	<?php } ?>
</ol>

<div class="row">
	<div class="col-lg-12">
			<?php
			if($b == "preview") {
			$query = $db->query("SELECT * FROM exchanges WHERE id='$id'");
			if($query->num_rows==0) { header("Location: ./?a=exchanges"); }
			$row = $query->fetch_assoc();
				?>
				<div class="panel panel-primary">
                  <div class="panel-heading"> 
						<i class="fa fa-search"></i> Preview exchange 
				  </div>
                  <div class="panel-body">
					<h3>Exchange id: <?php echo $row['exchange_id']; ?></h3>
					<table class="table">
						<thead>
							<tr>
								<td>User</td>
								<td>Send</td>
								<td>Receive</td>
								<td>Amount</td>
								<td>Status</td>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><?php if($row['uid']>0) { echo idinfo($row['uid'],"username"); } else { echo 'Anonymous ('.$row[ip].')'; } ?></td>
								<td><?php echo $row['cfrom']; ?></td>
								<td><?php echo $row['cto']; ?></td>
								<td><?php echo $row['amount_from']; ?> <?php echo $row['currency_from']; ?></td>
								<td><?php
										if($row['status'] == "1") { echo '<span class="label label-info">Awaiting payment</span>'; }
										elseif($row['status'] == "2") { echo '<span class="label label-info">Processing</span>'; }
										elseif($row['status'] == "3") { echo '<span class="label label-danger">Payment fail</span>'; }
										elseif($row['status'] == "4") { echo '<span class="label label-danger">Refused</span>'; }
										elseif($row['status'] == "5") { echo '<span class="label label-success">Processed</span>'; }
										elseif($row['status'] == "6") { echo '<span class="label label-danger">Timeout</span>'; }
										else {
											echo '<span class="label label-default">Unknown</span>';
										}
										?></td>
							</tr>
							<tr>
								<td colspan="5">&nbsp;</td>
							</tr>
						</tbody>
						<thead>
							<tr>
								<td>Payee</td>
								<td>Referral id</td>
								<td>Exchange rate</td>
								<td>Amount receive</td>
								<td>Requested on</td>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><?php echo $row1['account']; ?></td>
								<td><?php if($row['referral_id'] == "0") { echo 'None'; } else { echo idinfo($row['referral_id'],"username"); } ?></td>
								<td><?php if($row['wallet_id'] > 0) { echo $row['rate']."%"; } else { echo "1 ".$row['currency_from']." = ".$row['rate']." ".$row['currency_to']; } ?></td>
								<td><?php echo $row['amount_to']; ?> <?php echo $row['currency_to']; ?></td>
								<td><?php echo date("d/m/Y H:i",$row['created']); ?></td>
							</tr>
							<tr>
								<td colspan="5">&nbsp;</td>
							</tr>
						</tbody>
						<thead>
							<tr>
								<td>Receiver account</td>
								<td>Receiver email</td>
								<td>Expiration</td>
								<td>Earned</td>
								<td>Action</td>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><?php echo $row['u_field_1']; ?></td>
								<td><?php echo $row['u_field_2']; ?></td>
								<td><?php echo date("d/m/Y H:i",$row['expiration']); ?></td>
								<td><?php if($row['status'] == "5") { echo number_format($row['amount_from']-$row['amount_to'],2); echo $row['currency_from']; } ?></td>
								<td></td>
							</tr>
						</tbody>
					</table>
					
					<?php if($row['status'] == "2" or $row['cto'] == "Bank Transfer" or $row['cto'] == "Western union" or $row['cto'] == "Moneygram" or $row['cfrom'] == "Bank Transfer" or $row['cfrom'] == "Western union" or $row['cfrom'] == "Moneygram") { ?>
					<?php
					if($row['cto'] == "Bank Transfer") {
					?>
					<br>
					<h3>Details to make transfer:</h3>
					<b></b>
					<b>Client name</b>: <?php echo $row['u_field_3'];?><br/>
						<b>Client location</b>: <?php echo $row['u_field_4'];?><br/>
						<b>Bank name</b>: <?php echo $row['u_field_5'];?><br/>
						<b>Bank account number/IBAN</b>: <?php echo $row['u_field_6'];?><br/>
						<b>Bank swift</b>: <?php echo $row['u_field_7'];?><br/>
						<b>Description</b>: Exchange <?php echo $row['amount_from']; ?> <?php echo $row['currency_from']; ?><br/>
						<b>Amount</b>: <?php echo $row['amount_to']; ?> <?php echo $row['currency_to']; ?><br><br>
					<?php
					} elseif($row['cto'] == "Western union") {
					?>
					<br>
					<h3>Details to make transfer:</h3>
					<b></b>
					<b>Send to</b>: <?php echo $row['u_field_3']; ?><br/>
						<b>Location</b>: <?php echo $row['u_field_4']; ?><br/>
						<b>Description</b>: Exchange <?php echo $row['amount_from']; ?> <?php echo $row['currency_from']; ?><br/>
						<b>Amount</b>: <?php echo $row['amount_to']; ?> <?php echo $row['currency_to']; ?><br><br>
					<?php
					} elseif($row['cto'] == "Moneygram") {
					?>
					<br>
					<h3>Details to make transfer:</h3>
					<b></b>
					<b>Send to</b>: <?php echo $row['u_field_3']; ?><br/>
						<b>Location</b>: <?php echo $row['u_field_4']; ?><br/>
						<b>Description</b>: Exchange <?php echo $row['amount_from']; ?> <?php echo $row['currency_from']; ?><br/>
						<b>Amount</b>: <?php echo $row['amount_to']; ?> <?php echo $row['currency_to']; ?><br><br>
					<?php
					} else {
					
					}
					?>
					<div id="take_action">
						<?php
						if(isset($_POST['btn_update'])) {
							$redirect = './?a=exchanges&b=preview&id='.$id;
							$status = protect($_POST['status']);
							$pin = protect($_POST['pin']);
							if($status == "5") {
								$earnings = $row['amount_from']-$row['amount_to'];
								$date = date("d/m/Y");
								$check_e = $db->query("SELECT * FROM earnings WHERE company='$row[cfrom]' and currency='$row[currency_from]'");
								if($check_e->num_rows>0) {
									$update = $db->query("UPDATE earnings SET amount=amount+$earnings WHERE company='$row[cfrom]' and currency='$row[currency_from]'");
								} else {
									$insert = $db->query("INSERT earnings (amount,currency,company) VALUES ('$earnings','$row[currency_from]','$row[cfrom]')");
								}
								if($row['referral_id']>0) {
									$com = $row['amount_from'] - $row['amount_to'];
									$comm = 100+$settings[referral_comission];
									$com2 = ($com * 100) / $comm; 
									$com = $com-$com2; 
									$comission = $com;
									$comission = number_format($comission,2);
									$time = time();
									$check_wallet = $db->query("SELECT * FROM wallets WHERE uid='$row[referral_id]' and currency='$row[currency_from]'");
									if($check_wallet->num_rows>0) {
										$update_wallet = $db->query("UPDATE wallets SET amount=amount+$comission,updated='$time' WHERE uid='$row[referral_id]' and currency='$row[currency_from]'");
									} else {
										$insert = $db->query("INSERT wallets (uid,amount,currency,created) VALUES ('$row[referral_id]','$comission','$row[currency_from]','$time')");
									}
								}
								$update = $db->query("UPDATE exchanges SET status='5' WHERE id='$row[id]'");
								if($row['cto'] == "Western union") {
									$pin = 'Your Western union pin: '.$pin;
								} elseif($row['cto'] == "Moneygram") {
									$pin = 'Your Moneygram pin: '.$pin;
								} else {
									$pin = '';
								}
								$msubject = '['.$settings[sitename].'] Exchange '.$row[exchange_id].' was processed.';
								$mreceiver = $row[email];
								$message = 'Hi, '.$row[email].'
Your exchange ('.$row[exchange_id].') was processed successfully.
You receive in your '.$row[cto].' account '.$row[amount_to].' '.$row[currency_to].'
'.$pin.'

If you have some problems please feel free to contact with us on '.$settings[siteemail];
								$headers = 'From: '.$settings[siteemail].'' . "\r\n" .
									'Reply-To: '.$settings[siteemail].'' . "\r\n" .
									'X-Mailer: PHP/' . phpversion();

								mail($mreceiver, $msubject, $message, $headers);
								header("Location: $redirect");
							} else {
								if($status == "4") {
									if($row['wallet_id'] > 0) {
										$update = $db->query("UPDATE wallets SET amount=amount+$row[amount_from] WHERE id='$row[wallet_id]'");
									}
								}
								$update = $db->query("UPDATE exchanges SET status='$status' WHERE id='$row[id]'");
								header("Location: $redirect");
							}
						}
						?>
						<form action="" method="POST">
							<div class="form-group">
								<label>Status</label>
								<select name="status" class="form-control">
									<option value="1" <?php if($row['status'] == "1") { echo 'selected'; } ?>>Awaiting payment</option>
									<option value="2" <?php if($row['status'] == "2") { echo 'selected'; } ?>>Processing</option>
									<option value="3" <?php if($row['status'] == "3") { echo 'selected'; } ?>>Payment fail</option>
									<option value="4" <?php if($row['status'] == "4") { echo 'selected'; } ?>>Refused</option>
									<option value="5" <?php if($row['status'] == "5") { echo 'selected'; } ?>>Processed</option>
									<option value="6" <?php if($row['status'] == "6") { echo 'selected'; } ?>>Timeout</option>
								</select>
							</div>
							<?php
							if($row['cto'] == "Western union") {
								?>
								<div class="form-group">
									<label>Enter Western union PIN CODE (will be send to user via email to get his/her money)</label>
									<input type="text" class="form-control" name="pin">
								</div>
								<?php
							} elseif($row['cto'] == "Moneygram") {
								?>
								<div class="form-group">
									<label>Enter Moneygram PIN CODE (will be send to user via email to get his/her money)</label>
									<input type="text" class="form-control" name="pin">
								</div>
								<?php
							}
							?>
							<button type="submit" class="btn btn-primary" name="btn_update"><i class="fa fa-check"></i> Update</button>
						</form>
					</div>
					<?php } ?>
				  </div>
				</div>
				<?php
			} else {
				?>
				<div class="panel panel-primary">
                  <div class="panel-heading"> 
						<i class="fa fa-refresh"></i> Exchanges 
						<span class="pull-right">
						  <form action="" method="POST">
							<input type="text" class="input_search" name="qry" placeholder="Search by exchange id...">
						  </form>
						</span>
				  </div>
                  <div class="panel-body">
					<table class="table table-hover">
						<thead>
							<tr>
								<td width="25%">User</td>
								<td width="20%">Send</td>
								<td width="20%">Receive</td>
								<td width="15%">Amount</td>
								<td width="15%">Status</td>
								<td width="5%">Action</td>
							</tr>
						</thead>
						<tbody>
							<?php
							$page = (int) (!isset($_GET["page"]) ? 1 : $_GET["page"]);
							$limit = 20;
							$startpoint = ($page * $limit) - $limit;
							if($page == 1) {
								$i = 1;
							} else {
								$i = $page * $limit;
							}
							
							if(isset($_POST['qry'])) {
								$qry = protect($_POST['qry']);
								$query = $db->query("SELECT * FROM exchanges WHERE exchange_id='$qry' ORDER BY id");
								$searching=1;
							} else {
								$statement = "exchanges";
								$query = $db->query("SELECT * FROM {$statement} ORDER BY id LIMIT {$startpoint} , {$limit}");
								$searching=0;
							}
							if($query->num_rows>0) {
								while($row = $query->fetch_assoc()) {
									$rows[] = $row;
								}
								foreach($rows as $row) {
									?>
									<tr>
										<td><?php if($row['uid'] == "0") { echo 'Anonymous'; } else { echo idinfo($row['uid'],"username"); } ?></td>
										<td><?php echo $row['cfrom']; ?></td>
										<td><?php echo $row['cto']; ?></td>
										<td><?php echo $row['amount_from']; ?> <?php echo $row['currency_from']; ?></td>
										<td>
										<?php
										if($row['status'] == "1") { echo '<span class="label label-info">Awaiting payment</span>'; }
										elseif($row['status'] == "2") { echo '<span class="label label-info">Processing</span>'; }
										elseif($row['status'] == "3") { echo '<span class="label label-danger">Payment fail</span>'; }
										elseif($row['status'] == "4") { echo '<span class="label label-danger">Refused</span>'; }
										elseif($row['status'] == "5") { echo '<span class="label label-success">Processed</span>'; }
										elseif($row['status'] == "6") { echo '<span class="label label-danger">Timeout</span>'; }
										else {
											echo '<span class="label label-default">Unknown</span>';
										}
										?>
										</td>
										<td>
											<a href="./?a=exchanges&b=preview&id=<?php echo $row['id']; ?>" data-toggle="tooltip" data-placement="bottom" title="Preview"><i class="fa fa-search"></i></a> 
										</td>
									</tr>
									<?php
								}
							} else {
								if($searching==1) {
									echo '<tr><td colspan="6">No found results.</td></tr>';
								} else {
									echo '<tr><td colspan="6">No have recorded exchanges.</td></tr>';
								}
							}
							?>
						</tbody>
					</table>
					<?php
					if($searching==0) {
						$ver = "./?a=exchanges";
						if(admin_pagination($statement,$ver,$limit,$page)) {
							echo admin_pagination($statement,$ver,$limit,$page);
						}
					}
					?>
			      </div>
				</div>
				<?php
			}
			?>
	</div>
</div>