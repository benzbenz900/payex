<?php
$b = protect($_GET['b']);
$id = protect($_GET['id']);
?>
<ol class="breadcrumb">
	<li><a href="./">WebAdmin</a></li>
	<li class="active">Web Settings</li>
</ol>

<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<i class="fa fa-cogs"></i> Web Settings
			</div>
			<div class="panel-body">
				<?php
				if(isset($_POST['btn_save'])) {
					$title = protect($_POST['title']);
					$description = protect($_POST['description']);
					$keywords = protect($_POST['keywords']);
					$sitename = protect($_POST['sitename']);
					$url = protect($_POST['url']);
					$siteemail = protect($_POST['siteemail']);
					$exchminamount = protect($_POST['exchminamount']);
					$exchtimewithoper = protect($_POST['exchtimewithoper']);
					$exchtimewithoutoper = protect($_POST['exchtimewithoutoper']);
					$referral_comission = protect($_POST['referral_comission']);
					$wallet_comission = protect($_POST['wallet_comission']);
					if(empty($title) or empty($description) or empty($keywords) or empty($sitename) or empty($url) or empty($siteemail) or empty($exchminamount) or empty($exchtimewithoper) or empty($exchtimewithoutoper) or empty($wallet_comission) or empty($referral_comission)) { echo error("All fields are required."); }
					elseif(!isValidURL($url)) { echo error("Please enter valid site url address. Eg: http://pandachanger.com/"); }
					elseif(!isValidEmail($siteemail)) { echo error("Please enter valid site email address. Eg: support@pandachanger.com"); }
					elseif(!is_numeric($referral_comission)) { echo error("Please enter referral comission with numbers. Eg: 10 = 10% , 15 = 15%"); }
					elseif(!is_numeric($wallet_comission)) { echo error("Please enter wallet comission with numbers. Eg: 10 = 10% , 15 = 15%"); }
					else {
						$update = $db->query("UPDATE settings SET title='$title',description='$description',keywords='$keywords',sitename='$sitename',url='$url',siteemail='$siteemail',exchminamount='$exchminamount',exchtimewithoper='$exchtimewithoper',exchtimewithoutoper='$exchtimewithoutoper',referral_comission='$referral_comission',wallet_comission='$wallet_comission'");
						echo success("Your changes was saved successfully.");
						$settingsQuery = $db->query("SELECT * FROM settings ORDER BY id DESC LIMIT 1");
						$settings = $settingsQuery->fetch_assoc();
					}
				}
				?>
				<form action="" method="POST">
					<div class="form-group">
						<label>Title</label>
						<input type="text" class="form-control" name="title" value="<?php echo $settings['title']; ?>">
					</div>
					<div class="form-group">
						<label>Description</label>
						<textarea class="form-control" name="description" rows="3"><?php echo $settings['description']; ?></textarea>
					</div>
					<div class="form-group">
						<label>Keywords</label>
						<textarea class="form-control" name="keywords" rows="3"><?php echo $settings['keywords']; ?></textarea>
					</div>
					<div class="form-group">
						<label>ชื่อเว็บ</label>
						<input type="text" class="form-control" name="sitename" value="<?php echo $settings['sitename']; ?>">
					</div>
					<div class="form-group">
						<label>URL เว็บ</label>
						<input type="text" class="form-control" name="url" value="<?php echo $settings['url']; ?>">
					</div>
					<div class="form-group">
						<label>Email</label>
						<input type="text" class="form-control" name="siteemail" value="<?php echo $settings['siteemail']; ?>">
					</div>
					<div class="form-group">
						<label>แลกขั้นต่ำ</label>
						<select name="exchminamount" class="form-control">
							<option value="1" <?php if($settings['exchminamount'] == "1") { echo 'selected'; } ?>>1</option>
							<option value="5" <?php if($settings['exchminamount'] == "5") { echo 'selected'; } ?>>5</option>
							<option value="10" <?php if($settings['exchminamount'] == "10") { echo 'selected'; } ?>>10</option>
							<option value="20" <?php if($settings['exchminamount'] == "20") { echo 'selected'; } ?>>20</option>
							<option value="50" <?php if($settings['exchminamount'] == "50") { echo 'selected'; } ?>>50</option>
						</select>
					</div>
					<div class="form-group">
						<label>เวลาแลกโดยประมาณ</label>
						<select name="exchtimewithoper" class="form-control">
							<option value="1 minute" <?php if($settings['exchtimewithoper'] == "1 minute") { echo 'selected'; } ?>>1 minute</option>
							<option value="5 minutes" <?php if($settings['exchtimewithoper'] == "5 minutes") { echo 'selected'; } ?>>5 minutes</option>
							<option value="10 minutes" <?php if($settings['exchtimewithoper'] == "10 minutes") { echo 'selected'; } ?>>10 minutes</option>
							<option value="20 minutes" <?php if($settings['exchtimewithoper'] == "20 minutes") { echo 'selected'; } ?>>20 minutes</option>
							<option value="30 minutes" <?php if($settings['exchtimewithoper'] == "30 minutes") { echo 'selected'; } ?>>30 minutes</option>
							<option value="1 hour" <?php if($settings['exchtimewithoper'] == "1 hour") { echo 'selected'; } ?>>1 hour</option>
							<option value="3 hours" <?php if($settings['exchtimewithoper'] == "3 hours") { echo 'selected'; } ?>>3 hours</option>
							<option value="6 hours" <?php if($settings['exchtimewithoper'] == "6 hours") { echo 'selected'; } ?>>6 hours</option>
							<option value="12 hours" <?php if($settings['exchtimewithoper'] == "12 hours") { echo 'selected'; } ?>>12 hours</option>
							<option value="24 hours" <?php if($settings['exchtimewithoper'] == "24 hours") { echo 'selected'; } ?>>24 hours</option>
						</select>
					</div>
					<div class="form-group">
						<label>เวลาแลกโดยประมาณ (กรณีไม่มีเจ้าหน้าที่ออนไลน์)</label>
						<select name="exchtimewithoutoper" class="form-control">
							<option value="1 minute" <?php if($settings['exchtimewithoutoper'] == "1 minute") { echo 'selected'; } ?>>1 minute</option>
							<option value="5 minutes" <?php if($settings['exchtimewithoutoper'] == "5 minutes") { echo 'selected'; } ?>>5 minutes</option>
							<option value="10 minutes" <?php if($settings['exchtimewithoutoper'] == "10 minutes") { echo 'selected'; } ?>>10 minutes</option>
							<option value="20 minutes" <?php if($settings['exchtimewithoutoper'] == "20 minutes") { echo 'selected'; } ?>>20 minutes</option>
							<option value="30 minutes" <?php if($settings['exchtimewithoutoper'] == "30 minutes") { echo 'selected'; } ?>>30 minutes</option>
							<option value="1 hour" <?php if($settings['exchtimewithoutoper'] == "1 hour") { echo 'selected'; } ?>>1 hour</option>
							<option value="3 hours" <?php if($settings['exchtimewithoutoper'] == "3 hours") { echo 'selected'; } ?>>3 hours</option>
							<option value="6 hours" <?php if($settings['exchtimewithoutoper'] == "6 hours") { echo 'selected'; } ?>>6 hours</option>
							<option value="12 hours" <?php if($settings['exchtimewithoutoper'] == "12 hours") { echo 'selected'; } ?>>12 hours</option>
							<option value="24 hours" <?php if($settings['exchtimewithoutoper'] == "24 hours") { echo 'selected'; } ?>>24 hours</option>
							<option value="48 hours" <?php if($settings['exchtimewithoutoper'] == "48 hours") { echo 'selected'; } ?>>48 hours</option>
							<option value="72 hours" <?php if($settings['exchtimewithoutoper'] == "72 hours") { echo 'selected'; } ?>>72 hours</option>
						</select>
					</div>
					<div class="form-group">
						<label>ค่าแนะนำคนมาใช้งาน (ไม่ต้องใส่ %)</label>
						<input type="text" class="form-control" name="referral_comission" value="<?php echo $settings['referral_comission']; ?>">
					</div>
					<div class="form-group">
						<label>ค่าแนะนำเมื่อมีลูกค้าแลกเงิน (ไม่ต้องใส่ %)</label>
						<input type="text" class="form-control" name="wallet_comission" value="<?php echo $settings['wallet_comission']; ?>">
					</div>
					<button type="submit" class="btn btn-primary" name="btn_save"><i class="fa fa-check"></i> Save changes</button>
				</form>
			</div>
		</div>
	</div>
</div>