<ol class="breadcrumb">
	<li><a href="./">WebAdmin</a></li>
	<li class="active">Dashboard</li>
</ol>

<div class="row">
	<div class="col-lg-3">
		 <div class="panel panel-default twitter">
                    <div class="panel-body fa-icons">
                        <small class="social-title">Users</small>
                        <h3 class="count">
                            <?php $get_stats = $db->query("SELECT * FROM users"); echo $get_stats->num_rows; ?></h3>
                        <i class="fa fa-users"></i>
                    </div>
                </div>
	</div>
	<div class="col-lg-3">
		<div class="panel panel-default google-plus">
                    <div class="panel-body fa-icons">
                        <small class="social-title">Exchanges</small>
                        <h3 class="count">
                            <?php $get_stats = $db->query("SELECT * FROM exchanges"); echo $get_stats->num_rows; ?></h3>
                        <i class="fa fa-refresh"></i>
                    </div>
                </div>
	</div>
	<div class="col-lg-3">
		<div class="panel panel-default facebook-like">
                    <div class="panel-body fa-icons">
                        <small class="social-title">Testimonials</small>
                        <h3 class="count">
                            <?php $get_stats = $db->query("SELECT * FROM testimonials"); echo $get_stats->num_rows; ?></h3>
                        <i class="fa fa-comments"></i>
                    </div>
                </div>
	</div>
	<div class="col-lg-3">
		<div class="panel panel-default visitor">
                    <div class="panel-body fa-icons">
                        <small class="social-title">Withdrawals</small>
                        <h3 class="count">
                            <?php $get_stats = $db->query("SELECT * FROM withdrawals"); echo $get_stats->num_rows; ?></h3>
                        <i class="fa fa-dollar"></i>
                    </div>
                </div>
	</div>
	<div class="col-lg-8">
		<div class="panel panel-primary">
			<div class="panel-heading">Pending exchanges</div>
			<div class="panel-body">
				<table class="table">
		      <thead>
		        <tr>
		          <th>#</th>
		          <th>User</th>
		          <th>From - To</th>
		          <th>Amount</th>
				  <th>Action</th>
		        </tr>
		      </thead>
		      <tbody>
		      <?php
			  $t=1;
			  $query = $db->query("SELECT * FROM exchanges WHERE status='2' ORDER BY id");
			  if($query->num_rows>0) {
			    while($row = $query->fetch_assoc()) {
				?>
				<tr>
		          <th scope="row"><?php echo $t; ?></th>
		          <td><?php if($row['uid']>0) { ?><a href="./?a=users&b=edit&id=<?php echo $row['uid']; ?>"><?php echo idinfo($row['uid'],"username"); ?></a><?php } else { echo 'Anonymous ('.$row[ip].')'; } ?></td>
		          <td><?php echo $row['cfrom']; ?> to <?php echo $row['cto']; ?></td>
		          <td><?php echo $row['amount_from']; ?> <?php echo $row['currency_from']; ?></td>
				  <td>
					<a href="./?a=exchanges&b=preview&id=<?php echo $row['id']; ?>#take_action" data-toggle="tooltip" data-placement="bottom" title="Open to take action"><i class="fa fa-check"></i></a> 
				  </td>
		        </tr>
				<?php 
				$t++;
				}
			  } else {
				echo '<tr><td colspan="5">No have requests for exchanges.</td></tr>';
			  }
			  ?>
		      </tbody>
		    </table>
			</div>	
		</div>
		<br>
		<div class="panel panel-primary">
			<div class="panel-heading">Pending withdrawals</div>
			<div class="panel-body">
				<table class="table">
		      <thead>
		        <tr>
		          <th>#</th>
		          <th>User</th>
		          <th>Amount</th>
		          <th>Account</th>
				  <th>Action</th>
		        </tr>
		      </thead>
		      <tbody>
		      <?php
			  $i=1;
			  $query = $db->query("SELECT * FROM withdrawals WHERE status='1' ORDER BY id");
			  if($query->num_rows>0) {
			    while($row = $query->fetch_assoc()) {
				?>
				<tr>
		          <th scope="row"><?php echo $i; ?></th>
		          <td><a href="./?a=users&b=edit&id=<?php echo $row['id']; ?>"><?php echo idinfo($row['uid'],"username"); ?></a></td>
		          <td><?php echo $row['amount']; ?> <?php echo $row['currency']; ?></td>
		          <td><?php echo $row['account']; ?> (<?php echo $row['company']; ?>)</td>
				  <td>
					<a href="./?a=withdrawals&b=mark_completed&id=<?php echo $row['id']; ?>" data-toggle="tooltip" data-placement="bottom" title="Mark as completed"><i class="fa fa-check"></i></a> 
											<a href="./?a=withdrawals&b=mark_denied&id=<?php echo $row['id']; ?>" data-toggle="tooltip" data-placement="bottom" title="Mark as denied"><i class="fa fa-times"></i></a>
				  </td>
		        </tr>
				<?php 
				$i++;
				}
			  } else {
				echo '<tr><td colspan="5">No have requests for withdrawal.</td></tr>';
			  }
			  ?>
		      </tbody>
		    </table>
			</div>	
		</div>
		<?php
		$query = $db->query("SELECT * FROM deposit_requests WHERE status='1' ORDER BY id");
		if($query->num_rows>0) {
			?>
			<br>
			<div class="panel panel-primary">
				<div class="panel-heading">Pending deposits</div>
				<div class="panel-body">
					<table class="table">
				  <thead>
					<tr>
					  <th>#</th>
					  <th>User</th>
					  <th>Amount</th>
					  <th>Via</th>
					  <th></th>
					</tr>
				  </thead>
				  <tbody>
				  <?php
				  $i=1;
				  while($row = $query->fetch_assoc()) {
					?>
					<tr>
					  <th scope="row"><?php echo $i; ?></th>
					  <td><a href="./?a=users&b=edit&id=<?php echo $row['id']; ?>"><?php echo idinfo($row['uid'],"username"); ?></a></td>
					  <td><?php echo $row['amount']; ?> <?php echo $row['currency']; ?></td>
					  <td><?php echo $row['cfrom']; ?></td>
					  <td>
						<a href="./?a=deposits&b=complete&id=<?php echo $row['id']; ?>" data-toggle="tooltip" data-placement="bottom" title="Complete deposit"><i class="fa fa-check"></i></a> 
					    <a href="./?a=deposits&b=delete&id=<?php echo $row['id']; ?>" data-toggle="tooltip" data-placement="bottom" title="Delete deposit request"><i class="fa fa-times"></i></a>
					  </td>
					</tr>
					<?php 
					$i++;
					}
				  ?>
				  </tbody>
				</table>
				</div>	
			</div>
			<?php
		}
		?>
	</div>
	<div class="col-lg-4">
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default google-plus">
                    <div class="panel-body fa-icons">
                        <small class="social-title">Your earnings</small>
                        <br><br>
						<?php
						$earningsQuery = $db->query("SELECT * FROM earnings ORDER BY id");
						if($earningsQuery->num_rows>0) {
							while($earn = $earningsQuery->fetch_assoc()) {
								?>
								<div>
									<span class="pull-left"><?php echo $earn['company']; ?></span>
									<span class="pull-right"><?php echo $earn['amount']." ".$earn['currency']; ?></span>
								</div><br>
								<?php
							}
						} else {
							echo 'For the moment you do not have any earnings.'; 
						}
						?>
                    </div>
                </div>
		</div>
	</div>
</div>