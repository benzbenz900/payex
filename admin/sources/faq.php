<?php
$b = protect($_GET['b']);
$id = protect($_GET['id']);
?>
<ol class="breadcrumb">
	<li><a href="./">WebAdmin</a></li>
	<?php if($b == "edit") { ?>
	<li><a href="./?a=faq">FAQ</a></li>
	<li class="active">Edit question</li>
	<?php } elseif($b == "delete") { ?>
	<li><a href="./?a=faq">FAQ</a></li>
	<li class="active">Delete question</li>
	<?php } elseif($b == "add") { ?>
	<li><a href="./?a=faq">FAQ</a></li>
	<li class="active">Add question</li>
	<?php } else { ?>
	<li class="active">FAQ</li>
	<?php } ?>
</ol>

<div class="row">
	<div class="col-lg-12">
			<?php
			if($b == "add") {
			?>
				<div class="panel panel-primary">
                  <div class="panel-heading"> 
						<i class="fa fa-plus"></i> Add question
				  </div>
                  <div class="panel-body">
					<?php
					if(isset($_POST['btn_add'])) {
						$title = protect($_POST['title']);
						$content = $_POST['content'];
						$check = $db->query("SELECT * FROM faq WHERE question='$title'");
						if(empty($title) or empty($content)) { echo error("All fields are required."); }
						elseif($check->num_rows>0) { echo error("Already exists question with this title."); }
						else {	
							$insert = $db->query("INSERT faq (question,answer) VALUES ('$title','$content')");
							echo success("Question ($title) was added successfully.");
						}
					}
					?>
					<form action="" method="POST">
						<div class="form-group">
							<label>Title</label>
							<input type="text" class="form-control" name="title">
						</div>
						<div class="form-group">
							<label>Content</label>
							<textarea class="form-control" rows="15" name="content"></textarea>
						</div>
						<button type="submit" class="btn btn-primary" name="btn_add"><i class="fa fa-plus"></i> Add</button>
					</form>
				  </div>
				</div>
			<?php
			} elseif($b == "edit") {
			$query = $db->query("SELECT * FROM faq WHERE id='$id'");
			if($query->num_rows==0) { header("Location: ./?a=faq"); }
			$row = $query->fetch_assoc();
				?>
				<div class="panel panel-primary">
                  <div class="panel-heading"> 
						<i class="fa fa-pencil"></i> Edit question
				  </div>
                  <div class="panel-body">
					<?php
					if(isset($_POST['btn_save'])) {
						$title = protect($_POST['title']);
						$content = $_POST['content'];
						$check = $db->query("SELECT * FROM faq WHERE question='$title'");
						if(empty($title) or empty($content)) { echo error("All fields are required."); }
						elseif($row['title'] !== $title && $check->num_rows>0) { echo error("Already exists question with this title."); }
						else {	
							$update = $db->query("UPDATE faq SET question='$title',answer='$content' WHERE id='$id'");
							echo success("Your changes was saved successfully.");
							$query = $db->query("SELECT * FROM faq WHERE id='$id'");
							$row = $query->fetch_assoc();
						}
					}
					?>
					<form action="" method="POST">
						<div class="form-group">
							<label>Title</label>
							<input type="text" class="form-control" name="title" value="<?php echo $row['question']; ?>">
						</div>
						<div class="form-group">
							<label>Content</label>
							<textarea class="form-control" rows="15" name="content"><?php echo $row['answer']; ?></textarea>
						</div>
						<button type="submit" class="btn btn-primary" name="btn_save"><i class="fa fa-check"></i> Save changes</button>
					</form>
				  </div>
				</div>
				<?php
			} elseif($b == "delete") {
			$query = $db->query("SELECT * FROM faq WHERE id='$id'");
			if($query->num_rows==0) { header("Location: ./?a=faq"); }
			$row = $query->fetch_assoc();
				?>
				<div class="panel panel-primary">
                  <div class="panel-heading"> 
						<i class="fa fa-times"></i> Delete question
				  </div>
                  <div class="panel-body">
					<?php
					if(isset($_GET['confirmed'])) {
						$delete = $db->query("DELETE FROM faq WHERE id='$id'");
						echo success("Question ($row[question]) was deleted.");
					} else {
						echo info("Are you sure you want to delete question ($row[question])?");
						echo '<a href="./?a=faq&b=delete&id='.$row[id].'&confirmed=1" class="btn btn-success"><i class="fa fa-check"></i> Yes</a>&nbsp;&nbsp;
								<a href="./?a=faq" class="btn btn-danger"><i class="fa fa-times"></i> No</a>';
					}
					?>
				  </div>
				</div>
				<?php
			} else {
				?>
				<div class="panel panel-primary">
                  <div class="panel-heading"> 
						<i class="fa fa-question-circle"></i> FAQ 
						<span class="pull-right">
							<a href="./?a=faq&b=add" style="color:#fff;"><i class="fa fa-plus"></i> Add question</a>
						</span>
				  </div>
                  <div class="panel-body">
					<table class="table table-hover">
						<thead>
							<tr>
								<td width="95%">Title</td>
								<td width="5%">Action</td>
							</tr>
						</thead>
						<tbody>
							<?php
							$page = (int) (!isset($_GET["page"]) ? 1 : $_GET["page"]);
							$limit = 20;
							$startpoint = ($page * $limit) - $limit;
							if($page == 1) {
								$i = 1;
							} else {
								$i = $page * $limit;
							}
							$statement = "faq";
							$query = $db->query("SELECT * FROM {$statement} ORDER BY id LIMIT {$startpoint} , {$limit}");
							if($query->num_rows>0) {
								while($row = $query->fetch_assoc()) {
									$rows[] = $row;
								}
								foreach($rows as $row) {
									?>
									<tr>
										<td><?php echo $row['question'] ?></td>
										<td>
											<a href="./?a=faq&b=edit&id=<?php echo $row['id']; ?>" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="fa fa-pencil"></i></a> 
											<a href="./?a=faq&b=delete&id=<?php echo $row['id']; ?>" data-toggle="tooltip" data-placement="bottom" title="Delete"><i class="fa fa-times"></i></a>
										</td>
									</tr>
									<?php
								}
							} else {
								echo '<tr><td colspan="2">No have recorded questions.</td></tr>';
							}
							?>
						</tbody>
					</table>
					<?php
					$ver = "./?a=faq";
					if(admin_pagination($statement,$ver,$limit,$page)) {
						echo admin_pagination($statement,$ver,$limit,$page);
					}
					?>
			      </div>
				</div>
				<?php
			}
			?>
	</div>
</div>