<?php
$b = protect($_GET['b']);
$id = protect($_GET['id']);
?>
<ol class="breadcrumb">
	<li><a href="./">WebAdmin</a></li>
	<?php if($b == "edit") { ?>
	<li><a href="./?a=users">Users</a></li>
	<li class="active">Edit</li>
	<?php } elseif($b == "delete") { ?>
	<li><a href="./?a=users">Users</a></li>
	<li class="active">Delete</li>
	<?php } else { ?>
	<li class="active">Users</li>
	<?php } ?>
</ol>

<div class="row">
	<div class="col-lg-12">
			<?php
			if($b == "edit") {
			$query = $db->query("SELECT * FROM users WHERE id='$id'");
			if($query->num_rows==0) { header("Location: ./?a=users"); }
			$row = $query->fetch_assoc();
				?>
				<div class="panel panel-primary">
                  <div class="panel-heading"> 
						<i class="fa fa-pencil"></i> Edit user 
				  </div>
                  <div class="panel-body">
					<?php
					if(isset($_POST['btn_save'])) {
						$username = protect($_POST['username']);
						$email = protect($_POST['email']);
						$status = protect($_POST['status']);
						$check_u = $db->query("SELECT * FROM users WHERE username='$username'");
						$check_e = $db->query("SELECT * FROM users WHERE email='$email'");
						if(empty($username) or empty($email) or empty($status)) { echo error("All fields are required."); }
						elseif($row['username'] !== $username && !isValidUsername($username)) { echo error("Enter valid username. Use latin characters, numbers and symbols - and _"); }
						elseif($row['username'] !== $username && $check_u->num_rows>0) { echo error("This username is already used by another user."); }
						elseif($row['email'] !== $email && !isValidEmail($email)) { echo error("Enter valid email address. Eg: john@gmail.com"); }
						elseif($row['email'] !== $email && $check_e->num_rows>0) { echo error("This email address is already used by another user."); }
						else { 
							$update = $db->query("UPDATE users SET username='$username',email='$email',status='$status' WHERE id='$id'");
							echo success("Your changes was saved successfully.");
							$query = $db->query("SELECT * FROM users WHERE id='$id'");
							$row = $query->fetch_assoc();
						}
					}
					?>
					<form action="" method="POST">
						<div class="form-group">
							<label>Username</label>
							<input type="text" class="form-control" name="username" value="<?php echo $row['username']; ?>">
						</div>
						<div class="form-group">
							<label>Email address</label>
							<input type="text" class="form-control" name="email" value="<?php echo $row['email']; ?>">
						</div>
						<div class="form-group">
							<label>Level</label>
							<select name="status" class="form-control">
								<option value="1" <?php if($row['status'] == "1") { echo 'selected'; } ?>>Member</option>
								<option value="2" <?php if($row['status'] == "2") { echo 'selected'; } ?>>Banned</option>
								<option value="666" <?php if($row['status'] == "666") { echo 'selected'; } ?>>Admin</option>
							</select>
						</div>
						<button type="submit" class="btn btn-primary" name="btn_save"><i class="fa fa-check"></i> Save changes</button>
					</form>
				  </div>
				</div>
				<?php
			} elseif($b == "delete") {
			$query = $db->query("SELECT * FROM users WHERE id='$id'");
			if($query->num_rows==0) { header("Location: ./?a=users"); }
			$row = $query->fetch_assoc();
				?>
				<div class="panel panel-primary">
                  <div class="panel-heading"> 
						<i class="fa fa-times"></i> Delete user 
				  </div>
                  <div class="panel-body">
					<?php
					if(isset($_GET['confirmed'])) {
						$delete = $db->query("DELETE FROM users WHERE id='$id'");
						$delete = $db->query("DELETE FROM testimonials WHERE uid='$id'");
						$delete = $db->query("DELETE FROM withdrawals WHERE uid='$id'");
						$delete = $db->query("DELETE FROM exchanges WHERE uid='$id'");
						echo success("User ($row[username]) was deleted successfully.");
					} else {
						echo info("Are you sure you want to delete user ($row[username])?");
						echo '<a href="./?a=users&b=delete&id='.$row[id].'&confirmed=1" class="btn btn-success"><i class="fa fa-check"></i> Yes</a>&nbsp;&nbsp;
								<a href="./?a=users" class="btn btn-danger"><i class="fa fa-times"></i> No</a>';
					}
					?>
				  </div>
				</div>
			<?php
			} else {
				?>
				<div class="panel panel-primary">
                  <div class="panel-heading"> 
						<i class="fa fa-users"></i> Users 
						<span class="pull-right">
						  <form action="" method="POST">
							<input type="text" class="input_search" name="qry" placeholder="Search...">
						  </form>
						</span>
				  </div>
                  <div class="panel-body">
					<table class="table table-hover">
						<thead>
							<tr>
								<td width="25%">Username</td>
								<td width="25%">Email address</td>
								<td width="15%">Exchanges</td>
								<td width="15%">Referral earnings</td>
								<td width="15%">Withdrawals</td>
								<td width="5%">Action</td>
							</tr>
						</thead>
						<tbody>
							<?php
							$searching=0;
							if(isset($_POST['qry'])) {
								$searching=1;
								$qry = protect($_POST['qry']);
							}
							$page = (int) (!isset($_GET["page"]) ? 1 : $_GET["page"]);
							$limit = 20;
							$startpoint = ($page * $limit) - $limit;
							if($page == 1) {
								$i = 1;
							} else {
								$i = $page * $limit;
							}
							$statement = "users";
							if($searching==1) {
								if(empty($qry)) {
									$qry = 'empty query';
								}
								$query = $db->query("SELECT * FROM {$statement} WHERE username LIKE '%$qry%' or email LIKE '%$qry%' ORDER BY id");
							} else {
								$query = $db->query("SELECT * FROM {$statement} ORDER BY id LIMIT {$startpoint} , {$limit}");
							}
							if($query->num_rows>0) {
								while($row = $query->fetch_assoc()) {
									$rows[] = $row;
								}
								foreach($rows as $row) {
									?>
									<tr>
										<td><?php echo $row['username']; ?></td>
										<td><?php echo $row['email']; ?></td>
										<td><?php $get_stats = $db->query("SELECT * FROM exchanges WHERE uid='$row[id]'"); echo $get_stats->num_rows; ?></td>
										<td>$<?php echo number_format($row['earnings'],2); ?></td>
										<td><?php $get_stats = $db->query("SELECT * FROM withdrawals WHERE uid='$row[id]' and status='2'"); echo $get_stats->num_rows; ?></td>
										<td>
											<a href="./?a=users&b=edit&id=<?php echo $row['id']; ?>" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="fa fa-pencil"></i></a> 
											<a href="./?a=users&b=delete&id=<?php echo $row['id']; ?>" data-toggle="tooltip" data-placement="bottom" title="Delete"><i class="fa fa-times"></i></a>
										</td>
									</tr>
									<?php
								}
							} else {
							  if($searching==1) {
								echo '<tr><td colspan="6">No found results for <b>'.$qry.'</b>.</td></tr>';
							  } else {
								echo '<tr><td colspan="6">No have registered users.</td></tr>';
							  }
							}
							?>
						</tbody>
					</table>
					<?php
					if($searching==0) {
						$ver = "./?a=users";
						if(admin_pagination($statement,$ver,$limit,$page)) {
							echo admin_pagination($statement,$ver,$limit,$page);
						}
					}
					?>
			      </div>
				</div>
				<?php
			}
			?>
	</div>
</div>