<?php
error_reporting(0);
ob_start();
session_start(); 
if(file_exists("../install.php")) {
	header("Location: ../install.php");
} 
include("../includes/config.php");
$db = new mysqli($CONF['host'], $CONF['user'], $CONF['pass'], $CONF['name']);
if ($db->connect_errno) {
    echo "Failed to connect to MySQL: (" . $db->connect_errno . ") " . $db->connect_error;
}
$db->set_charset("utf8");
$settingsQuery = $db->query("SELECT * FROM settings ORDER BY id DESC LIMIT 1");
$settings = $settingsQuery->fetch_assoc();
include("../includes/functions.php");
$a = protect($_GET['a']);
if(checkAdminSession()) {
include("sources/page-header.php");
switch($a) {
	case "users": include("sources/users.php"); break;
	case "exchanges": include("sources/exchanges.php"); break;
	case "withdrawals": include("sources/withdrawals.php"); break;
	case "testimonials": include("sources/testimonials.php"); break;
	case "settings": include("sources/settings.php"); break;
	case "companies": include("sources/companies.php"); break;
	case "pages": include("sources/pages.php"); break;
	case "faq": include("sources/faq.php"); break;
	case "deposits": include("sources/deposits.php"); break;
	case "logout": 
		unset($_SESSION['ex_adm_uid']);
		unset($_SESSION['ex_adm_username']);
		session_unset();
		session_destroy();
		header("Location: ./");
	break;
	default: include("sources/dashboard.php"); 
}
include("sources/page-footer.php");
} else { 
	include("sources/login.php");
}
mysqli_close($db);
?>