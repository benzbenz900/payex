<?php
ob_start();
session_start();
error_reporting(0);
if(file_exists("./install.php")) {
	header("Location: ./install.php");
} 
include("includes/config.php");
$db = new mysqli($CONF['host'], $CONF['user'], $CONF['pass'], $CONF['name']);
if ($db->connect_errno) {
    echo "Failed to connect to MySQL: (" . $db->connect_errno . ") " . $db->connect_error;
}
$db->set_charset("utf8");
$settingsQuery = $db->query("SELECT * FROM settings ORDER BY id DESC LIMIT 1");
$settings = $settingsQuery->fetch_assoc();
include("includes/functions.php");
include(getLanguage($settings['url'],null,null));
if(checkSession()) {
	$time = time();
	$update = $db->query("UPDATE users SET acttime='$time' WHERE id='$_SESSION[suid]'");
	if(idinfo($_SESSION['suid'],"status") == "2") {
		unset($_SESSION['suid']);
		unset($_SESSION['susername']);
		session_unset();
		session_destroy();
		header("Location: $settings[url]");
	}
} else {
	if($_COOKIE['mexchange_user_id']) {
		$_SESSION['suid'] = $_COOKIE['mexchange_user_id'];
		$_SESSION['susername'] = $_COOKIE['mexchange_username'];
		header("Location: $settings[url]");
	}
}

check_unpayed();

if(isset($_GET['refid'])) {
	$_SESSION['refid'] = protect($_GET['refid']);
	header("Location: $settings[url]");
}

include("sources/header.php");
$a = protect($_GET['a']);
switch($a) {
	case "login": include("sources/login.php"); break;
	case "register": include("sources/register.php"); break;
	case "forgot-password": include("sources/forgot-password.php"); break;
	case "password-recovery": include("sources/password-recovery.php"); break;
	case "check_payment": include("sources/check_payment.php"); break;
	case "check_deposit": include("sources/check_deposit.php"); break;
	case "account": include("sources/account.php"); break;
	case "page": include("sources/page.php"); break;
	case "testimonials": include("sources/testimonials.php"); break;
	case "submit-testimonial": include("sources/submit-testimonial.php"); break;
	case "become_payment": include("sources/become_payment.php"); break;
	case "logout": 
		unset($_SESSION['suid']);
		unset($_SESSION['susername']);
		unset($_COOKIE['mexchange_user_id']);
		unset($_COOKIE['mexchange_username']);
		setcookie("mexchange_user_id", "", time() - (86400 * 30), '/'); // 86400 = 1 day
		setcookie("mexchange_username", "", time() - (86400 * 30), '/'); // 86400 = 1 day
		session_unset();
		session_destroy();
		header("Location: $settings[url]");
	break;
	default: include("sources/homepage.php");
}
include("sources/footer.php");
mysqli_close($db);
?>