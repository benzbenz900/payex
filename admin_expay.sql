-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 10, 2019 at 10:36 PM
-- Server version: 5.5.31
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `admin_expay`
--

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `a_field_1` varchar(255) NOT NULL,
  `a_field_2` varchar(255) NOT NULL,
  `a_field_3` varchar(255) NOT NULL,
  `a_field_4` varchar(255) NOT NULL,
  `a_field_5` varchar(255) NOT NULL,
  `a_field_6` varchar(255) NOT NULL,
  `a_field_7` varchar(255) NOT NULL,
  `a_field_8` varchar(255) NOT NULL,
  `a_field_9` varchar(255) NOT NULL,
  `a_field_10` varchar(255) NOT NULL,
  `allow_send` int(11) NOT NULL,
  `receive_list` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `name`, `a_field_1`, `a_field_2`, `a_field_3`, `a_field_4`, `a_field_5`, `a_field_6`, `a_field_7`, `a_field_8`, `a_field_9`, `a_field_10`, `allow_send`, `receive_list`) VALUES
(1, 'PayPal', 'benzbenz900@gmail.com', '', '', '', '', '', '', '', '', '', 1, '//PayPal,//Payeer,//Perfect Money,//AdvCash,//OKPay,//Entromoney,//Payza,//Bitcoin,Bank Transfer,//Western union,//Moneygram'),
(2, 'Payeer', '', '', '', '', '', '', '', '', '', '', 0, 'PayPal,Payeer,Perfect Money,AdvCash,OKPay,Entromoney,Payza,Bitcoin,Bank Transfer,Western union,Moneygram'),
(3, 'Perfect Money', '', '', '', '', '', '', '', '', '', '', 0, 'PayPal,Payeer,Perfect Money,AdvCash,OKPay,Entromoney,Payza,Bitcoin,Bank Transfer,Western union,Moneygram'),
(4, 'AdvCash', '', '', '', '', '', '', '', '', '', '', 0, 'PayPal,Payeer,Perfect Money,AdvCash,OKPay,Entromoney,Payza,Bitcoin,Bank Transfer,Western union,Moneygram'),
(5, 'OKPay', '', '', '', '', '', '', '', '', '', '', 0, 'PayPal,Payeer,Perfect Money,AdvCash,OKPay,Entromoney,Payza,Bitcoin,Bank Transfer,Western union,Moneygram'),
(6, 'Entromoney', '', '', '', '', '', '', '', '', '', '', 0, 'PayPal,Payeer,Perfect Money,AdvCash,OKPay,Entromoney,Payza,Bitcoin,Bank Transfer,Western union,Moneygram'),
(7, 'Payza', '', '', '', '', '', '', '', '', '', '', 0, 'PayPal,Payeer,Perfect Money,AdvCash,OKPay,Entromoney,Payza,Bitcoin,Bank Transfer,Western union,Moneygram'),
(8, 'Bitcoin', '', '', '', '', '', '', '', '', '', '', 0, 'PayPal,Payeer,Perfect Money,AdvCash,OKPay,Entromoney,Payza,Bitcoin,Bank Transfer,Western union,Moneygram'),
(9, 'Bank Transfer', '', '', '', '', '', '', '', '', '', '', 1, 'PayPal,//Payeer,//Perfect Money,//AdvCash,//OKPay,//Entromoney,//Payza,//Bitcoin,//Bank Transfer,//Western union,//Moneygram'),
(10, 'Western union', '', '', '', '', '', '', '', '', '', '', 0, 'PayPal,Payeer,Perfect Money,AdvCash,OKPay,Entromoney,Payza,Bitcoin,Bank Transfer,Western union,Moneygram'),
(11, 'Moneygram', '', '', '', '', '', '', '', '', '', '', 0, 'PayPal,Payeer,Perfect Money,AdvCash,OKPay,Entromoney,Payza,Bitcoin,Bank Transfer,Western union,Moneygram');

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE `currencies` (
  `id` int(11) NOT NULL,
  `cid` int(11) NOT NULL,
  `company_from` varchar(255) NOT NULL,
  `company_to` varchar(255) NOT NULL,
  `currency_from` varchar(255) NOT NULL,
  `currency_to` varchar(255) NOT NULL,
  `rate` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `cid`, `company_from`, `company_to`, `currency_from`, `currency_to`, `rate`) VALUES
(1, 1, 'PayPal', 'Bank Transfer', 'USD', 'THB', '32.3'),
(2, 9, 'Bank Transfer', 'PayPal', 'THB', 'USD', '0.025606');

-- --------------------------------------------------------

--
-- Table structure for table `earnings`
--

CREATE TABLE `earnings` (
  `id` int(11) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `currency` varchar(255) NOT NULL,
  `company` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exchanges`
--

CREATE TABLE `exchanges` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `wallet_id` int(11) NOT NULL,
  `cfrom` varchar(255) NOT NULL,
  `cto` varchar(255) NOT NULL,
  `amount_from` varchar(255) NOT NULL,
  `currency_from` varchar(255) NOT NULL,
  `amount_to` varchar(255) NOT NULL,
  `currency_to` varchar(255) NOT NULL,
  `rate` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `created` int(11) NOT NULL,
  `updated` int(11) NOT NULL,
  `expired` int(11) NOT NULL,
  `u_field_1` varchar(255) NOT NULL,
  `u_field_2` varchar(255) NOT NULL,
  `u_field_3` varchar(255) NOT NULL,
  `u_field_4` varchar(255) NOT NULL,
  `u_field_5` varchar(255) NOT NULL,
  `u_field_6` varchar(255) NOT NULL,
  `u_field_7` varchar(255) NOT NULL,
  `u_field_8` varchar(255) NOT NULL,
  `u_field_9` varchar(255) NOT NULL,
  `u_field_10` varchar(255) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `transaction_id` varchar(255) NOT NULL,
  `exchange_id` varchar(255) NOT NULL,
  `referral_id` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exchanges`
--

INSERT INTO `exchanges` (`id`, `uid`, `wallet_id`, `cfrom`, `cto`, `amount_from`, `currency_from`, `amount_to`, `currency_to`, `rate`, `status`, `created`, `updated`, `expired`, `u_field_1`, `u_field_2`, `u_field_3`, `u_field_4`, `u_field_5`, `u_field_6`, `u_field_7`, `u_field_8`, `u_field_9`, `u_field_10`, `ip`, `transaction_id`, `exchange_id`, `referral_id`) VALUES
(1, 0, 0, '', '', '', '', '0', '', '', 6, 1466883183, 0, 0, '', '', '', '', '', '', '', '', '', '', '173.245.52.71', '', 'b7fc89-34937c24ee1-c07df', '0'),
(2, 0, 0, 'PayPal', 'Bank Transfer', '1000', 'USD', '32300', 'THB', '32.3', 6, 1488870840, 0, 0, '', 'abc@aaa.com', 'ทดสอบ', 'bangkok', '1113', '1113', '1113', '', '', '', '172.68.142.195', '', '660e79-006eb7c919f-6eb9d', '0'),
(3, 0, 0, 'PayPal', 'Bank Transfer', '100', 'USD', '3230', 'THB', '32.3', 6, 1520423121, 0, 0, '', 'support@asiagb.com', 'pattrawut', 'thailand', 'kabank', '1150', '1150', '', '', '', '172.68.144.155', '', '39050b-7c4e9dd3463-44aac', '0'),
(4, 10000, 0, 'Bank Transfer', 'PayPal', '1000', 'THB', '25.606', 'USD', '0.025606', 2, 1520423358, 0, 0, 'pattrawut_injan@outlook.com', 'pattrawut_injan@outlook.com', '', '', '', '', '', '', '', '', '172.68.144.155', '', 'c2bf57-80c54562f75-ac419', '0'),
(5, 10000, 0, 'Bank Transfer', 'PayPal', '10000', 'THB', '256.06', 'USD', '0.025606', 6, 1520423942, 0, 0, 'jjkghfu@kjvjkvik.com', 'lnwphp@lnwphp.in.th', '', '', '', '', '', '', '', '', '172.68.144.155', '', 'cb5e1f-e20980d6a5b-f72fe', '0'),
(6, 10000, 0, 'Bank Transfer', 'PayPal', '10000', 'THB', '256.06', 'USD', '0.025606', 6, 1520423951, 0, 0, 'jjkghfu@kjvjkvik.com', 'lnwphp@lnwphp.in.th', '', '', '', '', '', '', '', '', '172.68.144.155', '', '23ca6b-99f248546eb-5836f', '0'),
(7, 10000, 0, 'Bank Transfer', 'PayPal', '10000', 'THB', '256.06', 'USD', '0.025606', 6, 1520423959, 0, 0, 'jjkghfu@kjvjkvik.com', 'lnwphp@lnwphp.in.th', '', '', '', '', '', '', '', '', '172.68.144.155', '', '898eb4-0e8e392eac6-eb4b8', '0');

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE `faq` (
  `id` int(11) NOT NULL,
  `question` text NOT NULL,
  `answer` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `keywords` varchar(255) NOT NULL,
  `sitename` varchar(255) NOT NULL,
  `siteemail` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `referral_comission` varchar(255) NOT NULL,
  `wallet_comission` varchar(255) NOT NULL,
  `exchminamount` varchar(255) NOT NULL,
  `exchtimewithoper` varchar(255) NOT NULL,
  `exchtimewithoutoper` varchar(255) NOT NULL,
  `earnings` varchar(255) NOT NULL,
  `p_about` text NOT NULL,
  `p_terms` text NOT NULL,
  `p_poilcy` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `title`, `description`, `keywords`, `sitename`, `siteemail`, `url`, `referral_comission`, `wallet_comission`, `exchminamount`, `exchtimewithoper`, `exchtimewithoutoper`, `earnings`, `p_about`, `p_terms`, `p_poilcy`) VALUES
(1, 'แลกเงิน PayPal เป็นเงินไทย บาท | ชื้อเงิน PayPal จากเงินบาทด้วยการโอนเงิน', 'รับแลกเงินบาทเป็นเงิน US PayPal เรทสูง และรับแลก เงิน US PayPal เป็นเงินบาท บริการเร็วทันใจ จากทีมงาน LnwPHP | US ดอลล่า เป็น บาท และ บาท เป็น US ดอลล่า เรทสูง 33.4 34.5 35.4 ถึง 36.2 ขึ้นยุกับช่วงเวลาแต่ละวัน', 'ถอนเงิน Paypal,แลกเงิน US,ชื้อเงิน US ราคาถูก,แลกเงินบาทเป็นดอลล่า,แลกเงินดอลล่า,เติมเงินเกมส์,ชื้อเงิน US เติมเกมส์,ดอลล่าเป็นบาท', 'WonderEx PayPal', 'benzbenz900@gmail.com', 'https://demo.mul.pw/payex/', '5', '1', '10', '10 minutes', '24 hours', '0', 'WonderExPay หรือ WonderEX PayPal เป็นเว็บให้บริการสำหรับคนที่ต้องการเปลี่ยนเปลี่ยนเงิน US จาก Paypal เป็นเงิน ไทยบาทเพื่อนำไปเติมเกมส์ หรือชื้อสินค้าจากบริการออนไลน์ต่างๆ\r\nทางเราให้บริการทั้งการแลกเปลี่ยนจาก US Paypal เป็นไทยบาท และ ไทยบาทเป็น US Paypal เพื่อให้ง่ายต่อการชื้อสินค้าออนไลน์ เติมเกมส์ออนไลน์ ต่างๆ ไม่ได้มีส่วนได้เสียจาก Paypal และยังรับเรทราคาตามเวลาปัจจุบัญ ลดตามเรทเพียงเล็กน้อย เพื่อให้ผู้ใช่บริการได้รับ เงินเทียบเท่าเงินจริง ณ เวลานั้นๆ', '', ''),
(2, 'แลกเงิน PayPal เป็นเงินไทย บาท | ชื้อเงิน PayPal จากเงินบาทด้วยการโอนเงิน', 'รับแลกเงินบาทเป็นเงิน US PayPal เรทสูง และรับแลก เงิน US PayPal เป็นเงินบาท บริการเร็วทันใจ จากทีมงาน LnwPHP | US ดอลล่า เป็น บาท และ บาท เป็น US ดอลล่า เรทสูง 33.4 34.5 35.4 ถึง 36.2 ขึ้นยุกับช่วงเวลาแต่ละวัน', 'ถอนเงิน Paypal,แลกเงิน US,ชื้อเงิน US ราคาถูก,แลกเงินบาทเป็นดอลล่า,แลกเงินดอลล่า,เติมเงินเกมส์,ชื้อเงิน US เติมเกมส์,ดอลล่าเป็นบาท', 'WonderEx PayPal', 'benzbenz900@gmail.com', 'https://demo.mul.pw/payex/', '5', '1', '10', '10 minutes', '24 hours', '0', 'WonderExPay หรือ WonderEX PayPal เป็นเว็บให้บริการสำหรับคนที่ต้องการเปลี่ยนเปลี่ยนเงิน US จาก Paypal เป็นเงิน ไทยบาทเพื่อนำไปเติมเกมส์ หรือชื้อสินค้าจากบริการออนไลน์ต่างๆ\r\nทางเราให้บริการทั้งการแลกเปลี่ยนจาก US Paypal เป็นไทยบาท และ ไทยบาทเป็น US Paypal เพื่อให้ง่ายต่อการชื้อสินค้าออนไลน์ เติมเกมส์ออนไลน์ ต่างๆ ไม่ได้มีส่วนได้เสียจาก Paypal และยังรับเรทราคาตามเวลาปัจจุบัญ ลดตามเรทเพียงเล็กน้อย เพื่อให้ผู้ใช่บริการได้รับ เงินเทียบเท่าเงินจริง ณ เวลานั้นๆ', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `content` text NOT NULL,
  `status` int(11) NOT NULL,
  `time` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `txn_id` varchar(255) NOT NULL,
  `payee` varchar(255) NOT NULL,
  `company` varchar(255) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `currency` varchar(255) NOT NULL,
  `time` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `password_recovery` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `earnings` varchar(255) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `signuptime` int(11) NOT NULL,
  `lastsignintime` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `password`, `password_recovery`, `email`, `earnings`, `ip`, `status`, `signuptime`, `lastsignintime`) VALUES
(10000, 'Admin', 'lnwphp', '308c0b9b0dd15b044ac3146e642f92d5', '', 'lnwphp@lnwphp.in.th', '0', '', 666, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wallets`
--

CREATE TABLE `wallets` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `currency` varchar(255) NOT NULL,
  `created` varchar(255) NOT NULL,
  `updated` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `withdrawals`
--

CREATE TABLE `withdrawals` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `wallet_id` int(11) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `currency` varchar(255) NOT NULL,
  `company` varchar(255) NOT NULL,
  `account` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `requested_on` int(11) NOT NULL,
  `processed_on` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `earnings`
--
ALTER TABLE `earnings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exchanges`
--
ALTER TABLE `exchanges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wallets`
--
ALTER TABLE `wallets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `withdrawals`
--
ALTER TABLE `withdrawals`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `earnings`
--
ALTER TABLE `earnings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `exchanges`
--
ALTER TABLE `exchanges`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10003;
--
-- AUTO_INCREMENT for table `wallets`
--
ALTER TABLE `wallets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `withdrawals`
--
ALTER TABLE `withdrawals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
