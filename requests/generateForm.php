<?php
ob_start();
session_start();
error_reporting(0);
include("../includes/config.php");
$db = new mysqli($CONF['host'], $CONF['user'], $CONF['pass'], $CONF['name']);
if ($db->connect_errno) {
    echo "Failed to connect to MySQL: (" . $db->connect_errno . ") " . $db->connect_error;
}
$db->set_charset("utf8");
$settingsQuery = $db->query("SELECT * FROM settings ORDER BY id DESC LIMIT 1");
$settings = $settingsQuery->fetch_assoc();
include("../includes/functions.php");
include(getLanguage($settings['url'],null,2));
$from = protect($_GET['from']);
$to = protect($_GET['to']);
$amount_from = protect($_GET['amount_from']);
$currency_from = protect($_GET['currency_from']);
$query = $db->query("SELECT * FROM companies WHERE name='$from'");
$row = $query->fetch_assoc();
$getFirstCurrency = $db->query("SELECT * FROM currencies WHERE company_from='$from' and company_to='$to' and currency_from='$currency_from' ORDER BY id LIMIT 1");
$cur = $getFirstCurrency->fetch_assoc();
if(empty($from) or empty($to) or empty($amount_from) or empty($currency_from)) { echo error("All fields are required."); }
elseif(!is_numeric($amount_from)) { echo error("Please enter amount with numbers. Eg: 19.99"); } 
elseif($from !== "Bitcoin" && $amount_from < $settings['exchminamount']) { echo error("Minimal amount for exchange is $settings[exchminamount]."); }
else {
?>
	<div class="form-group">
		<label>Receive amount</label>
		<div class="input-group">
			<input type="text" class="form-control input-lg" name="amount_to" id="amount_to" value="<?php echo $amount_from*$cur['rate']; ?>" disabled>
			<span class="input-group-btn">
				<select class="form-control input-lg" style="min-width:100px;" onChange="reCalculateAmount();" name="currency_to" id="currency_to_list">
					<?php
					$getCurrencies = $db->query("SELECT * FROM currencies WHERE company_from='$from' and company_to='$to' and currency_from='$currency_from' ORDER BY id");
					if($getCurrencies->num_rows>0) {
						while($get = $getCurrencies->fetch_assoc()) {
							if($cur['currency_to'] == $get['currency_to']) { $sel = 'selected'; } else { $sel = ''; }
							echo '<option value="'.$get[currency_to].'" '.$sel.' ">'.$get[currency_to].'</option>';
						}
					} else {
						echo '<option value="">NaN</option>';
					}
					?>
				</select>
			</span>
		</div>
	</div>
	<input type="hidden" name="rate" id="rate" value="<?php echo $cur['rate']; ?>">
	<?php
	if($to == "Bank Transfer") {
	?>
	<div class="form-group">
		<label>Your name</label>
		<input type="text" class="form-control input-lg" name="u_field_3" id="s_name"> 
	</div>
	<div class="form-group">
		<label>Your location</label>
		<input type="text" class="form-control input-lg" name="u_field_4" id="s_location"> 
	</div>
	<div class="form-group">
		<label>Bank name</label>
		<input type="text" class="form-control input-lg" name="u_field_5" id="s_bankname"> 
	</div>
	<div class="form-group">
		<label>Bank Account IBAN</label>
		<input type="text" class="form-control input-lg" name="u_field_6" id="s_bankiban"> 
	</div>
	<div class="form-group">
		<label>Bank Swift</label>
		<input type="text" class="form-control input-lg" name="u_field_7" id="s_bankswift"> 
	</div>
	<?php
	} elseif($to == "Western union") {
	?>
	<div class="form-group">
		<label>Your name</label>
		<input type="text" class="form-control input-lg" name="u_field_3" id="s_name"> 
	</div>
	<div class="form-group">
		<label>Your location</label>
		<input type="text" class="form-control input-lg" name="u_field_4" id="s_location"> 
	</div>
	<?php
	} elseif($to == "Moneygram") {
		?>
	<div class="form-group">
		<label>Your name</label>
		<input type="text" class="form-control input-lg" name="u_field_3" id="s_name"> 
	</div>
	<div class="form-group">
		<label>Your location</label>
		<input type="text" class="form-control input-lg" name="u_field_4" id="s_location"> 
	</div>
	<?php
	} else {
	?>
	<div class="form-group">
		<label>Your <?php echo $to; ?> <?php if($to == "Bitcoin") { echo 'address'; } else { echo 'account'; } ?></label>
		<input type="text" class="form-control input-lg" name="account">
	</div>
	<?php
	}
	?>
	<div class="form-group">
		<label>Your email address</label>
		<input type="text" class="form-control input-lg" name="email" value="<?php if(checkSession()) { echo idinfo($_SESSION['suid'],"email"); } ?>">
	</div>
	<button type="button" class="btn btn-primary btn-lg btn-block" onclick="validateForm();"><i class="fa fa-mail-forward"></i> <?php echo $lang['btn_9']; ?></button>
<?php
}
?>