<?php
ob_start();
session_start();
error_reporting(0);
include("../includes/config.php");
$db = new mysqli($CONF['host'], $CONF['user'], $CONF['pass'], $CONF['name']);
if ($db->connect_errno) {
    echo "Failed to connect to MySQL: (" . $db->connect_errno . ") " . $db->connect_error;
}
$db->set_charset("utf8");
$settingsQuery = $db->query("SELECT * FROM settings ORDER BY id DESC LIMIT 1");
$settings = $settingsQuery->fetch_assoc();
include("../includes/functions.php");
$c_from = protect($_GET['c_from']);
$c_to = protect($_GET['c_to']);
$from = protect($_GET['from']);
$to = protect($_GET['to']);
	$query = $db->query("SELECT * FROM currencies WHERE company_from='$from' and company_to='$to' and currency_from='$c_from' and currency_to='$c_to'"); 
	if($query->num_rows>0) {
		$row = $query->fetch_assoc();
		echo '<center><i class="fa fa-refresh"></i> Exchange rate: 1 '.$c_from.' = <span id="ex_rate">'.$row[rate].'</span> '.$c_to.'</center>';
	}
?>