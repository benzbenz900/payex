<?php
ob_start();
session_start();
error_reporting(0);
include("../includes/config.php");
$db = new mysqli($CONF['host'], $CONF['user'], $CONF['pass'], $CONF['name']);
if ($db->connect_errno) {
    echo "Failed to connect to MySQL: (" . $db->connect_errno . ") " . $db->connect_error;
}
$db->set_charset("utf8");
$settingsQuery = $db->query("SELECT * FROM settings ORDER BY id DESC LIMIT 1");
$settings = $settingsQuery->fetch_assoc();
include("../includes/functions.php");
include(getLanguage($settings['url'],null,2));
$from = protect($_GET['from']);
$to = protect($_GET['to']);
$amount_from = protect($_GET['amount_from']);
$currency_from = protect($_GET['currency_from']);
$currency_to = protect($_GET['currency_to']);
$query = $db->query("SELECT * FROM currencies WHERE company_from='$from' and company_to='$to' and currency_from='$currency_from' and currency_to='$currency_to'");
if($query->num_rows>0) {
	$row = $query->fetch_assoc();
	echo $amount_from*$row['rate'];
} else {
	echo 'Please refresh page!';
}
?>