<?php
ob_start();
session_start();
error_reporting(0);
include("../includes/config.php");
$db = new mysqli($CONF['host'], $CONF['user'], $CONF['pass'], $CONF['name']);
if ($db->connect_errno) {
    echo "Failed to connect to MySQL: (" . $db->connect_errno . ") " . $db->connect_error;
}
$db->set_charset("utf8");
$settingsQuery = $db->query("SELECT * FROM settings ORDER BY id DESC LIMIT 1");
$settings = $settingsQuery->fetch_assoc();
include("../includes/functions.php");
$from = protect($_GET['from']);
if(!empty($from)) {
	$query = $db->query("SELECT * FROM companies WHERE name='$from'");
	$row = $query->fetch_assoc();
	$list = explode(",",$row['receive_list']);
	echo '<option value=""></option>';
	foreach($list as $l) {
		if (strpos($l,'//') !== false) { } else {
			echo '<option value="'.$l.'">'.$l.'</option>';
		}
	}
} else {	
	echo '<option value="">Something was wrong</option>';
}
?>