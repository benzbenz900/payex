<?php
ob_start();
session_start();
error_reporting(E_ALL);
include("../includes/config.php");
$db = new mysqli($CONF['host'], $CONF['user'], $CONF['pass'], $CONF['name']);
if ($db->connect_errno) {
    echo "Failed to connect to MySQL: (" . $db->connect_errno . ") " . $db->connect_error;
}
$db->set_charset("utf8");
$settingsQuery = $db->query("SELECT * FROM settings ORDER BY id DESC LIMIT 1");
$settings = $settingsQuery->fetch_assoc();
include("../includes/functions.php");
$from = protect($_POST['from']);
$to = protect($_POST['to']);
$query = $db->query("SELECT * FROM companies WHERE name='$from'");
$row = $query->fetch_assoc();
$query2 = $db->query("SELECT * FROM currencies WHERE cid='$row[id]' and company_from='$from' and company_to='$to' ORDER BY id");
?>
<script type="text/javascript" src="<?php echo $settings['url']; ?>assets/homepage/js/jquery-1.11.1.min.js"></script>
	<script type="text/javascript" src="<?php echo $settings['url']; ?>assets/js/script.js"></script>
<div id="exchange_status"></div>
<div class="form-group">
	<label>Amount send</label>
	<div class="input-group">
	  <input type="text" class="form-control" name="a_from" id="a_from" onkeyup="calculateAmount();changeAmountFrom(this.value);" onkeydown="calculateAmount();changeAmountFrom(this.value);">
	  <div class="input-group-btn" style="width:25%;">
		<select class="form-control" name="c_from" id="c_from" onchange="calculateAmount();changeCurrencyFrom(this.value);">
			<?php
			if($query2->num_rows>0) {
				while($row2 = $query2->fetch_assoc()) {
					if (strpos($list,$row2['currency_from']) !== false) { } else {
						echo '<option value="'.$row2[currency_from].'">'.$row2[currency_from].'</option>';
					}
					$list .= $row2['currency_from'].",";
				}
			} else {
				echo '<option value="0">None</option>';
			}
			?>
		</select>
	  </div>
	</div>
</div>
<div id="receive_field"></div>
<div id="exchange_rate"></div>
<div id="account_field"></div>