<?php
ob_start();
session_start();
error_reporting(0);
include("../includes/config.php");
$db = new mysqli($CONF['host'], $CONF['user'], $CONF['pass'], $CONF['name']);
if ($db->connect_errno) {
    echo "Failed to connect to MySQL: (" . $db->connect_errno . ") " . $db->connect_error;
}
$db->set_charset("utf8");
$settingsQuery = $db->query("SELECT * FROM settings ORDER BY id DESC LIMIT 1");
$settings = $settingsQuery->fetch_assoc();
include("../includes/functions.php");
$from = protect($_GET['from']);
if(!empty($from)) {
	?>
	<script type="text/javascript" src="<?php echo $settings['url']; ?>assets/homepage/js/jquery-1.11.1.min.js"></script>
	<script type="text/javascript" src="<?php echo $settings['url']; ?>assets/js/script.js"></script>
	<?php
	$query = $db->query("SELECT * FROM companies WHERE name='$from'");
	$row = $query->fetch_assoc();
	$list = explode(",",$row['receive_list']);
	foreach($list as $l) {
		if (strpos($l,'//') !== false) { } else {
			echo '<a href="javascript:void(0);" class="list-group-item">'.$l.'</a>';
		}
	}
} else {	
	echo '<a href="#" class="list-group-item">Something wrong..</a>';
}
?>