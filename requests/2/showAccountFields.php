<?php
ob_start();
session_start();
error_reporting(0);
include("../includes/config.php");
$db = new mysqli($CONF['host'], $CONF['user'], $CONF['pass'], $CONF['name']);
if ($db->connect_errno) {
    echo "Failed to connect to MySQL: (" . $db->connect_errno . ") " . $db->connect_error;
}
$db->set_charset("utf8");
$settingsQuery = $db->query("SELECT * FROM settings ORDER BY id DESC LIMIT 1");
$settings = $settingsQuery->fetch_assoc();
include("../includes/functions.php");
$to = protect($_GET['to']);
?>
<br>
<?php 
if($to == "Bank Transfer") {
?>
<div class="form-group">
	<label>Your name</label>
	<input type="text" class="form-control" name="s_name" id="s_name" onkeyup="changeUFIELD3(this.value);" onkeydown="changeUFIELD3(this.value);"> 
</div>
<div class="form-group">
	<label>Your location</label>
	<input type="text" class="form-control" name="s_location" id="s_location" onkeyup="changeUFIELD4(this.value);" onkeydown="changeUFIELD4(this.value);"> 
</div>
<div class="form-group">
	<label>Bank name</label>
	<input type="text" class="form-control" name="s_bankname" id="s_bankname" onkeyup="changeUFIELD5(this.value);" onkeydown="changeUFIELD5(this.value);"> 
</div>
<div class="form-group">
	<label>Bank Account IBAN</label>
	<input type="text" class="form-control" name="s_bankiban" id="s_bankiban" onkeyup="changeUFIELD6(this.value);" onkeydown="changeUFIELD6(this.value);"> 
</div>
<div class="form-group">
	<label>Bank Swift</label>
	<input type="text" class="form-control" name="s_bankswift" id="s_bankswift" onkeyup="changeUFIELD7(this.value);" onkeydown="changeUFIELD7(this.value);"> 
</div>
<?php 
} elseif($to == "Moneygram") {
?>
<div class="form-group">
	<label>Your name</label>
	<input type="text" class="form-control" name="s_name" id="s_name" onkeyup="changeUFIELD3(this.value);" onkeydown="changeUFIELD3(this.value);"> 
</div>
<div class="form-group">
	<label>Your location</label>
	<input type="text" class="form-control" name="s_location" id="s_location" onkeyup="changeUFIELD4(this.value);" onkeydown="changeUFIELD4(this.value);"> 
</div>
<?php 
} elseif($to == "Western union") {
?>
<div class="form-group">
	<label>Your name</label>
	<input type="text" class="form-control" name="s_name" id="s_name" onkeyup="changeUFIELD3(this.value);" onkeydown="changeUFIELD3(this.value);"> 
</div>
<div class="form-group">
	<label>Your location</label>
	<input type="text" class="form-control" name="s_location" id="s_location" onkeyup="changeUFIELD4(this.value);" onkeydown="changeUFIELD4(this.value);"> 
</div>
<?php 
} else { 
?>
<div class="form-group">
	<label>Your <?php echo $to; ?> account</label>
	<input type="text" class="form-control" name="s_account" id="s_account" onkeyup="changeAccount(this.value);" onkeydown="changeAccount(this.value);"> 
</div>
<?php 
}
?>
<div class="form-group">
	<label>Your email account</label>
	<input type="text" class="form-control" name="s_email" id="s_email" onkeyup="changeEmail(this.value);" onkeydown="changeEmail(this.value);"> 
</div>
<button type="button" id="exchange_button" class="btn btn-primary btn-lg btn-block" onclick="validateForm();"><i class="fa fa-refresh"></i> Become Exchange</button>