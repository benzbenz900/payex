<?php
ob_start();
session_start();
error_reporting(0);
include("../includes/config.php");
$db = new mysqli($CONF['host'], $CONF['user'], $CONF['pass'], $CONF['name']);
if ($db->connect_errno) {
    echo "Failed to connect to MySQL: (" . $db->connect_errno . ") " . $db->connect_error;
}
$db->set_charset("utf8");
$settingsQuery = $db->query("SELECT * FROM settings ORDER BY id DESC LIMIT 1");
$settings = $settingsQuery->fetch_assoc();
include("../includes/functions.php");
$from = protect($_POST['from']);
$to = protect($_POST['to']);
$amount_from = protect($_POST['amount_from']);
$amount_to = protect($_POST['amount_to']);
$currency_from = protect($_POST['currency_from']);
$currency_to = protect($_POST['currency_to']);
$rate = protect($_POST['rate']);
$account = protect($_POST['account']);
$email = protect($_POST['email']);
$u_field_3 = protect($_POST['u_field_3']);
$u_field_4 = protect($_POST['u_field_4']);
$u_field_5 = protect($_POST['u_field_5']);
$u_field_6 = protect($_POST['u_field_6']);
$u_field_7 = protect($_POST['u_field_7']);
$u_field_8 = protect($_POST['u_field_8']);
$u_field_9 = protect($_POST['u_field_9']);
$u_field_10 = protect($_POST['u_field_10']);

if(empty($from) or empty($to) or empty($amount_from) or empty($amount_to) or empty($currency_from) or empty($currency_to) or empty($rate) or empty($email)) { $data['status'] = "error"; $data['msg'] = error("All fields are required."); }
elseif(!is_numeric($amount_from)) { $data['status'] = "error"; $data['msg'] = error("Please enter amount with numbers."); }
elseif($from !== "Bitcoin" && $settings['exchminamount'] > $amount_from) { $data['status'] = "error"; $data['msg'] = error("Minimal amount for exchange is $settings[exchminamount]."); }
elseif(!isValidEmail($email)) { $data['status'] = "error"; $data['msg'] = error("Please enter valid email address."); }
elseif($to == "PayPal" && !isValidEmail($account)) { $data['status'] = "error"; $data['msg'] = error("Please enter valid $to account."); }
elseif($to == "Payeer" && strlen($account)<8) { $data['status'] = "error"; $data['msg'] = error("Please enter valid $to account."); }
elseif($to == "Perfect Money" && strlen($account)<7) { $data['status'] = "error"; $data['msg'] = error("Please enter valid $to account."); }
elseif($to == "AdvCash" && !isValidEmail($account)) { $data['status'] = "error"; $data['msg'] = error("Please enter valid $to account."); }
elseif($to == "OKPay" && strlen($account)<8) { $data['status'] = "error"; $data['msg'] = error("Please enter valid $to account."); }
elseif($to == "Entromoney" && strlen($account)<9) { $data['status'] = "error"; $data['msg'] = error("Please enter valid $to account."); }
elseif($to == "Payza" && !isValodEmail($account)) { $data['status'] = "error"; $data['msg'] = error("Please enter valid $to account."); }
elseif($to == "Bitcoin" && strlen($account)<20) { $data['status'] = "error"; $data['msg'] = error("Please enter valid $to address."); }
elseif($to == "Bank Transfer" && empty($u_field_3)) { $data['status'] = "error"; $data['msg'] = error("Please enter your name."); }
elseif($to == "Bank Transfer" && empty($u_field_4)) { $data['status'] = "error"; $data['msg'] = error("Please enter your location."); }
elseif($to == "Bank Transfer" && empty($u_field_5)) { $data['status'] = "error"; $data['msg'] = error("Please enter your bank name."); }
elseif($to == "Bank Transfer" && empty($u_field_6)) { $data['status'] = "error"; $data['msg'] = error("Please enter your bank account iban."); }
elseif($to == "Bank Transfer" && empty($u_field_7)) { $data['status'] = "error"; $data['msg'] = error("Please enter your bank swift."); }
elseif($to == "Moneygram" && empty($u_field_3)) { $data['status'] = "error"; $data['msg'] = error("Please enter your name."); }
elseif($to == "Moneygram" && empty($u_field_4)) { $data['status'] = "error"; $data['msg'] = error("Please enter your location."); }
elseif($to == "Western union" && empty($u_field_3)) { $data['status'] = "error"; $data['msg'] = error("Please enter your name."); }
elseif($to == "Western union" && empty($u_field_4)) { $data['status'] = "error"; $data['msg'] = error("Please enter your location."); }
else {
	$data['status'] = "success";
	$data['msg'] = "OK";
}
echo json_encode($data);
?>