<?php
ob_start();
session_start();
error_reporting(0);
include("../includes/config.php");
$db = new mysqli($CONF['host'], $CONF['user'], $CONF['pass'], $CONF['name']);
if ($db->connect_errno) {
    echo "Failed to connect to MySQL: (" . $db->connect_errno . ") " . $db->connect_error;
}
$db->set_charset("utf8");
$settingsQuery = $db->query("SELECT * FROM settings ORDER BY id DESC LIMIT 1");
$settings = $settingsQuery->fetch_assoc();
include("../includes/functions.php");
$a_from = protect($_GET['a_from']);
$c_from = protect($_GET['c_from']);
$from = protect($_GET['from']);
$to = protect($_GET['to']);
if(is_numeric($a_from)) {
	$query = $db->query("SELECT * FROM currencies WHERE company_from='$from' and company_to='$to' and currency_from='$c_from' ORDER BY id LIMIT 1"); 
	if($query->num_rows>0) {
		$row = $query->fetch_assoc();
		$currency_to = $row['currency_to'];
		$calculate = $a_from*$row['rate'];
		?>
		<script type="text/javascript" src="<?php echo $settings['url']; ?>assets/homepage/js/jquery-1.11.1.min.js"></script>
	<script type="text/javascript" src="<?php echo $settings['url']; ?>assets/js/script.js"></script>
		<div class="form-group">
			<label>Amount receive</label>
			<div class="input-group">
			  <input type="text" class="form-control" name="a_to" id="a_to" value="<?php echo $calculate; ?>" onkeyup="changeAmountTo(this.value);" onkeydown="changeAmountTo(this.value);" disabled>
			  <div class="input-group-btn" style="width:25%;">
				<select class="form-control" name="c_to" id="c_to" onchange="calculateAmount2();changeCurrencyTo(this.value);">
					<?php
					$query2 = $db->query("SELECT * FROM currencies WHERE company_from='$from' and company_to='$to' and currency_from='$c_from' ORDER BY id");
					if($query2->num_rows>0) {
						while($row2 = $query2->fetch_assoc()) {
							if($currency_to == $row2['currency_to']) {
								echo '<option value="'.$row2[currency_to].'" selected>'.$row2[currency_to].'</option>';
							} else {
								echo '<option value="'.$row2[currency_to].'">'.$row2[currency_to].'</option>';
							}
						}
					} else {
						echo '<option value="">None</option>';
					}
					?>
				</select>
			  </div>
			</div>
		</div>
		<?php
	} else {
		echo 'Our system no have added currecnies for this exchange';
	}
} else {
	echo 'Please enter amount with numbers.';
}
?>