<?php
ob_start();
session_start();
error_reporting(0);
include("../includes/config.php");
$db = new mysqli($CONF['host'], $CONF['user'], $CONF['pass'], $CONF['name']);
if ($db->connect_errno) {
    echo "Failed to connect to MySQL: (" . $db->connect_errno . ") " . $db->connect_error;
}
$db->set_charset("utf8");
$settingsQuery = $db->query("SELECT * FROM settings ORDER BY id DESC LIMIT 1");
$settings = $settingsQuery->fetch_assoc();
include("../includes/functions.php");
$from = protect($_POST['from']);
$to = protect($_POST['to']);
$amount_from = protect($_POST['amount_from']);
$amount_to = protect($_POST['amount_to']);
$currency_from = protect($_POST['currency_from']);
$currency_to = protect($_POST['currency_to']);
$rate = protect($_POST['rate']);
$account = protect($_POST['account']);
$email = protect($_POST['email']);
$u_field_3 = protect($_POST['u_field_3']);
$u_field_4 = protect($_POST['u_field_4']);
$u_field_5 = protect($_POST['u_field_5']);
$u_field_6 = protect($_POST['u_field_6']);
$u_field_7 = protect($_POST['u_field_7']);
$u_field_8 = protect($_POST['u_field_8']);
$u_field_9 = protect($_POST['u_field_9']);
$u_field_10 = protect($_POST['u_field_10']);

if($_SESSION['suid']) {
	$uid = $_SESSION['suid'];
} else {
	$uid = 0;
}

if($_SESSION['refid']) {
	$referral_id = $_SESSION['refid'];
} else {
	$referral_id = 0;
}

$time = time();
$exchange_id = randomHash(6).'-'.randomHash(11).'-'.randomHash(5);
$ip = $_SERVER['REMOTE_ADDR'];

$create = $db->query("INSERT exchanges (uid,cfrom,cto,amount_from,currency_from,amount_to,currency_to,rate,status,created,u_field_1,u_field_2,u_field_3,u_field_4,u_field_5,u_field_6,u_field_7,u_field_8,u_field_9,u_field_10,ip,exchange_id,referral_id) VALUES ('$uid','$from','$to','$amount_from','$currency_from','$amount_to','$currency_to','$rate','1','$time','$account','$email','$u_field_3','$u_field_4','$u_field_5','$u_field_6','$u_field_7','$u_field_8','$u_field_9','$u_field_10','$ip','$exchange_id','$referral_id')") or die(mysqli_error($db));;
$query = $db->query("SELECT * FROM exchanges WHERE exchange_id='$exchange_id'");
$row = $query->fetch_assoc();

$accountQuery = $db->query("SELECT * FROM companies WHERE name='$from'");
$acc = $accountQuery->fetch_assoc();

if($from == "PayPal") {
	include("../includes/paypal_class.php");
	define('EMAIL_ADD', $acc['a_field_1']); // For system notification.
	define('PAYPAL_EMAIL_ADD', $acc['a_field_1']);

	// Setup class
	$p = new paypal_class( ); 				 // initiate an instance of the class.
	$p -> admin_mail = EMAIL_ADD; 
	$this_script = $settings['url']."index.php?a=check_payment&b=paypal";
	$p->add_field('business', PAYPAL_EMAIL_ADD); //don't need add this item. if your set the $p -> paypal_mail.
	$p->add_field('return', $this_script.'&action=success');
	$p->add_field('cancel_return', $this_script.'&action=cancel');
	$p->add_field('notify_url', $this_script.'&action=ipn');
	$p->add_field('item_name', 'Exchange '.$amount_from.' '.$currency_from);
	$p->add_field('item_number', $row['id']);
	$p->add_field('amount', $amount_from);
	$p->add_field('currency_code', $currency_from);
	$p->add_field('cmd', '_xclick');
	$p->add_field('rm', '2');	// Return method = POST
					 
	$p->submit_paypal_post(); // submit the fields to paypal
	$return_msg = '<script type="text/javascript" src="'.$settings[url].'assets/js/jquery-1.9.1.js"></script>';
	$return_msg .= '<script type="text/javascript">$(document).ready(function() { $("#paypal_form").submit(); });</script>';
	$return_msg .= '<center><h2 style="margin-top:15px;"><i class="fa fa-spin fa-spinner fa-3x"></i><br/>Processing...</h2></center>';
	$msubject = '['.$settings[sitename].'] New request for exchange '.$row[exchange_id];
	$mreceiver = $email;
	$message = 'Hi, '.$email.'
You make new request for exchange from '.$from.' to '.$to.' for '.$amount_from.' '.$currency_from.'.
You must make payment before passing 24 hours because when the time expires your request will be not active.
Can complete payment on this link: '.$settings[url].'become_payment/'.$row[id].'
	
If you have some problems please feel free to contact with us on '.$settings[siteemail];
	$headers = 'From: '.$settings[siteemail].'' . "\r\n" .
				'Reply-To: '.$settings[siteemail].'' . "\r\n" .
				'X-Mailer: PHP/' . phpversion();
	mail($mreceiver, $msubject, $message, $headers);
	echo $return_msg;
} elseif($from == "Payeer") {
	$m_shop = $acc['a_field_1'];
	$m_orderid = $row['id'];
	$m_amount = number_format($amount_from, 2, '.', '');
	$m_curr = $currency;
	$desc = 'Exchange '.$amount_from.' '.$currency_from;
	$m_desc = base64_encode($desc);
	$m_key = $acc['a_field_2'];

	$arHash = array(
		$m_shop,
		$m_orderid,
		$m_amount,
		$m_curr,
		$m_desc,
		$m_key
	);
	$sign = strtoupper(hash('sha256', implode(':', $arHash)));
	$return_msg = '<div style="display:none;"><form method="GET" id="payeer_form" action="https://payeer.com/merchant/">
	<input type="hidden" name="m_shop" value="'.$m_shop.'">
	<input type="hidden" name="m_orderid" value="'.$m_orderid.'">
	<input type="hidden" name="m_amount" value="'.$m_amount.'">
	<input type="hidden" name="m_curr" value="'.$m_curr.'">
	<input type="hidden" name="m_desc" value="'.$m_desc.'">
	<input type="hidden" name="m_sign" value="'.$sign.'">
	<!--
	<input type="hidden" name="form[ps]" value="2609">
	<input type="hidden" name="form[curr[2609]]" value="USD">
	-->
	<input type="submit" name="m_process" value="Pay with Payeer" />
	</form></div>';
	$return_msg .= '<script type="text/javascript" src="'.$settings[url].'assets/js/jquery-1.9.1.js"></script>';
	$return_msg .= '<script type="text/javascript">$(document).ready(function() { $("#payeer_form").submit(); });</script>';
	$return_msg .= '<center><h2 style="margin-top:15px;"><i class="fa fa-spin fa-spinner fa-3x"></i><br/>Processing...</h2></center>';
	$msubject = '['.$settings[sitename].'] New request for exchange '.$row[exchange_id];
	$mreceiver = $email;
	$message = 'Hi, '.$email.'
You make new request for exchange from '.$from.' to '.$to.' for '.$amount_from.' '.$currency_from.'.
You must make payment before passing 24 hours because when the time expires your request will be not active.
Can complete payment on this link: '.$settings[url].'become_payment/'.$row[id].'
	
If you have some problems please feel free to contact with us on '.$settings[siteemail];
	$headers = 'From: '.$settings[siteemail].'' . "\r\n" .
				'Reply-To: '.$settings[siteemail].'' . "\r\n" .
				'X-Mailer: PHP/' . phpversion();
	mail($mreceiver, $msubject, $message, $headers);
	echo $return_msg;
} elseif($from == "Perfect Money") {
	$return_msg = '<div style="display:none;">
				<form action="https://perfectmoney.is/api/step1.asp" id="pm_form" method="POST">
					<input type="hidden" name="PAYEE_ACCOUNT" value="'.$acc[a_field_1].'">
					<input type="hidden" name="PAYEE_NAME" value="'.$settings[sitename].'">
					<input type="hidden" name="PAYMENT_ID" value="'.$row[id].'">
					<input type="text"   name="PAYMENT_AMOUNT" value="'.$amount_from.'"><BR>
					<input type="hidden" name="PAYMENT_UNITS" value="'.$currency_from.'">
					<input type="hidden" name="STATUS_URL" value="'.$settings[url].'index.php?a=check_payment&b=perfectmoney&c=status">
					<input type="hidden" name="PAYMENT_URL" value="'.$settings[url].'index.php?a=check_payment&b=perfectmoney&c=complete">
					<input type="hidden" name="PAYMENT_URL_METHOD" value="POST">
					<input type="hidden" name="NOPAYMENT_URL" value="'.$settings[url].'index.php?a=check_payment&b=perfectmoney&c=failed">
					<input type="hidden" name="NOPAYMENT_URL_METHOD" value="POST">
					<input type="hidden" name="SUGGESTED_MEMO" value="">
					<input type="hidden" name="BAGGAGE_FIELDS" value="IDENT"><br>
					<input type="submit" name="PAYMENT_METHOD" value="Pay Now!" class="tabeladugme"><br><br>
					</form></div>';
	$return_msg .= '<script type="text/javascript" src="'.$settings[url].'assets/js/jquery-1.9.1.js"></script>';
	$return_msg .= '<script type="text/javascript">$(document).ready(function() { $("#pm_form").submit(); });</script>';
	$return_msg .= '<center><h2 style="margin-top:15px;"><i class="fa fa-spin fa-spinner fa-3x"></i><br/>Processing...</h2></center>';
	$msubject = '['.$settings[sitename].'] New request for exchange '.$row[exchange_id];
	$mreceiver = $email;
	$message = 'Hi, '.$email.'
You make new request for exchange from '.$from.' to '.$to.' for '.$amount_from.' '.$currency_from.'.
You must make payment before passing 24 hours because when the time expires your request will be not active.
Can complete payment on this link: '.$settings[url].'become_payment/'.$row[id].'
	
If you have some problems please feel free to contact with us on '.$settings[siteemail];
	$headers = 'From: '.$settings[siteemail].'' . "\r\n" .
				'Reply-To: '.$settings[siteemail].'' . "\r\n" .
				'X-Mailer: PHP/' . phpversion();
	mail($mreceiver, $msubject, $message, $headers);
	echo $return_msg;
} elseif($from == "AdvCash") {
	$arHash = array(
		$acc[a_field_1],
		$settings[sitename],
		$amount,
		$currency,
		$acc[a_field_2],
		$row[id]
	);
	$sign = strtoupper(hash('sha256', implode(':', $arHash)));
	$return_msg = '<div style="display:none;">
				<form method="GET" id="advcash_form" action="https://wallet.advcash.com/sci/">
				<input type="hidden" name="ac_account_email" value="'.$acc[a_field_1].'">
				<input type="hidden" name="ac_sci_name" value="'.$settings[sitename].'">
				<input type="hidden" name="ac_amount" value="'.$amount_from.'">
				<input type="hidden" name="ac_currency" value="'.$currency_from.'">
				<input type="hidden" name="ac_order_id" value="'.$row[id].'">
				<input type="hidden" name="ac_sign"
				value="'.$sign.'">
				<input type="hidden" name="ac_success_url" value="'.$settings[url].'index.php?a=check_payment&b=advcash&c=success" />
				 <input type="hidden" name="ac_success_url_method" value="GET" />
				 <input type="hidden" name="ac_fail_url" value="'.$settings[url].'index.php?a=check_payment&b=advcash&c=fail" />
				 <input type="hidden" name="ac_fail_url_method" value="GET" />
				 <input type="hidden" name="ac_status_url" value="'.$settings[url].'index.php?a=check_payment&b=advcash&c=status" />
				 <input type="hidden" name="ac_status_url_method" value="GET" />
				<input type="hidden" name="ac_comments" value="Exchange '.$amount_from.' '.$currency_from.'">
				</form>
				</div>';
	$return_msg .= '<script type="text/javascript" src="'.$settings[url].'assets/js/jquery-1.9.1.js"></script>';
	$return_msg .= '<script type="text/javascript">$(document).ready(function() { $("#advcash_form").submit(); });</script>';
	$return_msg .= '<center><h2 style="margin-top:15px;"><i class="fa fa-spin fa-spinner fa-3x"></i><br/>Processing...</h2></center>';
	$msubject = '['.$settings[sitename].'] New request for exchange '.$row[exchange_id];
	$mreceiver = $email;
	$message = 'Hi, '.$email.'
You make new request for exchange from '.$from.' to '.$to.' for '.$amount_from.' '.$currency_from.'.
You must make payment before passing 24 hours because when the time expires your request will be not active.
Can complete payment on this link: '.$settings[url].'become_payment/'.$row[id].'
	
If you have some problems please feel free to contact with us on '.$settings[siteemail];
	$headers = 'From: '.$settings[siteemail].'' . "\r\n" .
				'Reply-To: '.$settings[siteemail].'' . "\r\n" .
				'X-Mailer: PHP/' . phpversion();
	mail($mreceiver, $msubject, $message, $headers);
	echo $return_msg;
} elseif($from == "OKPay") {
	$return_msg = '<form  method="post" id="okpay_form" action="https://checkout.okpay.com/">
				   <input type="hidden" name="ok_receiver" value="'.$acc[a_field_1].'"/>
				   <input type="hidden" name="ok_item_1_name" value="Exchange '.$amount_from.' '.$currency_from.'"/>
				   <input type="hidden" name="ok_item_1_price" value="'.$amount_from.'"/>
				   <input type="hidden" name="ok_item_1_id" value="'.$row[id].'"/>
				   <input type="hidden" name="ok_currency" value="'.$currency_from.'"/>
				</form>';
	$return_msg .= '<script type="text/javascript" src="'.$settings[url].'assets/js/jquery-1.9.1.js"></script>';
	$return_msg .= '<script type="text/javascript">$(document).ready(function() { $("#okpay_form").submit(); });</script>';
	$return_msg .= '<center><h2 style="margin-top:15px;"><i class="fa fa-spin fa-spinner fa-3x"></i><br/>Processing...</h2></center>';
	$msubject = '['.$settings[sitename].'] New request for exchange '.$row[exchange_id];
	$mreceiver = $email;
	$message = 'Hi, '.$email.'
You make new request for exchange from '.$from.' to '.$to.' for '.$amount_from.' '.$currency_from.'.
You must make payment before passing 24 hours because when the time expires your request will be not active.
Can complete payment on this link: '.$settings[url].'become_payment/'.$row[id].'
	
If you have some problems please feel free to contact with us on '.$settings[siteemail];
	$headers = 'From: '.$settings[siteemail].'' . "\r\n" .
				'Reply-To: '.$settings[siteemail].'' . "\r\n" .
				'X-Mailer: PHP/' . phpversion();
	mail($mreceiver, $msubject, $message, $headers);
	echo $return_msg;
} elseif($from == "Entromoney") {
	include("../includes/entromoney.php");
	$config = array();
	$config['sci_user'] = $acc['a_field_1'];
	$config['sci_id'] 	= $acc['a_field_2'];
	$config['sci_pass'] = $acc['a_field_3'];
	$config['receiver'] = $acc['a_field_4'];

	// Call lib sci
	try {
		$sci = new Paygate_Sci($config);
	}
	catch (Paygate_Exception $e) {
		exit($e->getMessage());
	}
	
	$return_msg = '';
	$input = array();
	$input['sci_user'] 		= $config['sci_user'];
	$input['sci_id'] 		= $config['sci_id'];
	$input['receiver'] 		= $config['receiver'];
	$input['amount'] 		= $amount_from;
	$input['desc'] 			= 'Exchange '.$amount_from.' '.$currency_from;
	$input['payment_id'] 	= $row['id'];
	$input['up_1'] 			= 'user_param_1';
	$input['up_2'] 			= 'user_param_2';
	$input['up_3'] 			= 'user_param_3';
	$input['up_4'] 			= 'user_param_4';
	$input['up_5'] 			= 'user_param_5';
	$input['url_status'] 	= $settings[url].'index.php?a=check_payment&b=entromoney&c=status';
	$input['url_success'] 	= $settings[url].'index.php?a=check_payment&b=entromoney&c=success';
	$input['url_fail'] 		= $settings[url].'index.php?a=check_payment&b=entromoney&c=fail';

	// Create hash
	$input['hash']			= $sci->create_hash($input);
	?>
	<form action="<?php echo Paygate_Sci::URL_SCI; ?>" id="entromoney_form" method="post">
		<?php foreach ($input as $p => $v): ?>
			<input type="hidden" name="<?php echo $p; ?>" value="<?php echo $v; ?>">
		<?php endforeach; ?>
	</form>
	<?php
	$return_msg .= '<script type="text/javascript" src="'.$settings[url].'assets/js/jquery-1.9.1.js"></script>';
	$return_msg .= '<script type="text/javascript">$(document).ready(function() { $("#entromoney_form").submit(); });</script>';
	$return_msg .= '<center><h2 style="margin-top:15px;"><i class="fa fa-spin fa-spinner fa-3x"></i><br/>Processing...</h2></center>';
	$msubject = '['.$settings[sitename].'] New request for exchange '.$row[exchange_id];
	$mreceiver = $email;
	$message = 'Hi, '.$email.'
You make new request for exchange from '.$from.' to '.$to.' for '.$amount_from.' '.$currency_from.'.
You must make payment before passing 24 hours because when the time expires your request will be not active.
Can complete payment on this link: '.$settings[url].'become_payment/'.$row[id].'
	
If you have some problems please feel free to contact with us on '.$settings[siteemail];
	$headers = 'From: '.$settings[siteemail].'' . "\r\n" .
				'Reply-To: '.$settings[siteemail].'' . "\r\n" .
				'X-Mailer: PHP/' . phpversion();
	mail($mreceiver, $msubject, $message, $headers);
	echo $return_msg;
} elseif($from == "Payza") {
	$return_msg = '<form method="post" id="payza_form" action="https://secure.payza.com/checkout" >
				<input type="hidden" name="ap_merchant" value="'.$acc[a_field_1].'"/>
				<input type="hidden" name="ap_purchasetype" value="item-goods"/>
				<input type="hidden" name="ap_itemname" value="Exchange '.$amount_from.' '.$currency_from.'"/>
				<input type="hidden" name="ap_amount" value="'.$amount_from.'"/>
				<input type="hidden" name="ap_currency" value="'.$currency_from.'"/>

				<input type="hidden" name="ap_quantity" value="1"/>
				<input type="hidden" name="ap_itemcode" value="'.$row[id].'"/>
				<input type="hidden" name="ap_description" value=""/>
				<input type="hidden" name="ap_returnurl" value="'.$settings[url].'index.php?a=check_payment&b=payza&c=results"/>
				<input type="hidden" name="ap_cancelurl" value="'.$settings[url].'index.php?a=check_payment&b=payza&c=cancel"/>

				<input type="hidden" name="ap_taxamount" value="0"/>
				<input type="hidden" name="ap_additionalcharges" value="0"/>
				<input type="hidden" name="ap_shippingcharges" value="0"/> 

				<input type="hidden" name="ap_discountamount" value="0"/> 
				<input type="hidden" name="apc_1" value="Blue"/>

			</form>';
	$return_msg .= '<script type="text/javascript" src="'.$settings[url].'assets/js/jquery-1.9.1.js"></script>';
	$return_msg .= '<script type="text/javascript">$(document).ready(function() { $("#payza_form").submit(); });</script>';
	$return_msg .= '<center><h2 style="margin-top:15px;"><i class="fa fa-spin fa-spinner fa-3x"></i><br/>Processing...</h2></center>';
	$msubject = '['.$settings[sitename].'] New request for exchange '.$row[exchange_id];
	$mreceiver = $email;
	$message = 'Hi, '.$email.'
You make new request for exchange from '.$from.' to '.$to.' for '.$amount_from.' '.$currency_from.'.
You must make payment before passing 24 hours because when the time expires your request will be not active.
Can complete payment on this link: '.$settings[url].'become_payment/'.$row[id].'
	
If you have some problems please feel free to contact with us on '.$settings[siteemail];
	$headers = 'From: '.$settings[siteemail].'' . "\r\n" .
				'Reply-To: '.$settings[siteemail].'' . "\r\n" .
				'X-Mailer: PHP/' . phpversion();
	mail($mreceiver, $msubject, $message, $headers);
	echo $return_msg;
} elseif($from == "Bitcoin") {
	?>
	<center>
		<img src="https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=<?php echo $acc['a_field_1']; ?>&choe=UTF-8"><br/>
		Send <?php echo $amount_from.' '.$currency_from; ?> to Bitcoin address:<br/>
		<b><?php echo $acc['a_field_1']; ?></b><br/>
		After making the transfer, please let us know <a href="<?php echo $settings['url']; ?>page/contact"><b><?php echo $settings['siteemail']; ?></b></a> email so we can continue the exchange. Please indicate the <b>Transaction ID</b> in message.
	</center>
	<?php
} elseif($from == "Bank Transfer") {
	?>
	<h3>Exchange:<br/>#<?php echo $row['exchange_id']; ?></h3>
	<h4>Bank Details:</h4>
	<b>Bank account owner name:</b> <?php echo $acc['a_field_1']; ?><br/>
	<b>Bank name:</b> <?php echo $acc['a_field_2']; ?><br/>
	<b>Bank location:</b> <?php echo $acc['a_field_3']; ?><br/>
	<b>Bank account iban:</b> <?php echo $acc['a_field_4']; ?><br/>
	<b>Bank swift:</b> <?php echo $acc['a_field_5']; ?><br/>
	<b>Description:</b> Exchange <?php echo $amount_from.' '.$currency_from; ?><br/><br/>
	After making the transfer, please let us know <a href="<?php echo $settings['url']; ?>page/contact"><b><?php echo $settings['siteemail']; ?></b></a> email so we can continue the exchange. Please indicate the <b>Exchange ID</b> and <b>Transaction id</b> in message.
	<?php
} elseif($from == "Moneygram") {
	?>
	<h3>Exchange:<br/>#<?php echo $row['exchange_id']; ?></h3>
	<h4>Moneygram Details:</h4>
	<b>Name:</b> <?php echo $acc['a_field_1']; ?><br/>
	<b>Location:</b> <?php echo $acc['a_field_2']; ?><br/>
	<b>Description:</b> Exchange <?php echo $amount_from.' '.$currency_from; ?><br/><br/>
	After making the transfer, please let us know <a href="<?php echo $settings['url']; ?>page/contact"><b><?php echo $settings['siteemail']; ?></b></a> email so we can continue the exchange. Please indicate the <b>Exchange ID</b> and <b>Moneygram PIN</b> in message.
	<?php
} elseif($from == "Western union") {
	?>
	<h3>Exchange:<br/>#<?php echo $row['exchange_id']; ?></h3>
	<h4>Western union Details:</h4>
	<b>Name:</b> <?php echo $acc['a_field_1']; ?><br/>
	<b>Location:</b> <?php echo $acc['a_field_2']; ?><br/>
	<b>Description:</b> Exchange <?php echo $amount_from.' '.$currency_from; ?><br/><br/>
	After making the transfer, please let us know <a href="<?php echo $settings['url']; ?>page/contact"><b><?php echo $settings['siteemail']; ?></b></a> email so we can continue the exchange. Please indicate the <b>Exchange ID</b> and <b>Western union PIN</b> in message.
	<?php
} else {
	echo error("Something wrong.. Please try again.");
}
?>