<?php
if(checkSession()) {
	header("Location: $settings[url]");
}
?>

<div class="container">

	<div class="row" style="margin-top:50px;margin-bottom:50px;">
		<div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
			<form role="form" action="" method="POST">
				<fieldset>
					<h2><?php echo $lang['forgot_password']; ?></h2>
					<hr class="colorgraph">
					<?php
					if(isset($_POST['btn_reset'])) {
						$email = protect($_POST['email']);
						$check = $db->query("SELECT * FROM users WHERE email='$email'");
						if($check->num_rows>0) {
							$row = $check->fetch_assoc();
							$rand = randomHash(10);
							$update = $db->query("UPDATE users SET password_recovery='$rand' WHERE id='$row[id]'");
							$link=$settings['url']."password-recovery/$rand";
							$subject = $settings[sitename].' - Password Recovery';
							$message = 'Click on this link to change your password:
'.$link.'
This email was automatically generated.';
							mail($row['email'], $subject, $message,  "FROM: $settings[sitename]  <$settings[siteemail]>");
							echo success($lang['success_1']);
						} else {
							echo error($lang['error_3']);
						}
					}
					?>
					<div class="form-group">
						<input type="text" name="email" id="email" class="form-control input-lg" placeholder="<?php echo $lang['email_address']; ?>">
					</div>
					<hr class="colorgraph">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12">
							<input type="submit" class="btn btn-lg btn-success btn-block" name="btn_reset" value="<?php echo $lang['btn_1']; ?>">
						</div>
					</div>
				</fieldset>
			</form>
		</div>
	</div>

</div>