	<div class="fullpagecover">
		<div class="container">
			<div class="row" style="padding-top:50px;padding-bottom:50px;">
				<div class="col-md-4">
						<h3 style="color:#fff;">Send</h3>
						<div class="list-group" id="send_list">
						  <?php
						  $query = $db->query("SELECT * FROM companies WHERE allow_send='1' ORDER BY id");
						  if($query->num_rows>0) {
							while($row = $query->fetch_assoc()) {
								echo '<a href="javascript:void(0);" class="list-group-item">'.$row[name].'</a>';
							}
						  } else {
							echo '<a href="#" class="list-group-item">No companies</a>';
						  }
						  ?>
						</div>
		          </div>
				  <div class="col-md-4">
						<div class="panel panel-default" id="exchange_results" style="margin-top:57px;display:none;">
							<div class="panel-body">
								<div id="loader" style="text-align:center;padding-top:10px; display:none;">
									<h3><i class="fa fa-spin fa-spinner"></i> Loading...</h3>
								</div>
								<div id="results">
								
								</div>
							</div>
						</div>
						
						<div style="margin-top:70px; text-align:center;color:#fff;" id="exchange_direction">
							<center>
								<i class="fa fa-exchange fa-4x" style="text-align:center;"></i><br><br>
								select exchange direction..
							</center>
						</div>
						
						<div style="display:none;">
							<form id="exchange_form">
								<input type="hidden" name="from" id="from">
								<input type="hidden" name="to" id="to">
								<input type="hidden" name="amount_from" id="amount_from">
								<input type="hidden" name="currency_from" id="currency_from">
								<input type="hidden" name="amount_to" id="amount_to">
								<input type="hidden" name="currency_to" id="currency_to">
								<input type="hidden" name="rate" id="rate">
								<input type="hidden" name="account" id="account">
								<input type="hidden" name="email" id="email">
								<input type="hidden" name="u_field_3" id="u_field_3">
								<input type="hidden" name="u_field_4" id="u_field_4">
								<input type="hidden" name="u_field_5" id="u_field_5">
								<input type="hidden" name="u_field_6" id="u_field_6">
								<input type="hidden" name="u_field_7" id="u_field_7">
								<input type="hidden" name="u_field_8" id="u_field_8">
								<input type="hidden" name="u_field_9" id="u_field_9">
								<input type="hidden" name="u_field_10" id="u_field_10">
							</form>
						</div>
				  </div>
		          <div class="col-md-4">
						<h3 style="color:#fff;">Receive</h3>
						<div class="list-group" id="receive_list">
						  <?php
						  $query = $db->query("SELECT * FROM companies WHERE allow_send='1' ORDER BY id");
						  if($query->num_rows>0) {
							while($row = $query->fetch_assoc()) {
								echo '<a href="javascript:void(0);" class="list-group-item">'.$row[name].'</a>';
							}
						  } else {
							echo '<a href="#" class="list-group-item">No companies</a>';
						  }
						  ?>
						</div>
		          </div>
			</div>	
		</div>
	</div>
	
	<div class="container">
		<div class="row" style="padding-top:20px;padding-bottom:20px;">
			<div class="col-sm-6 col-md-6 col-lg-6">
				<h3><?php echo $lang['why']; ?> <?php echo $settings['sitename']; ?>?</h3>
				<div class="media">
				  <div class="media-left">
					<i class="fa fa-support fa-3x"></i>
				  </div>
				  <div class="media-body">
					<h4 class="media-heading"><?php echo $lang['why_ans_1']; ?></h4>
				  </div>
				</div>
				<br>
				<div class="media">
				  <div class="media-left">
					<i class="fa fa-line-chart fa-3x"></i>
				  </div>
				  <div class="media-body">
					<h4 class="media-heading"><?php echo $lang['why_ans_2']; ?></h4>
				  </div>
				</div>
				<br>
				<div class="media">
				  <div class="media-left">
					<i class="fa fa-clock-o fa-3x"></i>
				  </div>
				  <div class="media-body">
					<h4 class="media-heading"><?php echo $lang['why_ans_3']; ?></h4>
				  </div>
				</div>
				<br>
				<div class="media">
				  <div class="media-left">
					<i class="fa fa-money fa-3x"></i>
				  </div>
				  <div class="media-body">
					<h4 class="media-heading"><?php echo $lang['why_ans_4']; ?></h4>
				  </div>
				</div>
			</div>
			<div class="col-sm-6 col-md-6 col-lg-6">
				<h3><?php echo $lang['clients_testimonials']; ?></h3>
					<div class="row">
						<div class="col-md-12">
							<div class="quote"><i class="fa fa-quote-left fa-4x"></i></div>
							<div class="carousel slide" id="fade-quote-carousel" data-ride="carousel" data-interval="5000">
							  <!-- Carousel indicators -->
							  <ol class="carousel-indicators">
								<?php
								$i=1;
								$get_tests = $db->query("SELECT * FROM testimonials WHERE status='1' ORDER BY RAND() LIMIT 5");
								if($get_tests->num_rows>0) {
									while($get = $get_tests->fetch_assoc()) {
										$gets[] = $get;
									}
									foreach($gets as $get) {
										?><li data-target="#fade-quote-carousel" data-slide-to="<?php echo $i; ?>" class="<?php if($i == "1") { echo 'active'; } ?>"></li><?php
										$i++;
									}
								}
								?>
							  </ol>
							  <!-- Carousel items -->
							  <div class="carousel-inner">
								<?php
								$t=1;
								if($get_tests->num_rows>0) {
									foreach($gets as $get) {
										?>
										<div class="<?php if($t == "1") { echo 'active'; } ?> item">
											<blockquote>
												<p><?php echo $get['content']; ?><br/><i class="text-muted">- <?php echo $lang['from']; ?> <?php echo idinfo($get['uid'],"name"); ?></i></p>
											</blockquote>
											<div class="profile-circle" style="background-color: rgba(0,0,0,.2);"></div>
										</div>
										<?php
										$t++;
									}
								}
								?>
							  </div>
							</div>
						</div>							
					</div>
			</div>
		</div>
	</div>