	<div class="container">
		<div class="row" style="padding-top:20px;padding-bottom:20px;">
			<div class="col-sm-12 col-md-12 col-lg-12">
			<h3><?php echo $lang['payment_status']; ?></h3>
			<?php
			$b = protect($_GET['b']); 
			if($b == "paypal") {
				include("ipn/paypal.php");
			} elseif($b == "skrill") {
				include("ipn/skrill.php");
			} elseif($b == "webmoney") {	
				include("ipn/webmoney.php");
			} elseif($b == "payeer") {
				include("ipn/payeer.php");
			} elseif($b == "perfectmoney") {
				include("ipn/perfectmoney.php");
			} elseif($b == "advcash") {
				include("ipn/advcash.php");
			} elseif($b == "okpay") {
				include("ipn/okpay.php");
			} elseif($b == "entromoney") {
				include("ipn/entromoney.php");
			} elseif($b == "bitcoin") {
				include("ipn/bitcoin.php");
			} elseif($b == "payza") {
				include("ipn/payza.php");
			} else {
				echo error($lang['error_2']);
			}
			?>
			</div>
		</div>
	</div>