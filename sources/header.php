<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $settings['title']; ?></title>
	<meta name="keywords" content="<?php echo $settings['keywords']; ?>" />
	<meta name="description" content="<?php echo $settings['description']; ?>" />
    <link href="<?php echo $settings['url']; ?>assets/css/united.boot.css" rel="stylesheet">
	<link href="<?php echo $settings['url']; ?>assets/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo $settings['url']; ?>assets/css/style.css" rel="stylesheet">
	<script type="text/javascript" src="<?php echo $settings['url']; ?>assets/js/jquery-1.11.1.min.js"></script>
	<script type="text/javascript" src="<?php echo $settings['url']; ?>assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo $settings['url']; ?>assets/js/script.js"></script>
	<style type="text/css">
	body { padding-top: 50px; }
	.footer_container {
    background: #CACACA;
}
.text-center {
    text-align: center;
    text-align: -webkit-center;
}
	</style>
</head>
<body>
<!-- By lnwPHP.in.th -->
	<!-- Static navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo $settings['url']; ?>"><i class="fa fa-exchange"></i> <?php echo $settings['sitename']; ?></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="<?php echo $settings['url']; ?>"><?php echo $lang['exchange']; ?></a></li>
			<li><a href="<?php echo $settings['url']; ?>testimonials"><?php echo $lang['testimonials']; ?></a></li>
			<li><a href="<?php echo $settings['url']; ?>page/about"><?php echo $lang['about']; ?></a></li>
			<li><a href="<?php echo $settings['url']; ?>page/contact"><?php echo $lang['contact']; ?></a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
			<?php if(checkSession()) { ?>
			 <li class="dropdown">
			  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user"></i> <?php echo $_SESSION['susername']; ?> <span class="caret"></span></a>
			  <ul class="dropdown-menu">
				<li><a href="<?php echo $settings['url']; ?>account/exchanges"><?php echo $lang['exchanges']; ?></a></li>
				<li><a href="<?php echo $settings['url']; ?>account/wallet">Wallet</a></li>
				<li><a href="<?php echo $settings['url']; ?>account/deposit">Deposit</a></li>
				<li><a href="<?php echo $settings['url']; ?>account/referrals"><?php echo $lang['referrals']; ?></a></li>
				<li><a href="<?php echo $settings['url']; ?>account/withdrawals"><?php echo $lang['withdrawals']; ?></a></li>
				<li><a href="<?php echo $settings['url']; ?>account/settings"><?php echo $lang['settings']; ?></a></li>
				<li role="separator" class="divider"></li>
				<li><a href="<?php echo $settings['url']; ?>logout"><?php echo $lang['logout']; ?></a></li>
			  </ul>
			</li>
			<?php } else { ?>
            <li><a href="<?php echo $settings['url']; ?>register"><?php echo $lang['create_account']; ?></a></li>
            <li><a href="<?php echo $settings['url']; ?>login"><?php echo $lang['login']; ?></a></li>
			<?php } ?>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
	