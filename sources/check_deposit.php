<?php
if(!checkSession()) {
	$redirect = $settings['url']."login";
	header("Location:$redirect");
}
?>
	<div class="container">
		<div class="row" style="padding-top:20px;padding-bottom:20px;">
			<div class="col-sm-12 col-md-12 col-lg-12">
			<h3><?php echo $lang['payment_status']; ?></h3>
			<?php
			$b = protect($_GET['b']); 
			if($b == "paypal") {
				include("ipn_deposit/paypal.php");
			} elseif($b == "payeer") {
				include("ipn_deposit/payeer.php");
			} elseif($b == "perfectmoney") {
				include("ipn_deposit/perfectmoney.php");
			} elseif($b == "advcash") {
				include("ipn_deposit/advcash.php");
			} elseif($b == "okpay") {
				include("ipn_deposit/okpay.php");
			} elseif($b == "entromoney") {
				include("ipn_deposit/entromoney.php");
			} elseif($b == "payza") {
				include("ipn_deposit/payza.php");
			} else {
				echo error($lang['error_2']);
			}
			?>
			</div>
		</div>
	</div>