<div class="jumbotron">
	<div class="container text-center">
		<img class="img-responsive" src="<?php echo $settings['url']; ?>assets/imgs/lnwphpthlogo.png">
	</div>
	<hr>
	<h2 class="text-center">เว็บตัวอย่าง สำหรับผู้สนใจ ติดต่อ <a href="https://www.lnwphp.in.th">www.lnwphp.in.th</a></h2>
	<div class="container">
		<div class="row" style="padding-top:50px;padding-bottom:50px;">
			<div class="col-sm-12 col-md-12 col-lg-12" id="exchange_results"></div>
			<form id="exchange_form">
				<div class="col-sm-5 col-md-5 col-lg-5">
					<div class="panel">
						<div class="panel-body">
							<div class="form-group">
								<label><?php echo $lang['send_from']; ?></label>
								<select class="form-control input-lg" name="from" id="send_list" onChange="getReceiveList(this.value);">
									<option value=""></option>
									<?php
									$send_list = $db->query("SELECT * FROM companies WHERE allow_send='1' ORDER BY id");
									if($send_list->num_rows>0) {
										while($send = $send_list->fetch_assoc()) {
											echo '<option value="'.$send[name].'">'.$send[name].'</option>';
										}
									}
									?>
								</select>
							</div>
							<div class="form-group">
								<label><?php echo $lang['receive_to']; ?></label>
								<select class="form-control input-lg" name="to" id="receive_list" onChange="getCurrenciesList();">
								</select>
							</div>
							<div class="form-group">
								<label><?php echo $lang['amount']; ?></label>
								<div class="input-group">
									<input type="text" class="form-control input-lg" name="amount_from" id="amount_from" placeholder="Eg: 100">
									<span class="input-group-btn">
										<select class="form-control input-lg" style="min-width:100px;" name="currency_from" id="currency_from_list">
											<option value="">USD</option>
											<option value="">EUR</option>
										</select>
									</span>
								</div>
							</div>
							<button type="button" id="exchange_button" onclick="generateForm();" class="btn btn-primary btn-block btn-lg"><i class="fa fa-refresh"></i> <?php echo $lang['btn_10']; ?></button>

						</div>
					</div>
				</div>
				<div class="col-sm-2 col-md-2 col-lg-2"></div>
				<div class="col-sm-5 col-md-5 col-lg-5">
					<div class="panel">
						<div class="panel-body" id="exchange_status">
							<div style="margin-top:105px;margin-bottom:105px;">
								<center>
									<i class="fa fa-exchange fa-4x" style="text-align:center;"></i><br><br>
									<?php echo $lang['select_exchange']; ?>
								</center>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="container">
	<div class="row" style="padding-top:20px;padding-bottom:20px;">
		<div class="col-sm-12 col-md-12 col-lg-12">
			<h3><?php echo $lang['clients_testimonials']; ?></h3>
			<div class="row">
				<div class="col-md-12">
					<div class="quote"><i class="fa fa-quote-left fa-4x"></i></div>
					<div class="carousel slide" id="fade-quote-carousel" data-ride="carousel" data-interval="5000">
						<!-- Carousel indicators -->
						<ol class="carousel-indicators">
							<?php
							$i=1;
							$get_tests = $db->query("SELECT * FROM testimonials WHERE status='1' ORDER BY RAND() LIMIT 5");
							if($get_tests->num_rows>0) {
								while($get = $get_tests->fetch_assoc()) {
									$gets[] = $get;
								}
								foreach($gets as $get) {
									?><li data-target="#fade-quote-carousel" data-slide-to="<?php echo $i; ?>" class="<?php if($i == "1") { echo 'active'; } ?>"></li><?php
									$i++;
								}
							}
							?>
						</ol>
						<!-- Carousel items -->
						<div class="carousel-inner">
							<?php
							$t=1;
							if($get_tests->num_rows>0) {
								foreach($gets as $get) {
									?>
									<div class="<?php if($t == "1") { echo 'active'; } ?> item">
										<blockquote>
											<p><?php echo $get['content']; ?><br/><i class="text-muted">- <?php echo $lang['from']; ?> <?php echo idinfo($get['uid'],"name"); ?></i></p>
										</blockquote>
										<div class="profile-circle" style="background-color: rgba(0,0,0,.2);"></div>
									</div>
									<?php
									$t++;
								}
							}
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row" style="padding-top:20px;padding-bottom:20px;">
		<div class="col-sm-6 col-md-6 col-lg-6">
			<h3><?php echo $lang['why']; ?> <?php echo $settings['sitename']; ?>?</h3>
			<div class="media">
				<div class="media-left">
					<i class="fa fa-support fa-3x"></i>
				</div>
				<div class="media-body">
					<h4 class="media-heading"><?php echo $lang['why_ans_1']; ?></h4>
				</div>
			</div>
			<br>
			<div class="media">
				<div class="media-left">
					<i class="fa fa-line-chart fa-3x"></i>
				</div>
				<div class="media-body">
					<h4 class="media-heading"><?php echo $lang['why_ans_2']; ?></h4>
				</div>
			</div>
			<br>
			<div class="media">
				<div class="media-left">
					<i class="fa fa-clock-o fa-3x"></i>
				</div>
				<div class="media-body">
					<h4 class="media-heading"><?php echo $lang['why_ans_3']; ?></h4>
				</div>
			</div>
			<br>
			<div class="media">
				<div class="media-left">
					<i class="fa fa-money fa-3x"></i>
				</div>
				<div class="media-body">
					<h4 class="media-heading"><?php echo $lang['why_ans_4']; ?></h4>
				</div>
			</div>
		</div>
		<div class="col-sm-6 col-md-6 col-lg-6">
			<h3>Exchange rates</h3>
			<div class="row">
				<div class="col-md-12">
					<div id="box_rates">
						<table class="table table-striped">
							<thead>
								<tr>
									<th width="80%">จาก - เป็น</th>
									<th width="20%">อัตตรา</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$currenciesQuery = $db->query("SELECT * FROM currencies ORDER BY id");
								if($currenciesQuery->num_rows>0) {
									while($c = $currenciesQuery->fetch_assoc()) {
										if($c['currency_from'] == 'THB'){
											$c['rate'] = round(1 / $c['rate'],1);
										}
										echo '<tr>
										<td>'.getIcon($c['company_from']).' '.$c['company_from'].' ('.$c['currency_from'].') to '.getIcon($c['company_to']).' '.$c['company_to'].' ('.$c['currency_to'].')</td>
										<td>'.$c['rate'].' (THB)</td>
									</tr>';
								}
							} else {
								echo '<tr><td colspan="2">No have added currencies</td></tr>';
							}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
</div>