<div class="container">

	<div class="row" style="margin-top:20px;margin-bottom:20px;">
		
		<div class="col-sm-12 col-md-12 col-lg-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<h3><?php echo $lang['testimonials']; ?> <?php
					if(checkSession()) {
						$check_query = $db->query("SELECT * FROM exchanges WHERE uid='$_SESSION[suid]' and status='5'");
						if($check_query->num_rows>0) {
							$check_test = $db->query("SELECT * FROM testimonials WHERE uid='$_SESSION[suid]'");
							if($check_test->num_rows==0) {
								echo '<span class="pull-right"><a href="'.$settings[url].'submit-testimonial"><i class="fa fa-plus"></i> '.$lang[submit_testimonial].'</a></span>';
							}
						}
					}
					?>
					</h3>
					<div class="row">
						<?php
						$query = $db->query("SELECT * FROM testimonials WHERE status='1' ORDER BY id");
						if($query->num_rows>0) {
							while($row = $query->fetch_assoc()) {
								?>
								<div class="col-sm-6 col-md-6 col-lg-6">
									<blockquote>
									  <p><?php echo $row['content']; ?></p>
									  <footer><cite><?php echo idinfo($row['uid'],"name"); ?> (<?php echo idinfo($row['uid'],"username"); ?>)</cite></footer>
									</blockquote>
								</div>
								<?php
							}
						}
						?>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>