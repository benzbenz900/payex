<?php
$b = protect($_GET['b']);
?>

<div class="container">

	<div class="row" style="margin-top:20px;margin-bottom:20px;">
		<div class="col-sm-3 col-md-3 col-lg-3">
			<div class="panel panel-default">
				<div class="panel-body">
					<ul class="nav nav-pills nav-stacked">
						<li <?php if($b == "privacy-policy") { echo 'class="active"'; } ?>><a href="<?php echo $settings['url']; ?>page/privacy-policy"><?php echo $lang['privacy_policy']; ?></a></li>
						<li <?php if($b == "terms-of-service") { echo 'class="active"'; } ?>><a href="<?php echo $settings['url']; ?>page/terms-of-service"><?php echo $lang['terms_of_service']; ?></a></li>
						<li <?php if($b == "faq") { echo 'class="active"'; } ?>><a href="<?php echo $settings['url']; ?>page/faq">FAQ</a></li>
						<li <?php if($b == "about") { echo 'class="active"'; } ?>><a href="<?php echo $settings['url']; ?>page/about"><?php echo $lang['about']; ?></a></li>
						<li <?php if($b == "contact") { echo 'class="active"'; } ?>><a href="<?php echo $settings['url']; ?>page/contact"><?php echo $lang['contact']; ?></a></li>
					</ul>
				</div>
			</div>
		</div>
		
		<div class="col-sm-9 col-md-9 col-lg-9">
			<div class="panel panel-default">
				<div class="panel-body">
					<?php
					if($b == "privacy-policy") {
						include("page/privacy-policy.php");
					} elseif($b == "terms-of-service") {
						include("page/terms-of-service.php");
					} elseif($b == "faq") {
						include("page/faq.php");
					} elseif($b == "about") {
						include("page/about.php");
					} elseif($b == "contact") {
						include("page/contact.php");
					} else {
						header("Location: $settings[url]");
					}
					?>
				</div>
			</div>
		</div>
	</div>

</div>