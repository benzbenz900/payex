<?php
if(checkSession()) {
	header("Location: $settings[url]");
}
$hash = protect($_GET['hash']);
$query = $db->query("SELECT * FROM users WHERE password_recovery='$hash'");
if($query->num_rows==0) { header("Location: $settings[url]"); }
$row = $query->fetch_assoc();
?>

<div class="container">

	<div class="row" style="margin-top:50px;margin-bottom:50px;">
		<div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
			<form role="form" action="" method="POST">
				<fieldset>
					<h2><?php echo $lang['password_recovery']; ?></h2>
					<hr class="colorgraph">
					<?php
					if(isset($_POST['btn_change'])) {
						$password = protect($_POST['password']);
						$npass = md5($password);
						if(empty($password)) { echo error($lang['error_6']); }
						else {
							$update = $db->query("UPDATE users SET password_recovery='',password='$npass' WHERE id='$row[id]'");
							$_SESSION['suid'] = $row['id'];
							$_SESSION['susername'] = $row['username'];
							header("LOcation: $settings[url]");
						}
					}
					?>
					<div class="form-group">
						<input type="text" name="username" id="username" class="form-control input-lg" placeholder="<?php echo $lang['username']; ?>" disabled value="<?php echo $row['username']; ?>">
					</div>
					<div class="form-group">
						<input type="password" name="password" id="password" class="form-control input-lg" placeholder="<?php echo $lang['new_password']; ?>">
					</div>
					<hr class="colorgraph">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12">
							<input type="submit" class="btn btn-lg btn-success btn-block" name="btn_change" value="<?php echo $lang['btn_4']; ?>">
						</div>
					</div>
				</fieldset>
			</form>
		</div>
	</div>

</div>