<h3><?php echo $lang['contact']; ?></h3>

<?php
if(isset($_POST['btn_send'])) {
	$uname = protect($_POST['uname']);
	$uemail = protect($_POST['uemail']);
	$subject = protect($_POST['subject']);
	$message = protect($_POST['message']);
	
	if(empty($uname) or empty($uemail) or empty($subject) or empty($message)) { echo error($lang['error_7']); }
	elseif(!isValidEmail($uemail)) { echo error($lang['error_10']); }
	else {
		$subject = '['.$settings[sitename].'] '.$subject;
		$headers = 'From: '."$uname <$uemail>". "\r\n" .
'Reply-To: '.$uemail. "\r\n" .
"Content-Type:text/html; charset=UTF-8 \r\n".
'X-Mailer: PHP/' . phpversion();
		$mail = mail($settings['siteemail'], $subject, $message, $headers);
		if($mail) {
			echo success($lang['success_3']);
		} else {
			echo error($lang['error_14']);
		}
	}
}
?>

<form action="" method="POST">
	<div class="form-group">
		<label><?php echo $lang['your_name']; ?></label>
		<input type="text" class="form-control" name="uname">
	</div>
	<div class="form-group">
		<label><?php echo $lang['your_email']; ?></label>
		<input type="text" class="form-control" name="uemail">
	</div>
	<div class="form-group">
		<label><?php echo $lang['subject']; ?></label>
		<input type="text" class="form-control" name="subject">
	</div>
	<div class="form-group">
		<label><?php echo $lang['message']; ?></label>
		<textarea class="form-control" name="message" rows="3"></textarea>
	</div>
	<button type="submit" class="btn btn-primary" name="btn_send"><i class="fa fa-check"></i> <?php echo $lang['btn_6']; ?></button>
</form>