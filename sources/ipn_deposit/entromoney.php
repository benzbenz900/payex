<?php
include("includes/entromoney.php");
$c = protect($_GET['c']);

try {
	$sci = new Paygate_Sci($config);
}
catch (Paygate_Exception $e) {
	exit($e->getMessage());
}

$input = array();
$input['hash'] = $_POST['hash'];

// Decode hash
$error = '';
$tran = $sci->query($input, $error);
foreach($tran as $v => $k) {
	$trans[$v] = $k;
}
$date = date("d/m/Y H:i:s");
$status = $trans['status'];
$payment_id = $trans['payment_id'];
$receiver = $trans['account_purse'];
$sender = $trans['purse'];
$amount = $trans['amount'];
$batch = $trans['batch'];
$accountQuery = $db->query("SELECT * FROM companies WHERE name='Entromoney'");
$acc = $accountQuery->fetch_assoc();
$time = time();
if(checkSession()) { $uid = $_SESSION['suid']; } else { $uid = 0; }
$check_trans = $db->query("SELECT * FROM transactions WHERE txn_id='$batch' and date='$date' and uid='$uid'");
if($c == "status") {
	if($error) {
		echo error($error);
	} else {
		if($status == "completed") {
			if($check_trans->num_rows>0) {
									echo error($lang['error_15']);
								} else {
									$insert = $db->query("INSERT transactions (txn_id,payee,uid,company,amount,currency,date) VALUES ('$batch','$sender','$uid','Entromoney','$amount','$currency','$date')");
									$check_wallet = $db->query("SELECT * FROM wallets WHERE uid='$_SESSION[suid]' and currency='USD'");
				if($check_wallet->num_rows>0) {
					$update_wallet = $db->query("UPDATE wallets SET amount=amount+$amount,updated='$time' WHERE uid='$_SESSION[suid]' and currency='USD'");
					echo success("Your deposit was successfully. You added $amount USD to your wallet.");
				} else {
					$insert = $db->query("INSERT wallets (uid,amount,currency,created) VALUES ('$_SESSION[suid]','$amount','USD','$time')");
					echo success("Your deposit was successfully. You added $amount USD to your wallet.");
				};
								}
		} else {
			echo error($lang['error_20']);
		}
	}
} elseif($c == "success") {
	if($error) {
		echo error($error);
	} else {
		if($status == "completed") {
			if($check_trans->num_rows>0) {
									echo error($lang['error_15']);
								} else {
									$insert = $db->query("INSERT transactions (txn_id,payee,uid,company,amount,currency,date) VALUES ('$batch','$sender','$uid','Entromoney','$amount','$currency','$date')");
									$check_wallet = $db->query("SELECT * FROM wallets WHERE uid='$_SESSION[suid]' and currency='USD'");
				if($check_wallet->num_rows>0) {
					$update_wallet = $db->query("UPDATE wallets SET amount=amount+$amount,updated='$time' WHERE uid='$_SESSION[suid]' and currency='USD'");
					echo success("Your deposit was successfully. You added $amount USD to your wallet.");
				} else {
					$insert = $db->query("INSERT wallets (uid,amount,currency,created) VALUES ('$_SESSION[suid]','$amount','USD','$time')");
					echo success("Your deposit was successfully. You added $amount USD to your wallet.");
				}
								}
		} else {
			echo error($lang['error_20']);
		}
	}
} elseif($c == "fail") {
	echo error($lang['error_18']);
} else {
	echo error($lang['error_20']);
}
?>