<?php
$c = protect($_GET['c']);
//The value is the Security Code generated from the IPN section of your Payza account. Please change it to yours.
	define("IPN_SECURITY_CODE", "xxxxxxxxxxxxxxxx");
	define("MY_MERCHANT_EMAIL", "name@example.com");

	//Setting information about the transaction
	$receivedSecurityCode = $_POST['ap_securitycode'];
	$receivedMerchantEmailAddress = $_POST['ap_merchant'];	
	$transactionStatus = $_POST['ap_status'];
	$testModeStatus = $_POST['ap_test'];	 
	$purchaseType = $_POST['ap_purchasetype'];
	$totalAmountReceived = $_POST['ap_totalamount'];
	$feeAmount = $_POST['ap_feeamount'];
    $netAmount = $_POST['ap_netamount'];
	$transactionReferenceNumber = $_POST['ap_referencenumber'];
	$currency = $_POST['ap_currency']; 	
	$transactionDate= $_POST['ap_transactiondate'];
	$transactionType= $_POST['ap_transactiontype'];
	
	//Setting the customer's information from the IPN post variables
	$customerFirstName = $_POST['ap_custfirstname'];
	$customerLastName = $_POST['ap_custlastname'];
	$customerAddress = $_POST['ap_custaddress'];
	$customerCity = $_POST['ap_custcity'];
	$customerState = $_POST['ap_custstate'];
	$customerCountry = $_POST['ap_custcountry'];
	$customerZipCode = $_POST['ap_custzip'];
	$customerEmailAddress = $_POST['ap_custemailaddress'];
	
	//Setting information about the purchased item from the IPN post variables
	$myItemName = $_POST['ap_itemname'];
	$myItemCode = $_POST['ap_itemcode'];
	$myItemDescription = $_POST['ap_description'];
	$myItemQuantity = $_POST['ap_quantity'];
	$myItemAmount = $_POST['ap_amount'];
	
$accountQuery = $db->query("SELECT * FROM companies WHERE name='Payza'");
$time = time();
$acc = $accountQuery->fetch_assoc();
				$date = date("d/m/Y H:i:s");
				if(checkSession()) { $uid = $_SESSION['suid']; } else { $uid = 0; }
				$check_trans = $db->query("SELECT * FROM transactions WHERE txn_id='$transactionReferenceNumber' and date='$date' and uid='$uid'");
	//Setting extra information about the purchased item from the IPN post variables
	$additionalCharges = $_POST['ap_additionalcharges'];
	$shippingCharges = $_POST['ap_shippingcharges'];
	$taxAmount = $_POST['ap_taxamount'];
	$discountAmount = $_POST['ap_discountamount'];
	 
	//Setting your customs fields received from the IPN post variables
	$myCustomField_1 = $_POST['apc_1'];
	$myCustomField_2 = $_POST['apc_2'];
	$myCustomField_3 = $_POST['apc_3'];
	$myCustomField_4 = $_POST['apc_4'];
	$myCustomField_5 = $_POST['apc_5'];
	$myCustomField_6 = $_POST['apc_6'];
	if($c == "results") {
		if ($receivedMerchantEmailAddress != $row['account']) {
			echo error($lang['error_20']);
		}
		else {	
			//Check if the security code matches
			if ($receivedSecurityCode != $row['a_field_1']) {
				echo error($lang['error_20']);
			}
			else {
				if ($transactionStatus == "Success") {
					if ($testModeStatus == "1") {
						// Since Test Mode is ON, no transaction reference number will be returned.
						// Your site is currently being integrated with Payza IPN for TESTING PURPOSES
						// ONLY. Don't store any information in your production database and 
						// DO NOT process this transaction as a real order.
					}
					else {
						if($check_trans->num_rows>0) {
										echo error($lang['error_15']);
									} else {
										$insert = $db->query("INSERT transactions (txn_id,payee,uid,company,currency,date) VALUES ('$txn_id','$payer_email','$uid','PayPal','$payment_amount','$payment_currency','$date')");
										$check_wallet = $db->query("SELECT * FROM wallets WHERE uid='$_SESSION[suid]' and currency='$currency'");
				if($check_wallet->num_rows>0) {
					$update_wallet = $db->query("UPDATE wallets SET amount=amount+$payment_amount,updated='$time' WHERE uid='$_SESSION[suid]' and currency='$currency'");
					echo success("Your deposit was successfully. You added $payment_amount $currency to your wallet.");
				} else {
					$insert = $db->query("INSERT wallets (uid,amount,currency,created) VALUES ('$_SESSION[suid]','$payment_amount','$currency','$time')");
					echo success("Your deposit was successfully. You added $payment_amount $currency to your wallet.");
				}
									}
					}			
				}
				else {
						echo error($lang['error_20']);
				}
			}
		}
	} elseif($c == "cancel") {
		echo error($lang['error_18']);
	} else {
		echo error($lang['error_2']);
	}
?>