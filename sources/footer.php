	<input type="hidden" id="url" value="<?php echo $settings['url']; ?>">
	<div class="footer_container">
		<div class="container">
			<div class="row" style="padding:30px;">
				<div class="col-sm-12 col-md-12 col-lg-12">
						<a href="<?php echo $settings['url']; ?>page/privacy-policy"><?php echo $lang['privacy_policy']; ?></a> - 
						<a href="<?php echo $settings['url']; ?>page/terms-of-service"><?php echo $lang['terms_of_service']; ?></a> -
						<a href="<?php echo $settings['url']; ?>page/faq">FAQ</a> -
						<a href="<?php echo $settings['url']; ?>page/about"><?php echo $lang['about']; ?></a> - 
						<a href="<?php echo $settings['url']; ?>page/contact"><?php echo $lang['contact']; ?></a>
						<span class="pull-right">
						<?php echo $lang['language']; ?>: <?php echo getLanguage($settings['url'],null,1); ?>
						</span>
						<br>
					
						Copyright &copy; 2016 by <a href="https://www.lnwphp.in.th">lnwPHP</a>
					
				</div>
			</div>
		</div>	
	</div>
	
	<div class="modal fade" id="loader" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  data-backdrop="static" data-keyboard="false">
	  <div class="modal-dialog" role="document">
		<div style="padding:100px;">
			<center>
				<img src="<?php echo $settings['url']; ?>assets/imgs/loader.GIF">
			</center>
		</div>
	  </div>
	</div>
	
	<div id="processing" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
	  <div class="modal-dialog modal-sm" style="width:340px;">
		<div style="margin-top:50px;color:#fff;" id="processing_text">
		  ...
		</div>
	  </div>
	</div>
</body>
</html>