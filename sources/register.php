<?php
if(checkSession()) {
	header("Location: $settings[url]");
}
?>

<div class="container">

	<div class="row" style="margin-top:50px;margin-bottom:50px;">
		<div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
			<form role="form" action="" method="POST">
				<fieldset>
					<h2><?php echo $lang['create_account']; ?></h2>
					<hr class="colorgraph">
					<?php
					if(isset($_POST['btn_register'])) {
						$name = protect($_POST['name']);
						$username = protect($_POST['username']);
						$password = protect($_POST['password']);
						$cpassword = protect($_POST['cpassword']);
						$email = protect($_POST['email']);
						
						$check1 = $db->query("SELECT * FROM users WHERE username='$username'");
						$check2 = $db->query("SELECT * FROM users WHERE email='$email'");
						
						if(empty($name) or empty($username) or empty($password) or empty($cpassword) or empty($email)) { echo error($lang['error_7']); }
						elseif(!isValidUsername($username)) { echo error($lang['error_8']); }
						elseif($check1->num_rows>0) { echo error($lang['error_9']); }
						elseif(!isValidEmail($email)) { echo error($lang['error_10']); }
						elseif($check2->num_rows>0) { echo error($lang['error_11']); }
						elseif($password !== $cpassword) { echo error($lang['error_12']); }
						else {
							$password = md5($password);
							$time = time();
							$insert = $db->query("INSERT users (name,username,password,email,earnings,status,signuptime,ip) VALUES ('$name','$username','$password','$email','0','1','$time','$ip')");
							$query = $db->query("SELECT * FROM users WHERE username='$username' and password='$password'");
							$row = $query->fetch_assoc();
							$_SESSION['suid'] = $row['id'];
							$_SESSION['susername'] = $row['username'];
							header("Location: $settings[url]");
						}
					}
					?>
					<div class="form-group">
						<input type="text" name="name" id="name" class="form-control input-lg" placeholder="<?php echo $lang['your_name']; ?>">
					</div>
					<div class="form-group">
						<input type="text" name="username" id="username" class="form-control input-lg" placeholder="<?php echo $lang['username']; ?>">
					</div>
					<div class="form-group">
						<input type="password" name="password" id="password" class="form-control input-lg" placeholder="<?php echo $lang['password']; ?>">
					</div>
					<div class="form-group">
						<input type="password" name="cpassword" id="cpassword" class="form-control input-lg" placeholder="<?php echo $lang['confirm_password']; ?>">
					</div>
					<div class="form-group">
						<input type="text" name="email" id="email" class="form-control input-lg" placeholder="<?php echo $lang['email_address']; ?>">
					</div>
					<hr class="colorgraph">
					<div class="row">
						<div class="col-xs-6 col-sm-6 col-md-6">
							<input type="submit" class="btn btn-lg btn-success btn-block" name="btn_register" value="<?php echo $lang['btn_3']; ?>">
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6">
							<a href="<?php echo $settings['url']; ?>login" class="btn btn-lg btn-primary btn-block"><?php echo $lang['btn_2']; ?></a>
						</div>
					</div>
				</fieldset>
			</form>
		</div>
	</div>

</div>