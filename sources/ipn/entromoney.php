<?php
include("includes/entromoney.php");
$c = protect($_GET['c']);

try {
	$sci = new Paygate_Sci($config);
}
catch (Paygate_Exception $e) {
	exit($e->getMessage());
}

$input = array();
$input['hash'] = $_POST['hash'];

// Decode hash
$error = '';
$tran = $sci->query($input, $error);
foreach($tran as $v => $k) {
	$trans[$v] = $k;
}
$date = date("d/m/Y H:i:s");
$status = $trans['status'];
$payment_id = $trans['payment_id'];
$receiver = $trans['account_purse'];
$sender = $trans['purse'];
$amount = $trans['amount'];
$batch = $trans['batch'];
$query = $db->query("SELECT * FROM exchanges WHERE id='$trans[payment_id]'");
$row = $query->fetch_assoc();
if(checkSession()) { $uid = $_SESSION['suid']; } else { $uid = 0; }
$check_trans = $db->query("SELECT * FROM transactions WHERE txn_id='$batch' and date='$date' and uid='$uid'");
if($c == "status") {
	if($error) {
		echo error($error);
	} else {
		if($status == "completed") {
			if($check_trans->num_rows>0) {
									echo error($lang['error_15']);
								} else {
									$insert = $db->query("INSERT transactions (txn_id,payee,uid,company,amount,currency,date) VALUES ('$batch','$sender','$uid','Entromoney','$amount','$currency','$date')");
									$update = $db->query("UPDATE exchanges SET status='2' WHERE id='$row[id]'");
									echo success($lang['success_4']);
								}
		} else {
			echo error($lang['error_20']);
		}
	}
} elseif($c == "success") {
	if($error) {
		echo error($error);
	} else {
		if($status == "completed") {
			if($check_trans->num_rows>0) {
									echo error($lang['error_15']);
								} else {
									$insert = $db->query("INSERT transactions (txn_id,payee,uid,company,amount,currency,date) VALUES ('$batch','$sender','$uid','Entromoney','$amount','$currency','$date')");
									$update = $db->query("UPDATE exchanges SET status='2' WHERE id='$row[id]'");
									echo success($lang['success_4']);
								}
		} else {
			echo error($lang['error_20']);
		}
	}
} elseif($c == "fail") {
	$update = $db->query("UPDATE exchanges SET status='3' WHERE id='$row[id]'");
	echo error($lang['error_18']);
} else {
	echo error($lang['error_20']);
}
?>