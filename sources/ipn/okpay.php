<?php
   /* Check IPN and process payment */
   error_reporting(E_ALL ^ E_NOTICE);
  
   // Read the post from OKPAY and add 'ok_verify'
   $request = 'ok_verify=true';
  
   foreach ($_POST as $key => $value) {
       $value = urlencode(stripslashes($value));
       $request .= "&$key=$value";
   }
  
   $fsocket = false;
   $result = false;
  
   // Try to connect via SSL due sucurity reason
   if ( $fp = @fsockopen('ssl://checkout.okpay.com', 443, $errno, $errstr, 30) ) {
       // Connected via HTTPS
       $fsocket = true;
   } elseif ($fp = @fsockopen('checkout.okpay.com', 80, $errno, $errstr, 30)) {
       // Connected via HTTP
       $fsocket = true;
   }
  
   // If connected to OKPAY
   if ($fsocket == true) {
       $header = 'POST /ipn-verify HTTP/1.1' . "\r\n" .
                 'Host: checkout.okpay.com'."\r\n" .
                 'Content-Type: application/x-www-form-urlencoded' . "\r\n" .
                 'Content-Length: ' . strlen($request) . "\r\n" .
                 'Connection: close' . "\r\n\r\n";
  
       @fputs($fp, $header . $request);
       $string = '';
       while (!@feof($fp)) {
           $res = @fgets($fp, 1024);
           $string .= $res;
           // Find verification result in response
           if ( $res == 'VERIFIED' || $res == 'INVALID' || $res == 'TEST') {
               $result = $res;
               break;
           }
       }
       @fclose($fp);
   }
  
   if ($result == 'VERIFIED') {
       // check the "ok_txn_status" is "completed"
       // check that "ok_txn_id" has not been previously processed
       // check that "ok_receiver_email" is your OKPAY email
       // check that "ok_txn_gross"/"ok_txn_currency" are correct
       // process payment
	  	
	   if($_POST['ok_txn_status'] == "completed") {
		$ok_txn_id = $_POST['ok_txn_id'];
		$ok_receiver_email = $_POST['ok_receiver_email'];
		$ok_item_1_id = $_POST['ok_item_1_id'];
		$ok_txn_gross = $_POST['ok_txn_gross'];
		$ok_txn_currency = $_POST['ok_txn_currency'];
		$ok_txn_datetime = $_POST['ok_txn_datetime'];
		$ok_payer_email = $_POST['ok_payer_email'];
		$query = $db->query("SELECT * FROM exchanges WHERE id='$ok_item_1_id'");
		if($query->num_rows==0) { header("Location: $settings[url]"); }

		$row = $query->fetch_assoc();
		$accountQuery = $db->query("SELECT * FROM companies WHERE name='$row[cfrom]'");
$acc = $accountQuery->fetch_assoc();
		if(checkSession()) { $uid = $_SESSION['suid']; } else { $uid = 0; }
		$check_trans = $db->query("SELECT * FROM transactions WHERE txn_id='$ok_txn_id' and date='$ok_txn_datetime' and uid='$uid'");
		if($ok_receiver_email == $acc['a_field_1']) {
			if($ok_txn_gross == $row['amount_from'] or $ok_txn_currency == $row['amount_currency']) {
				if($check_trans->num_rows>0) {
					echo error($lang['error_15']);
				} else {
					$insert = $db->query("INSERT transactions (txn_id,payee,uid,company,amount,currency,date) VALUES ('$ok_txn_id','$ok_receiver_email','$uid','OKPay','$ok_txn_gross','$ok_txn_currency','$ok_txn_datetime')");
					$update = $db->query("UPDATE exchanges SET status='2' WHERE id='$row[id]'");
					echo success($lang['success_4']);
				}
			} else {
				echo error($lang['error_16']);
			}
		} else { 
			echo error($lang['error_17']);
		}
	   } else {
		echo error($lang['error_19']);
	   }
  
   } elseif($result == 'INVALID') {
       // If 'INVALID': log for manual investigation.
  
  
   } elseif($result == 'TEST') {
       // If 'TEST': do something
  
  
   } else {
       // IPN not verified or connection errors
       // If status != 200 IPN will be repeated later
  
       header("HTTP/1.1 404 Not Found");
       exit;
   }
?>