<?php

/*

   This script demonstrates getting and validating SCI
   payment confirmation data from Perfec Money server

   !!! WARNING !!!
   This sample PHP-script is provided AS IS and you should
   use it at your own risk.
   The only purpose of this script is to demonstarate main
   principles of SCI-payment validation proccess.
   You MUST modify it before using with your particular
   Perfect Money account.

*/


/* Constant below contains md5-hashed alternate passhrase in upper case.
   You can generate it like this:
   strtoupper(md5('your_passphrase'));
   Where `your_passphrase' is Alternate Passphrase you entered
   in your Perfect Money account.

   !!! WARNING !!!
   We strongly recommend NOT to include plain Alternate Passphrase in
   this script and use its pre-generated hashed version instead (just
   like we did in this scipt below).
   This is the best way to keep it secure. */
define('ALTERNATE_PHRASE_HASH',  '80F632EBFE5295A9F8933E360EB382DF');

// Path to directory to save logs. Make sure it has write permissions.
define('PATH_TO_LOG',  '/somewhere/out/of/document_root/');

$string=
      $_POST['PAYMENT_ID'].':'.$_POST['PAYEE_ACCOUNT'].':'.
      $_POST['PAYMENT_AMOUNT'].':'.$_POST['PAYMENT_UNITS'].':'.
      $_POST['PAYMENT_BATCH_NUM'].':'.
      $_POST['PAYER_ACCOUNT'].':'.ALTERNATE_PHRASE_HASH.':'.
      $_POST['TIMESTAMPGMT'];

$hash=strtoupper(md5($string));
$orderid = $_POST['PAYMENT_ID'];
$amount = $_POST['PAYMENT_AMOUNT'];
$currency = $_POST['PAYMENT_UNITS'];
$buyer = $_POST['PAYEE_ACCOUNT'];
$trans_id = $_POST['PAYMENT_BATCH_NUM'];
$date = date("d/m/Y H:i:s");
$query = $db->query("SELECT * FROM exchanges WHERE id='$orderid'");
if($query->num_rows==0) { header("Location: $settings[url]"); }
$row = $query->fetch_assoc();
$accountQuery = $db->query("SELECT * FROM companies WHERE name='$row[cfrom]'");
$acc = $accountQuery->fetch_assoc();
if(checkSession()) { $uid = $_SESSION['suid']; } else { $uid = 0; }
$check_trans = $db->query("SELECT * FROM transactions WHERE txn_id='$trans_id' and date='$date' and uid='$uid'");

/* 
   Please use this tool to see how valid hash is genereted: 
   https://perfectmoney.is/acct/md5check.html 
*/
if($hash==$_POST['V2_HASH']){ // proccessing payment if only hash is valid

   /* In section below you must implement comparing of data you recieved
   with data you sent. This means to check if $_POST['PAYMENT_AMOUNT'] is
   particular amount you billed to client and so on. */

   if($_POST['PAYMENT_AMOUNT']==$row['amount_from'] && $_POST['PAYEE_ACCOUNT']==$acc['a_field_1'] && $_POST['PAYMENT_UNITS']==$row['currency_from']){

	if($check_trans->num_rows>0) {
				echo error($lang['error_15']);
			} else {
				$insert = $db->query("INSERT transactions (txn_id,payee,uid,company,amount,currency,date) VALUES ('$trans_id','$buyer','$uid','Perfect Money','$amount','$currency','$date')");
				$update = $db->query("UPDATE exchanges SET status='2' WHERE id='$row[id]'");
				echo success($lang['success_4']);
			}
     
   }else{ // you can also save invalid payments for debug purposes

		echo error($lang['error_21']);
   }


}else{ // you can also save invalid payments for debug purposes

	echo error($lang['error_2']);

}

?>