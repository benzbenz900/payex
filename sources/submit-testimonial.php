<?php
if(!checkSession()) { header("Location: ./login"); }
$show_page=0;
$check_query = $db->query("SELECT * FROM exchanges WHERE uid='$_SESSION[suid]'");
if($check_query->num_rows>0) {
	$check_test = $db->query("SELECT * FROM testimonials WHERE uid='$_SESSION[suid]'");
	if($check_test->num_rows>0) {
		header("Location: ./testimonials");
	}
$show_page=1;
} else {
$show_page=0;
}

if($show_page==0) {
	header("Location: ./testimonials");
}
?>
<div class="container">

	<div class="row" style="margin-top:20px;margin-bottom:20px;">
		
		<div class="col-sm-12 col-md-12 col-lg-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<h3><i class="fa fa-plus"></i> <?php echo $lang['submit_testimonial']; ?></h3></h3>
					<?php
	 if(isset($_POST['btn_submit'])) {
		$message = protect($_POST['message']);
		if(empty($message)) { echo error($lang['error_13']); }
		else {
			$time = time();
			$insert = $db->query("INSERT testimonials (uid,content,status,time) VALUES ('$_SESSION[suid]','$message','0','$time')");
			echo success($lang['success_2']);
		}
	 }
	 ?>
	 <form action="" method="POST">
		<div class="form-group">
			<textarea placeholder="<?php echo $lang['your_feedback']; ?>" name="message" rows="3" class="form-control"></textarea>
		</div>
		<button type="submit" class="btn btn-primary" name="btn_submit"><?php echo $lang['btn_5']; ?></button>
	 </form>	
				</div>
			</div>
		</div>
	</div>

</div>