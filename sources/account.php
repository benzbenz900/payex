<?php
if(!checkSession()) {
	$redirect = $settings['url']."login";
	header("Location: $redirect");
}

$b = protect($_GET['b']);
?>

<div class="container">

	<div class="row" style="margin-top:20px;margin-bottom:20px;">
		<div class="col-sm-3 col-md-3 col-lg-3">
			<div class="panel panel-default">
				<div class="panel-body">
					<ul class="nav nav-pills nav-stacked">
						<li <?php if($b == "exchanges") { echo 'class="active"'; } if($b == "exchange") { echo 'class="active"'; } ?>><a href="<?php echo $settings['url']; ?>account/exchanges"><?php echo $lang['exchanges']; ?></a></li>
						<li <?php if($b == "wallet" or $b == "wallet_exchange") { echo 'class="active"'; } ?>><a href="<?php echo $settings['url']; ?>account/wallet">Wallet</a></li>
						<li <?php if($b == "deposit") { echo 'class="active"'; } ?>><a href="<?php echo $settings['url']; ?>account/deposit">Deposit</a></li>
						<li <?php if($b == "referrals") { echo 'class="active"'; } ?>><a href="<?php echo $settings['url']; ?>account/referrals"><?php echo $lang['referrals']; ?></a></li>
						<li <?php if($b == "withdrawals") { echo 'class="active"'; } if($b == "withdrawal") { echo 'class="active"'; } ?>><a href="<?php echo $settings['url']; ?>account/withdrawals"><?php echo $lang['withdrawals']; ?></a></li>
						<li <?php if($b == "settings") { echo 'class="active"'; } ?>><a href="<?php echo $settings['url']; ?>account/settings"><?php echo $lang['settings']; ?></a></li>
					</ul>
				</div>
			</div>
		</div>
		
		<div class="col-sm-9 col-md-9 col-lg-9">
			<div class="panel panel-default">
				<div class="panel-body">
					<?php
					if($b == "exchanges") {
						include("account/exchanges.php");
					} elseif($b == "exchange") {
						include("account/exchange.php");
					} elseif($b == "wallet") {
						include("account/wallet.php");
					}  elseif($b == "wallet_exchange") {
						include("account/wallet_exchange.php");
					} elseif($b == "deposit") {
						include("account/deposit.php");
					} elseif($b == "referrals") {
						include("account/referrals.php");
					} elseif($b == "withdrawals") {
						include("account/withdrawals.php");
					} elseif($b == "withdrawal") {
						include("account/withdrawal.php");
					} elseif($b == "settings") {
						include("account/settings.php");
					} else {
						echo error($lang['error_1']);
					}
					?>
				</div>
			</div>
		</div>
	</div>

</div>