<?php
$id = protect($_GET['id']);
$query = $db->query("SELECT * FROM exchanges WHERE exchange_id='$id' and uid='$_SESSION[suid]' or exchange_id='$id' and referral_id='$_SESSION[suid]'");
if($query->num_rows==0) { $redirect = $settings['url']."account/exchanges"; header("Location: $redirect"); } 
$row = $query->fetch_assoc();
$accountQuery = $db->query("SELECT * FROM companies WHERE name='$row[cfrom]'");
$acc = $accountQuery->fetch_assoc();
?>
<h3><?php echo $lang['exchange']; ?> #<?php echo $row['exchange_id']; ?></h3>

<table class="table table-hover">
				<thead>
					<tr>
						<td><i class="fa fa-angle-up"></i> <?php echo $lang['send']; ?></td>
						<td><i class="fa fa-angle-down"></i> <?php echo $lang['receive']; ?></td>
						<td><i class="fa fa-dollar"></i> <?php echo $lang['amount']; ?></td>
						<td><i class="fa fa-info-circle"></i> <?php echo $lang['status']; ?></td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><?php echo $row['cfrom']; ?></td>
						<td><?php echo $row['cto']; ?></td>
						<td><?php echo $row['amount_from']; ?> <?php echo $row['currency_from']; ?></td>
						<td>
						<?php
						if($row['status'] == "1") { echo '<span class="label label-info">'.$lang[status_1].'</span>'; }
						elseif($row['status'] == "2") { echo '<span class="label label-info">'.$lang[status_2].'</span>'; }
						elseif($row['status'] == "3") { echo '<span class="label label-danger">'.$lang[status_3].'</span>'; }
						elseif($row['status'] == "4") { echo '<span class="label label-danger">'.$lang[status_4].'</span>'; }
						elseif($row['status'] == "5") { echo '<span class="label label-success">'.$lang[status_5].'</span>'; }
						elseif($row['status'] == "6") { echo '<span class="label label-danger">'.$lang[status_6].'</span>'; }
						else {
							echo '<span class="label label-default">'.$lang[status_7].'</span>';
						}
						?>
						</td>
					</tr>
					<tr>
						<td colspan="4"><br></td>
					</tr>
				</tbody>
				<thead>
					<tr>
						<td><i class="fa fa-user"></i> <?php echo $lang['merchant']; ?></td>
						<td><i class="fa fa-refresh"></i> <?php echo $lang['exchange_rate']; ?></td>
						<td><i class="fa fa-dollar"></i> <?php echo $lang['receive_amount']; ?></td>
						<td><i class="fa fa-clock-o"></i> <?php echo $lang['expiration']; ?></td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><?php echo $acc['a_field_1']; ?></td>
						<td><?php if($row['wallet_id'] > 0) { ?><?php echo $settings['wallet_comission']; ?>%<?php } else { ?>1 <?php echo $row['currency_from']; ?> = <?php echo $row['rate']." ".$row['currency_to']; ?><?php } ?></td>
						<td><?php echo $row['amount_to']; ?>  <?php echo $row['currency_to']; ?></td>
						<td><?php if($row['status'] == "1") { $time = $row['created']+86400; echo date("d/m/Y H:i:s"); } else { echo 'n/a'; } ?></td>
					</tr>
				</tbody>
			</table>