<h3>Exchange from Wallet</h3>

<script type="text/javascript">
function modifyForm(value) {
	if(value == "Bank Transfer") {
		$("#account").hide();
		$("#western").hide();
		$("#bank").show();
	} else if(value == "Western union") {
		$("#account").hide();
		$("#bank").hide();
		$("#western").show();
	} else if(value == "Moneygram") {
		$("#account").hide();
		$("#bank").hide();
		$("#western").show();
	} else {
		$("#bank").hide();
		$("#western").hide();
		$("#account").show();
	}
}
</script>

<?php
if(isset($_POST['btn_exchange'])) {
	$wallet_id = protect($_POST['wallet_id']);
	$walletQuery = $db->query("SELECT * FROM wallets WHERE uid='$_SESSION[suid]' and id='$wallet_id'");
	if($walletQuery->num_rows==0) { $redirect = $settings['url']."account/wallet"; header("Location: $redirect"); }
	$wallet = $walletQuery->fetch_assoc();
	$amount = protect($_POST['amount']);
	$to = protect($_POST['sto']);
	$account = protect($_POST['account']);
	$time = time();
	$u_field_3 = protect($_POST['u_field_3']);
	$u_field_4 = protect($_POST['u_field_4']);
	$uu_field_3 = protect($_POST['uu_field_3']);
	$uu_field_4 = protect($_POST['uu_field_4']);
	$u_field_5 = protect($_POST['u_field_5']);
	$u_field_6 = protect($_POST['u_field_6']);
	$u_field_7 = protect($_POST['u_field_7']);
	$u_field_8 = protect($_POST['u_field_8']);
	$u_field_9 = protect($_POST['u_field_9']);
	$u_field_10 = protect($_POST['u_field_10']);
	$email = idinfo($_SESSION['suid'],"email");
	if(empty($wallet_id) or empty($amount) or empty($to)) { echo error("All fields are required."); }
	elseif($amount > $wallet['amount']) { echo error("You can`t exchange more than $wallet[amount] $wallet[currency]."); }
	elseif(!is_numeric($amount)) { echo error("Please enter amount with numbers."); }
	elseif($from !== "Bitcoin" && $settings['exchminamount'] > $amount) { echo error("Minimal amount for exchange is $settings[exchminamount]."); }
	elseif(!isValidEmail($email)) { echo error("Please enter valid email address."); }
	elseif($to == "PayPal" && !isValidEmail($account)) { echo error("Please enter valid $to account."); }
	elseif($to == "Payeer" && strlen($account)<8) { echo error("Please enter valid $to account."); }
	elseif($to == "Perfect Money" && strlen($account)<7) { echo error("Please enter valid $to account."); }
	elseif($to == "AdvCash" && !isValidEmail($account)) { echo error("Please enter valid $to account."); }
	elseif($to == "OKPay" && strlen($account)<8) { echo error("Please enter valid $to account."); }
	elseif($to == "Entromoney" && strlen($account)<9) { echo error("Please enter valid $to account."); }
	elseif($to == "Payza" && !isValodEmail($account)) { echo error("Please enter valid $to account."); }
	elseif($to == "Bitcoin" && strlen($account)<20) { echo error("Please enter valid $to address."); }
	elseif($to == "Bank Transfer" && empty($u_field_3)) { echo error("Please enter your name."); }
	elseif($to == "Bank Transfer" && empty($u_field_4)) { echo error("Please enter your location."); }
	elseif($to == "Bank Transfer" && empty($u_field_5)) { echo error("Please enter your bank name."); }
	elseif($to == "Bank Transfer" && empty($u_field_6)) { echo error("Please enter your bank account iban."); }
	elseif($to == "Bank Transfer" && empty($u_field_7)) { echo error("Please enter your bank swift."); }
	elseif($to == "Moneygram" && empty($uu_field_3)) { echo error("Please enter your name."); }
	elseif($to == "Moneygram" && empty($uu_field_4)) { echo error("Please enter your location."); }
	elseif($to == "Western union" && empty($uu_field_3)) { echo error("Please enter your name."); }
	elseif($to == "Western union" && empty($uu_field_4)) { echo error("Please enter your location."); }
	else {
		$from = $wallet[currency].' Wallet';
		$comm = 100+$settings['wallet_comission'];
		$com2 = ($amount * 100) / $comm; 
		$com = $amount-$com2; 
		$comission = $amount-$com;
		$amount_to = number_format($comission,2);
		$currency_to = $wallet['currency'];
		$exchange_id = randomHash(6).'-'.randomHash(11).'-'.randomHash(5);
		$ip = $_SERVER['REMOTE_ADDR'];
		if(empty($u_field_3) and !empty($uu_field_3)) {
			$u_field_3 = $uu_field_3;
			$u_field_4 = $uu_field_4;
		}
		$update = $db->query("UPDATE wallets SET amount=amount-$amount WHERE id='$wallet_id'");
		$create = $db->query("INSERT exchanges (uid,wallet_id,cfrom,cto,amount_from,currency_from,amount_to,currency_to,rate,status,created,u_field_1,u_field_2,u_field_3,u_field_4,u_field_5,u_field_6,u_field_7,u_field_8,u_field_9,u_field_10,ip,exchange_id,referral_id) VALUES ('$_SESSION[suid]','$wallet_id','$from','$to','$amount','$wallet[currency]','$amount_to','$currency_to','$settings[wallet_comission]','2','$time','$account','$email','$u_field_3','$u_field_4','$u_field_5','$u_field_6','$u_field_7','$u_field_8','$u_field_9','$u_field_10','$ip','$exchange_id','$referral_id')") or die(mysqli_error($db));;
		$query = $db->query("SELECT * FROM exchanges WHERE exchange_id='$exchange_id'");
		$row = $query->fetch_assoc();
		echo success("The exchange is initiated, you will receive an email when it is completed.");
	}
}
?>

<form action="" method="POST">
	<div class="form-group">
		<div class="form-group">
				<label>From Wallet</label>
				<select name="wallet_id" class="form-control"> 
					<?php 
					$getWallets = $db->query("SELECT * FROM wallets WHERE uid='$_SESSION[suid]'");
					if($getWallets->num_rows>0) {
						while($w = $getWallets->fetch_assoc()) {
							echo '<option value="'.$w[id].'">'.$w[currency].' Wallet: '.$w[amount].'</option>';
						}
					} else {
						echo '<option value="">You do not have wallets.</option>';
					}
					?>
				</select>
			</div>
			<div class="form-group">
				<label><?php echo $lang['amount']; ?></label>
				<input type="text" class="form-control" name="amount">
			</div>
			<div class="form-group">
				<label>To Company</label>
				<select name="sto" class="form-control" onchange="modifyForm(this.value);">
					<option value=""></option>
					<?php 
					$query = $db->query("SELECT * FROM companies WHERE allow_send='1'");
					if($query->num_rows>0) {
						while($row = $query->fetch_assoc()) {
							echo '<option value="'.$row[name].'">'.$row[name].'</option>';
						}
					} else {
						echo '<option value=""></option>';
					}
					?>
				</select>
			</div>	
			<div class="form-group" id="account" style="display:block;">
				<label><?php echo $lang['account']; ?></label>
				<input type="text" class="form-control" name="account">
			</div>
			<div id="bank" style="display:none;">
				<div class="form-group">
					<label>Your name</label>
					<input type="text" class="form-control" name="u_field_3"> 
				</div>
				<div class="form-group">
					<label>Your location</label>
					<input type="text" class="form-control" name="u_field_4"> 
				</div>
				<div class="form-group">
					<label>Bank name</label>
					<input type="text" class="form-control" name="u_field_5"> 
				</div>
				<div class="form-group">
					<label>Bank Account IBAN</label>
					<input type="text" class="form-control" name="u_field_6"> 
				</div>
				<div class="form-group">
					<label>Bank Swift</label>
					<input type="text" class="form-control" name="u_field_7"> 
				</div>
			</div>
			<div id="western" style="display:none;">
				<div class="form-group">
					<label>Your name</label>
					<input type="text" class="form-control" name="uu_field_3"> 
				</div>
				<div class="form-group">
					<label>Your location</label>
					<input type="text" class="form-control" name="uu_field_4"> 
				</div>
			</div>
	</div>
	<button type="submit" class="btn btn-primary" name="btn_exchange"><i class="fa fa-refresh"></i> Exchange</button>
</form>