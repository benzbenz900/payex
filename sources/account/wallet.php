<h3>Wallet</h3>

<?php
$check = $db->query("SELECT * FROM wallets WHERE uid='$_SESSION[suid]' and amount > 0");
if($check->num_rows>0) { ?>
<a href="<?php echo $settings['url']; ?>account/wallet_exchange" class="btn btn-primary"><i class="fa fa-refresh"></i> Exchange from Wallet</a>
<br><br>
<?php } ?>
<table class="table table-striped">
  <thead>
	<tr>
		<th>Wallet currency</th>
		<th>Amount</th>
		<th>Last deposit</th>
	</tr>
  </thead>
  <tbody>
	<?php
	$query = $db->query("SELECT * FROM wallets WHERE uid='$_SESSION[suid]'");
	if($query->num_rows>0) {
		while($row = $query->fetch_assoc()) {
			if($row['currency'] == "USD") {
				$name = 'United States Dollar (USD)';
			} elseif($row['currency'] == "EUR") {
				$name = 'Euro Member Countries (EUR)';
			} elseif($row['currency'] == "BTC") {
				$name = 'Bitcoin (BTC)';
			} else {
				$name = 'Unknown';
			}
			if($row['updated'] > 0) {
				$last_update = date("d/m/Y H:i",$row['updated']);
			} else {
				$last_update = date("d/m/Y H:i",$row['created']);
			}
			echo '<tr>
					<td>'.$name.'</td>
					<td>'.$row[amount].'</td>
					<td>'.$last_update.'</td>
				</tr>';
		}
	} else {
		echo '<tr><td colspan="3">Need to make first deposit to create wallet.</td></tr>'; 
	}
	?>
  </tbody>
</table>