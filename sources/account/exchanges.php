<h3><?php echo $lang['your_exchanges']; ?></h3>

<table class="table table-striped">
  <thead>
	<tr>
		<th width="25%"><i class="fa fa-chevron-up"></i> <?php echo $lang['send']; ?></th>
		<th width="25%"><i class="fa fa-chevron-down"></i> <?php echo $lang['receive']; ?></th>
		<th	width="20%"><?php echo $lang['amount']; ?></th>
		<th width="20%"><?php echo $lang['status']; ?></th>
		<th width="10%"><?php echo $lang['action']; ?></th>
	</tr>
  </thead>
  <tbody>
	<?php
	$page = (int) (!isset($_GET["page"]) ? 1 : $_GET["page"]);
	$limit = 20;
	$startpoint = ($page * $limit) - $limit;
	if($page == 1) {
		$i = 1;
	} else {
		$i = $page * $limit;
	}
	$statement = "exchanges WHERE uid='$_SESSION[suid]'";
	$query = $db->query("SELECT * FROM {$statement} ORDER BY id DESC LIMIT {$startpoint} , {$limit}");
	if($query->num_rows>0) {
		while($row = $query->fetch_assoc()) {
			?>
			<tr>
				<td><?php echo $row['cfrom']; ?></td>
				<td><?php echo $row['cto']; ?></td>
				<td><?php echo $row['amount_from']; ?> <?php echo $row['currency_from']; ?></td>
				<td>
					<?php
					if($row['status'] == "1") { echo '<span class="label label-info">'.$lang[status_1].'</span>'; }
					elseif($row['status'] == "2") { echo '<span class="label label-info">'.$lang[status_2].'</span>'; }
					elseif($row['status'] == "3") { echo '<span class="label label-danger">'.$lang[status_3].'</span>'; }
					elseif($row['status'] == "4") { echo '<span class="label label-danger">'.$lang[status_4].'</span>'; }
					elseif($row['status'] == "5") { echo '<span class="label label-success">'.$lang[status_5].'</span>'; }
					elseif($row['status'] == "6") { echo '<span class="label label-danger">'.$lang[status_6].'</span>'; }
					else {
						echo '<span class="label label-default">'.$lang[status_7].'</span>';
					}
					?>
				</td>
				<td>
					<a href="<?php echo $settings['url']; ?>account/exchange/<?php echo $row['exchange_id']; ?>"><i class="fa fa-search"></i></a> 
				</td>
			</tr>
			<?php
		}
	} else {
		echo '<tr>
				<td colspan="5">'.$lang[no_exchanges].'</td>
			</tr>';
	}
	?>
  </tbody>
</table>

<?php
$ver = $settings['url']."account/exchanges";
if(web_pagination($statement,$ver,$limit,$page)) {
	echo web_pagination($statement,$ver,$limit,$page);
}
?>