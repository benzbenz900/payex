<h3>Deposit</h3>

<?php
if(isset($_POST['btn_process'])) {
	$from = protect($_POST['from']);
	$amount = protect($_POST['amount']);
	$currency = protect($_POST['currency']);
	if(empty($from) or empty($amount) or empty($currency)) { echo error("All fields are required."); }
	elseif(!is_numeric($amount)) { echo error("Please enter amount via numbers. Eg: 20,40,55"); }
	else {
		if($from == "PayPal") {
			$query = $db->query("SELECT * FROM companies WHERE name='$from'");
			$row = $query->fetch_assoc();
			if(empty($row['a_field_1'])) {
				echo error("Sorry, we do not have setuped accounts for this company. Please contact with administrator.");
			} else {
				include("includes/paypal_class.php");
				define('EMAIL_ADD', $row['a_field_1']); // For system notification.
				define('PAYPAL_EMAIL_ADD', $row['a_field_1']);

				// Setup class
				$p = new paypal_class( ); 				 // initiate an instance of the class.
				$p -> admin_mail = EMAIL_ADD; 
				$this_script = $settings['url']."index.php?a=check_deposit&b=paypal";
				$p->add_field('business', PAYPAL_EMAIL_ADD); //don't need add this item. if your set the $p -> paypal_mail.
				$p->add_field('return', $this_script.'&action=success');
				$p->add_field('cancel_return', $this_script.'&action=cancel');
				$p->add_field('notify_url', $this_script.'&action=ipn');
				$p->add_field('item_name', 'Deposit: '.$amount.' '.$currency);
				$p->add_field('item_number', $_SESSION['suid']);
				$p->add_field('amount', $amount);
				$p->add_field('currency_code', $currency);
				$p->add_field('cmd', '_xclick');
				$p->add_field('rm', '2');	// Return method = POST
								 
				$p->submit_paypal_post(); // submit the fields to paypal
				$return = '<script type="text/javascript">$(document).ready(function() { $("#paypal_form").submit(); });</script>';
				$return .= '<div class="alert alert-info"><i class="fa fa-spin fa-spinner"></i> '.$lang[processing].'</div>';
				echo $return;
			}
		} elseif($from == "Payeer") {
			$query = $db->query("SELECT * FROM companies WHERE name='$from'");
			$row = $query->fetch_assoc();
			if(empty($row['a_field_1'])) {
				echo error("Sorry, we do not have setuped accounts for this company. Please contact with administrator.");
			} else {
				$m_shop = $row['a_field_1'];
				$m_orderid = $_SESSION['suid'];
				$m_amount = number_format($amount, 2, '.', '');
				$m_curr = $currency;
				$desc = 'Deposit: '.$amount.' '.$currency;
				$m_desc = base64_encode($desc);
				$m_key = $row['a_field_2'];

				$arHash = array(
					$m_shop,
					$m_orderid,
					$m_amount,
					$m_curr,
					$m_desc,
					$m_key
				);
				$sign = strtoupper(hash('sha256', implode(':', $arHash)));
				$return = '<div style="display:none;"><form method="GET" id="payeer_form" action="https://payeer.com/merchant/">
				<input type="hidden" name="m_shop" value="'.$m_shop.'">
				<input type="hidden" name="m_orderid" value="'.$m_orderid.'">
				<input type="hidden" name="m_amount" value="'.$m_amount.'">
				<input type="hidden" name="m_curr" value="'.$m_curr.'">
				<input type="hidden" name="m_desc" value="'.$m_desc.'">
				<input type="hidden" name="m_sign" value="'.$sign.'">
				<!--
				<input type="hidden" name="form[ps]" value="2609">
				<input type="hidden" name="form[curr[2609]]" value="USD">
				-->
				<input type="submit" name="m_process" value="Pay with Payeer" />
				</form></div>';
				$return .= '<script type="text/javascript">$(document).ready(function() { $("#payeer_form").submit(); });</script>';
				$return .= '<div class="alert alert-info"><i class="fa fa-spin fa-spinner"></i> '.$lang[processing].'</div>';
				echo $return;
			}	
		} elseif($from == "Perfect Money") {
			$query = $db->query("SELECT * FROM companies WHERE name='$from'");
			$row = $query->fetch_assoc();
			if(empty($row['a_field_1'])) {
				echo error("Sorry, we do not have setuped accounts for this company. Please contact with administrator.");
			} else {
				$return = '<div style="display:none;">
							<form action="https://perfectmoney.is/api/step1.asp" id="pm_form" method="POST">
								<input type="hidden" name="PAYEE_ACCOUNT" value="'.$row[a_field_1].'">
								<input type="hidden" name="PAYEE_NAME" value="'.$settings[sitename].'">
								<input type="hidden" name="PAYMENT_ID" value="'.$_SESSION[suid].'">
								<input type="text"   name="PAYMENT_AMOUNT" value="'.$amount.'"><BR>
								<input type="hidden" name="PAYMENT_UNITS" value="'.$currency.'">
								<input type="hidden" name="STATUS_URL" value="'.$settings[url].'index.php?a=check_deposit&b=perfectmoney&c=status">
								<input type="hidden" name="PAYMENT_URL" value="'.$settings[url].'index.php?a=check_deposit&b=perfectmoney&c=complete">
								<input type="hidden" name="PAYMENT_URL_METHOD" value="POST">
								<input type="hidden" name="NOPAYMENT_URL" value="'.$settings[url].'index.php?a=check_deposit&b=perfectmoney&c=failed">
								<input type="hidden" name="NOPAYMENT_URL_METHOD" value="POST">
								<input type="hidden" name="SUGGESTED_MEMO" value="">
								<input type="hidden" name="BAGGAGE_FIELDS" value="IDENT"><br>
								<input type="submit" name="PAYMENT_METHOD" value="Pay Now!" class="tabeladugme"><br><br>
								</form></div>';
				$return .= '<script type="text/javascript">$(document).ready(function() { $("#pm_form").submit(); });</script>';
				$return .= '<div class="alert alert-info"><i class="fa fa-spin fa-spinner"></i> '.$lang[processing].'</div>';
				echo $return;
			}		
		} elseif($from == "AdvCash") {
			$query = $db->query("SELECT * FROM companies WHERE name='$from'");
			$row = $query->fetch_assoc();
			if(empty($row['a_field_1'])) {
				echo error("Sorry, we do not have setuped accounts for this company. Please contact with administrator.");
			} else {
				$arHash = array(
					$row[a_field_1],
					$settings[sitename],
					$amount,
					$currency,
					$row[a_field_2],
					$_SESSION[suid]
				);
				$sign = strtoupper(hash('sha256', implode(':', $arHash)));
				$return = '<div style="display:none;">
							<form method="GET" id="advcash_form" action="https://wallet.advcash.com/sci/">
							<input type="hidden" name="ac_account_email" value="'.$row[a_field_1].'">
							<input type="hidden" name="ac_sci_name" value="'.$settings[sitename].'">
							<input type="hidden" name="ac_amount" value="'.$amount.'">
							<input type="hidden" name="ac_currency" value="'.$currency.'">
							<input type="hidden" name="ac_order_id" value="'.$_SESSION[suid].'">
							<input type="hidden" name="ac_sign"
							value="'.$sign.'">
							<input type="hidden" name="ac_success_url" value="'.$settings[url].'index.php?a=check_deposit&b=advcash&c=success" />
							 <input type="hidden" name="ac_success_url_method" value="GET" />
							 <input type="hidden" name="ac_fail_url" value="'.$settings[url].'index.php?a=check_deposit&b=advcash&c=fail" />
							 <input type="hidden" name="ac_fail_url_method" value="GET" />
							 <input type="hidden" name="ac_status_url" value="'.$settings[url].'index.php?a=check_deposit&b=advcash&c=status" />
							 <input type="hidden" name="ac_status_url_method" value="GET" />
							<input type="hidden" name="ac_comments" value="Deposit: '.$amount.' '.$currency.'">
							</form>
							</div>';
				$return .= '<script type="text/javascript">$(document).ready(function() { $("#advcash_form").submit(); });</script>';
				$return .= '<div class="alert alert-info"><i class="fa fa-spin fa-spinner"></i> '.$lang[processing].'</div>';
				echo $return;
			}		
		} elseif($from == "OKPay") {
			$query = $db->query("SELECT * FROM companies WHERE name='$from'");
			$row = $query->fetch_assoc();
			if(empty($row['a_field_1'])) {
				echo error("Sorry, we do not have setuped accounts for this company. Please contact with administrator.");
			} else {
				$return = '<form  method="post" id="okpay_form" action="https://checkout.okpay.com/">
							   <input type="hidden" name="ok_receiver" value="'.$row[a_field_1].'"/>
							   <input type="hidden" name="ok_item_1_name" value="Deposit: '.$amount.' '.$currency.'"/>
							   <input type="hidden" name="ok_item_1_price" value="'.$amount.'"/>
							   <input type="hidden" name="ok_item_1_id" value="'.$_SESSION[suid].'"/>
							   <input type="hidden" name="ok_currency" value="'.$currency.'"/>
							</form>';
				$return .= '<script type="text/javascript">$(document).ready(function() { $("#okpay_form").submit(); });</script>';
				$return .= '<div class="alert alert-info"><i class="fa fa-spin fa-spinner"></i> '.$lang[processing].'</div>';
				echo $return;
			}		
		} elseif($from == "Entromoney") {
			$query = $db->query("SELECT * FROM companies WHERE name='$from'");
			$row = $query->fetch_assoc();
			if(empty($row['a_field_1'])) {
				echo error("Sorry, we do not have setuped accounts for this company. Please contact with administrator.");
			} else {
				include("includes/entromoney.php");
				$config = array();
				$config['sci_user'] = $row['a_field_1'];
				$config['sci_id'] 	= $row['a_field_2'];
				$config['sci_pass'] = $row['a_field_3'];
				$config['receiver'] = $row['a_field_4'];

				// Call lib sci
				try {
					$sci = new Paygate_Sci($config);
				}
				catch (Paygate_Exception $e) {
					exit($e->getMessage());
				}

				$return = '';
				$input = array();
				$input['sci_user'] 		= $config['sci_user'];
				$input['sci_id'] 		= $config['sci_id'];
				$input['receiver'] 		= $config['receiver'];
				$input['amount'] 		= $amount;
				$input['desc'] 			= 'Exchange '.$amount.' '.$currency;
				$input['payment_id'] 	= $_SESSION['suid'];
				$input['up_1'] 			= 'user_param_1';
				$input['up_2'] 			= 'user_param_2';
				$input['up_3'] 			= 'user_param_3';
				$input['up_4'] 			= 'user_param_4';
				$input['up_5'] 			= 'user_param_5';
				$input['url_status'] 	= $settings[url].'index.php?a=check_deposit&b=entromoney&c=status';
				$input['url_success'] 	= $settings[url].'index.php?a=check_deposit&b=entromoney&c=success';
				$input['url_fail'] 		= $settings[url].'index.php?a=check_deposit&b=entromoney&c=fail';

				// Create hash
				$input['hash']			= $sci->create_hash($input);
				?>
				<form action="<?php echo Paygate_Sci::URL_SCI; ?>" id="entromoney_form" method="post">
					<?php foreach ($input as $p => $v): ?>
						<input type="hidden" name="<?php echo $p; ?>" value="<?php echo $v; ?>">
					<?php endforeach; ?>
				</form>
				<?php
				$return .= '<script type="text/javascript">$(document).ready(function() { $("#entromoney_form").submit(); });</script>';
				$return .= '<div class="alert alert-info"><i class="fa fa-spin fa-spinner"></i> '.$lang[processing].'</div>';
				echo $return;
			}		
		} elseif($from == "Payza") {
			$query = $db->query("SELECT * FROM companies WHERE name='$from'");
			$row = $query->fetch_assoc();
			if(empty($row['a_field_1'])) {
				echo error("Sorry, we do not have setuped accounts for this company. Please contact with administrator.");
			} else {
				$return = '<form method="post" id="payza_form" action="https://secure.payza.com/checkout" >
							<input type="hidden" name="ap_merchant" value="'.$row[a_field_1].'"/>
							<input type="hidden" name="ap_purchasetype" value="item-goods"/>
							<input type="hidden" name="ap_itemname" value="Deposit: '.$amount.' '.$currency.'"/>
							<input type="hidden" name="ap_amount" value="'.$amount.'"/>
							<input type="hidden" name="ap_currency" value="'.$currency.'"/>

							<input type="hidden" name="ap_quantity" value="1"/>
							<input type="hidden" name="ap_itemcode" value="'.$_SESSION[suid].'"/>
							<input type="hidden" name="ap_description" value=""/>
							<input type="hidden" name="ap_returnurl" value="'.$settings[url].'index.php?a=check_deposit&b=payza&c=results"/>
							<input type="hidden" name="ap_cancelurl" value="'.$settings[url].'index.php?a=check_deposit&b=payza&c=cancel"/>

							<input type="hidden" name="ap_taxamount" value="0"/>
							<input type="hidden" name="ap_additionalcharges" value="0"/>
							<input type="hidden" name="ap_shippingcharges" value="0"/> 

							<input type="hidden" name="ap_discountamount" value="0"/> 
							<input type="hidden" name="apc_1" value="Blue"/>

						</form>';
				$return .= '<script type="text/javascript">$(document).ready(function() { $("#payza_form").submit(); });</script>';
				$return .= '<div class="alert alert-info"><i class="fa fa-spin fa-spinner"></i> '.$lang[processing].'</div>';
				echo $return;
			}		
		} elseif($from == "Bitcoin") {
			$query = $db->query("SELECT * FROM companies WHERE name='$from'");
			$row = $query->fetch_assoc();
			if(empty($row['a_field_1'])) {
				echo error("Sorry, we do not have setuped accounts for this company. Please contact with administrator.");
			} else {
				$time = time();
				$insert = $db->query("INSERT deposit_requests (uid,cfrom,amount,currency,status,requested_on) VALUES ('$_SESSION[suid]','Bitcoin','$amount','$currency','1','$time')");
				?>
				<center>
					<img src="https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=<?php echo $row['a_field_1']; ?>&choe=UTF-8"><br/>
					Send <?php echo $amount.' '.$currency; ?> to Bitcoin address:<br/>
					<b><?php echo $row['a_field_1']; ?></b><br/>
					After making the transfer, please let us know <a href="<?php echo $settings['url']; ?>page/contact"><b><?php echo $settings['siteemail']; ?></b></a> email so we can continue the deposit. Please indicate the <b>Transaction ID</b> in message.
				</center>
				<?php
				$hide_form=1;
			}		
		} elseif($from == "Bank Transfer") {
			$query = $db->query("SELECT * FROM companies WHERE name='$from'");
			$row = $query->fetch_assoc();
			if(empty($row['a_field_1'])) {
				echo error("Sorry, we do not have setuped accounts for this company. Please contact with administrator.");
			} else {
				$time = time();
				$insert = $db->query("INSERT deposit_requests (uid,cfrom,amount,currency,status,requested_on) VALUES ('$_SESSION[suid]','Bank Transfer','$amount','$currency','1','$time')");
				?>
				<h4>Bank Details:</h4>
				<b>Bank account owner name:</b> <?php echo $row['a_field_1']; ?><br/>
				<b>Bank name:</b> <?php echo $row['a_field_2']; ?><br/>
				<b>Bank location:</b> <?php echo $row['a_field_3']; ?><br/>
				<b>Bank account iban:</b> <?php echo $row['a_field_4']; ?><br/>
				<b>Bank swift:</b> <?php echo $row['a_field_5']; ?><br/>
				<b>Description:</b> Deposit <?php echo $amount.' '.$currency; ?><br/><br/>
				After making the transfer, please let us know <a href="<?php echo $settings['url']; ?>page/contact"><b><?php echo $settings['siteemail']; ?></b></a> email so we can continue the deposit. Please indicate the <b>Transaction id</b> in message.
				<?php
				$hide_form=1;
			}	
		} elseif($from == "Western union") {
			$query = $db->query("SELECT * FROM companies WHERE name='$from'");
			$row = $query->fetch_assoc();
			if(empty($row['a_field_1'])) {
				echo error("Sorry, we do not have setuped accounts for this company. Please contact with administrator.");
			} else {
				$time = time();
				$insert = $db->query("INSERT deposit_requests (uid,cfrom,amount,currency,status,requested_on) VALUES ('$_SESSION[suid]','Western union','$amount','$currency','1','$time')");
				?>
				<h4>Western union Details:</h4>
				<b>Name:</b> <?php echo $row['a_field_1']; ?><br/>
				<b>Location:</b> <?php echo $row['a_field_2']; ?><br/>
				<b>Description:</b> Deposit <?php echo $amount.' '.$currency; ?><br/><br/>
				After making the transfer, please let us know <a href="<?php echo $settings['url']; ?>page/contact"><b><?php echo $settings['siteemail']; ?></b></a> email so we can continue the deposit. Please indicate the <b>Western union PIN</b> in message.
				<?php
				$hide_form=1;
			}	
		} elseif($from == "Moneygram") {
			$query = $db->query("SELECT * FROM companies WHERE name='$from'");
			$row = $query->fetch_assoc();
			if(empty($row['a_field_1'])) {
				echo error("Sorry, we do not have setuped accounts for this company. Please contact with administrator.");
			} else {
			$time = time();
				$insert = $db->query("INSERT deposit_requests (uid,cfrom,amount,currency,status,requested_on) VALUES ('$_SESSION[suid]','Moneygram','$amount','$currency','1','$time')");
				
				?>
				<h4>Moneygram Details:</h4>
				<b>Name:</b> <?php echo $row['a_field_1']; ?><br/>
				<b>Location:</b> <?php echo $row['a_field_2']; ?><br/>
				<b>Description:</b> Deposit <?php echo $amount.' '.$currency; ?><br/><br/>
				After making the transfer, please let us know <a href="<?php echo $settings['url']; ?>page/contact"><b><?php echo $settings['siteemail']; ?></b></a> email so we can continue the deposit. Please indicate the <b>Moneygram PIN</b> in message.
				<?php
				$hide_form=1;
			}	
		} else {
			echo error("Something wrong.. Please try again.");
		}
	}
}

if($hide_form == 0) {
?>
<form action="" method="POST">
	<div class="form-group">
		<label>Deposit from</label>
		<select class="form-control" name="from" onchange="depositCurrencies(this.value);">
		<option value=""></option>
		<?php
		$send_list = $db->query("SELECT * FROM companies WHERE allow_send='1' ORDER BY id");
		if($send_list->num_rows>0) {
			while($send = $send_list->fetch_assoc()) {
				echo '<option value="'.$send[name].'">'.$send[name].'</option>';
			}
		}
		?>
		</select>
	</div>
	<div class="form-group">
		<label>Amount</label>
		<input type="text" class="form-control" name="amount">
	</div>
	<div class="form-group">
		<label>Currency</label>
		<select class="form-control" name="currency" id="currency_list">
			<option value=""></option>
		</select>
	</div>
	<button type="submit" class="btn btn-primary" name="btn_process"><i class="fa fa-refresh"></i> Process deposit</button>
</form>
<?php
}
?>