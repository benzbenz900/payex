<h3><?php echo $lang['withdrawals']; ?> </h3>

<?php
$check = $db->query("SELECT * FROM wallets WHERE uid='$_SESSION[suid]' and amount > 0");
if($check->num_rows>0) { ?>
<a href="<?php echo $settings['url']; ?>account/withdrawal" class="btn btn-primary"><i class="fa fa-plus"></i> <?php echo $lang['make_withdrawal']; ?></a>
<br><br>
<?php } ?>

<table class="table table-striped">
  <thead>
	<tr>
		<th><?php echo $lang['amount']; ?></td></th>
		<th width="45%"><?php echo $lang['account']; ?></th>
		<th><?php echo $lang['status']; ?></th>
		<th><?php echo $lang['requested_on']; ?></th>
		<th><?php echo $lang['processed_on']; ?></th>
	</tr>
  </thead>
  <tbody>
						<?php
						$query = $db->query("SELECT * FROM withdrawals WHERE uid='$_SESSION[suid]' ORDER BY id");
						if($query->num_rows>0) {
							while($row = $query->fetch_assoc()) {
								$rows[] = $row;
							}
							foreach($rows as $row) {
								?>
								<tr>
									<td><?php echo $row['amount']." ".$row['currency']; ?></td>
									<td><?php echo $row['account']; ?> (<?php echo $row['company']; ?>)</td>
									<td><?php if($row['status'] == "1") { echo $lang['status_8']; } elseif($row['status'] == "2") { echo $lang['status_9']; } else { echo $lang['status_10']; } ?></td>
									<td><?php echo date("d/m/Y H:i",$row['requested_on']); ?></td>
									<td><?php if($row['processed_on'] > 0) { echo date("d/m/Y H:i",$row['processed_on']); } else { echo '-'; } ?></td>
								</tr>
								<?php
							}
						} else {
							echo '<tr><td colspan="5">'.$lang[no_withdrawals].'</td></tr>';
						}
						?>
  </tbody>
</table>
