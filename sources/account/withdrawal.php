<h3><?php echo $lang['make_withdrawal']; ?></h3>

		<?php
		if(isset($_POST['btn_submit'])) {
			$amount = protect($_POST['amount']);
			$company = protect($_POST['company']);
			$account = protect($_POST['account']);
			$wallet_id = protect($_POST['wallet_id']);
			$walletQuery = $db->query("SELECT * FROM wallets WHERE uid='$_SESSION[suid]' and id='$wallet_id'");
			if($walletQuery->num_rows==0) { $redirect = $settings['url']."account/withdrawals"; header("Location: $redirect"); }
			$wallet = $walletQuery->fetch_assoc();
			if(empty($amount)) { echo error($lang['error_24']); }
			elseif(!is_numeric($amount)) { echo error($lang['error_25']); }
			elseif($amount<1) { echo error($lang['error_26']); }
			elseif($amount>$wallet[amount]) { echo error("$lang[error_27] $wallet[amount] $wallet[currency]"); }
			elseif($company == "paypal" && empty($account)) { echo error($lang['error_28']); }
			elseif($company == "paypal" && !isValidEmail($account)) { echo error($lang['error_29']); }
			elseif($company == "skrill" && empty($account)) { echo error($lang['error_30']); }
			elseif($company == "payeer" && empty($account)) { echo error($lang['error_34']); }
			elseif($company == "payeer" && strlen($account)<8) { echo error($lang['error_35']); }
			elseif($company == "perfectmoney" && empty($account)) { echo error($lang['error_36']); }
			elseif($company == "perfectmoney" && strlen($account)<7) { echo error($lang['error_37']); }
			elseif($company == "advcash" && empty($account)) { echo error($lang['error_38']); }
			elseif($company == "advcash" && !isValidEmail($account)) { echo error($lang['error_39']); }
			elseif($company == "okpay" && empty($account)) { echo error($lang['error_40']); }
			elseif($company == "okpay" && strlen($account)<8 or !isValidEmail($account)) { echo error($lang['error_41']); }
			else { 
				$namount = $wallet[amount]-$amount;
				$namount = number_format($namount,2);
				$insert = $db->query("INSERT withdrawals (uid,wallet_id,amount,currency,account,company,status,requested_on) VALUES ('$_SESSION[suid]','$wallet_id','$amount','$wallet[currency]','$account','$company','1','$time')");
				$update = $db->query("UPDATE wallets SET amount='$namount' WHERE uid='$_SESSION[suid]' and id='$wallet_id'");
				$redirect = $settings['url']."account/withdrawals";
				header("Location: $redirect");
			}
		}
		?>
		
		<form id="form_withdrawal" method="POST" action="">
			<div class="form-group">
				<label>From Wallet</label>
				<select name="wallet_id" class="form-control"> 
					<?php 
					$getWallets = $db->query("SELECT * FROM wallets WHERE uid='$_SESSION[suid]'");
					if($getWallets->num_rows>0) {
						while($w = $getWallets->fetch_assoc()) {
							echo '<option value="'.$w[id].'">'.$w[currency].' Wallet: '.$w[amount].'</option>';
						}
					} else {
						echo '<option value="">You do not have wallets.</option>';
					}
					?>
				</select>
			</div>
			<div class="form-group">
				<label><?php echo $lang['amount']; ?></label>
				<input type="text" class="form-control" name="amount">
			</div>
			<div class="form-group">
				<label><?php echo $lang['company']; ?></label>
				<select name="company" class="form-control">
					<option value="paypal">PayPal</option>
					<option value="perfectmoney">Perfect Money</option>
					<option value="okpay">OKPay</option>
					<option value="payeer">Payeer</option>
					<option value="advcash">AdvCash</option>
				</select>
			</div>	
			<div class="form-group">
				<label><?php echo $lang['account']; ?></label>
				<input type="text" class="form-control" name="account">
			</div>
			<button type="submit" class="btn btn-primary" name="btn_submit"><i class="fa fa-check"></i> <?php echo $lang['btn_8']; ?></button>
		</form>