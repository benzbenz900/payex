<h3><?php echo $lang['settings']; ?></h3>

<?php
if(isset($_POST['btn_save'])) {
	$name = protect($_POST['name']);
	$email = protect($_POST['email']);
	$npass = protect($_POST['npass']);
	$mpass = md5($npass);
	$check = $db->query("SELECT * FROM users WHERE email='$email'");
	if(empty($name) or empty($email)) { echo error($lang['error_23']); }
	elseif($email !== idinfo($_SESSION['suid'],"email") && !isValidEmail($email)) { echo error($lang['error_10']); }
	elseif($email !== idinfo($_SESSION['suid'],"email") && $check->num_rows>0) { echo error($lang['error_11']); }
	else {
		if(empty($npass)) {
			$update = $db->query("UPDATE users SET name='$name',email='$email' WHERE id='$_SESSION[suid]'");
			echo success($lang['success_5']);
		} else {
			if($mpass == idinfo($_SESSION['suid'],"password")) {
				echo info($lang['info_1']);
			} else {
				$update = $db->query("UPDATE users SET name='$name',email='$email',password='$mpass' WHERE id='$_SESSION[suid]'");
				echo success($lang['success_5']);
			}
		}
	}
}
?>

<form action="" method="POST">
	<div class="form-group">
		<label><?php echo $lang['your_name']; ?></label>
		<input type="text" class="form-control" name="name" value="<?php echo idinfo($_SESSION['suid'],"name"); ?>">
	</div>
	<div class="form-group">
		<label><?php echo $lang['your_email']; ?></label>
		<input type="text" class="form-control" name="email" value="<?php echo idinfo($_SESSION['suid'],"email"); ?>">
	</div>
	<div class="form-group">
		<label><?php echo $lang['new_password']; ?></label>
		<input type="password" class="form-control" name="npass" placeholder="<?php echo $lang['leave_empty']; ?>">
	</div>
	<button type="submit" class="btn btn-primary" name="btn_save"><i class="fa fa-check"></i> <?php echo $lang['btn_7']; ?></button>
</form>