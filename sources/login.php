<?php
if(checkSession()) {
	header("Location: $settings[url]");
}
?>

<div class="container">

	<div class="row" style="margin-top:50px;margin-bottom:50px;">
		<div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
			<form role="form" action="" method="POST">
				<fieldset>
					<h2><?php echo $lang['login']; ?></h2>
					<hr class="colorgraph">
					<?php
					if(isset($_POST['btn_login'])) {
						$username = protect($_POST['username']);
						$password = protect($_POST['password']);
						$password = md5($password);
						$check = $db->query("SELECT * FROM users WHERE username='$username' and password='$password'");
						if($check->num_rows>0) {
							$row = $check->fetch_assoc();
							if($row['status'] == "2") {
								echo error($lang['error_4']);
							} else {
								if($_POST['remember_me'] == "yes") {
									setcookie("mexchange_user_id", $row['id'], time() + (86400 * 30), '/'); // 86400 = 1 day
									setcookie("mexchange_username", $row['username'], time() + (86400 * 30), '/'); // 86400 = 1 day
								}
								$_SESSION['suid'] = $row['id'];
								$_SESSION['susername'] = $row['username'];
								header("Location: $settings[url]");
							}
						} else {
							echo error($lang['error_5']);
						}
					}
					?>
					<div class="form-group">
						<input type="text" name="username" id="username" class="form-control input-lg" placeholder="<?php echo $lang['username']; ?>">
					</div>
					<div class="form-group">
						<input type="password" name="password" id="password" class="form-control input-lg" placeholder="<?php echo $lang['password']; ?>">
					</div>
					<span class="button-checkbox">
						<label><input type="checkbox" name="remember_me" id="remember_me" checked="checked" value="yes"> <?php echo $lang['remember_me']; ?></label>
						<a href="<?php echo $settings['url']; ?>forgot-password" class="btn btn-link pull-right"><?php echo $lang['forgot_password']; ?></a>
					</span>
					<hr class="colorgraph">
					<div class="row">
						<div class="col-xs-6 col-sm-6 col-md-6">
							<input type="submit" class="btn btn-lg btn-success btn-block" name="btn_login" value="<?php echo $lang['btn_2']; ?>">
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6">
							<a href="<?php echo $settings['url']; ?>register" class="btn btn-lg btn-primary btn-block"><?php echo $lang['btn_3']; ?></a>
						</div>
					</div>
				</fieldset>
			</form>
		</div>
	</div>

</div>