$(document).ready(function() {
	$("#exchange_button").hover(
		function() {
			$(this).find("i").addClass("fa-spin");
		},
		function() {
			$(this).find("i").removeClass("fa-spin");
		}
	);
});

function getReceiveList(from) {
	var url = $("#url").val();
	var data_url = url + "requests/getReceiveList.php?from="+from;
	$.ajax({
		type: "GET",
		url: data_url,
		dataType: "html",
		success: function (data) {
			$("#receive_list").html(data);
		}
	});
}

function getCurrenciesList() {
	var url = $("#url").val();
	var from = $("#send_list").val();
	var to = $("#receive_list").val();
	var data_url = url + "requests/getCurrenciesList.php?from="+from+"&to="+to;
	$.ajax({
		type: "GET",
		url: data_url,
		dataType: "html",
		success: function (data) {
			$("#currency_from_list").html(data);
		}
	});
}

function generateForm() {
	var url = $("#url").val();
	var from = $("#send_list").val();
	var to = $("#receive_list").val();
	var amount_from = $("#amount_from").val();
	var currency_from = $("#currency_from_list").val();
	var data_url = url + "requests/generateForm.php?from="+from+"&to="+to+"&amount_from="+amount_from+"&currency_from="+currency_from;
	$.ajax({
		type: "GET",
		url: data_url,
		dataType: "html",
		success: function (data) {
			$("#exchange_status").html(data);
		}
	});
}

function reCalculateAmount() {
	var url = $("#url").val();
	var from = $("#send_list").val();
	var to = $("#receive_list").val();
	var amount_from = $("#amount_from").val();
	var currency_from = $("#currency_from_list").val();
	var currency_to = $("#currency_to_list").val();
	var data_url = url + "requests/reCalculateAmount.php?from="+from+"&to="+to+"&amount_from="+amount_from+"&currency_from="+currency_from+"&currency_to="+currency_to;
	$.ajax({
		type: "GET",
		url: data_url,
		dataType: "html",
		success: function (data) {
			var ndata = $.trim(data);
			$("#amount_to").val(ndata);
			reloadExchangeRate(from,to,currency_from,currency_to);
		}
	});
}

function reloadExchangeRate(from,to,currency_from,currency_to) {
	var url = $("#url").val();
	var data_url = url + "requests/reloadExchangeRate.php?from="+from+"&to="+to+"&currency_from="+currency_from+"&currency_to="+currency_to;
	$.ajax({
		type: "GET",
		url: data_url,
		dataType: "html",
		success: function (data) {
			var ndata = $.trim(data);
			$("#rate").val(ndata);
		}
	});
}

function validateForm() {
	var url = $("#url").val();	
		var data_url = url + "requests/validateForm.php";
		$.ajax({
			type: "POST",
			url: data_url,
			data: $("#exchange_form").serialize(),
			dataType: "json",
			success: function (data) {
				if(data.status == "success") {
					makeExchange();
				} else {
					$("#exchange_results").html(data.msg);
				}
			}
		});
}

function makeExchange() {
		var url = $("#url").val();
		var data_url = url + "requests/makeExchange.php";
		$.ajax({
			type: "POST",
			url: data_url,
			data: $("#exchange_form").serialize(),
			dataType: "html",
			success: function (data) {
				$("#exchange_results").hide();
				$("#processing").modal("show");
				$("#processing_text").html(data);
			}
		});
}

function depositCurrencies(from) {
	var url = $("#url").val();
	var data_url = url + "requests/depositCurrencies.php?from="+from;
	$.ajax({
		type: "GET",
		url: data_url,
		dataType: "html",
		success: function (data) {
			$("#currency_list").html(data);
		}
	});
}